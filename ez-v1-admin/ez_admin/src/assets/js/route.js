import RouteGenerator from 'ea-router'
import routes from "../../pages/routes";

// 开发环境使用动态路由扫描注册，正式环境需要切换成自动注册的路由
let isDev = false

let route = []

if (isDev){
    let generator = new RouteGenerator(require.context('../../pages', true, /\.vue$/))
    route = generator.generate()
}else {
    route = addComp(routes)
}

function addComp(routes){
    for (let one of routes) {
        let path = one.componentPath.substr(1)
        one.component = ()=>import(`@/pages${path}`)
        if (one.children === undefined){
            one.children = []
        }
        one.children = addComp(one.children)
    }
    return routes
}

export default route
