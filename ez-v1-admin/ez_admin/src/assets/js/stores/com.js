export default {
    saveLoc(key,v){
        let toString = JSON.stringify(v)
        localStorage.setItem(key,toString)
    },
    getLoc(key){
        let string = localStorage.getItem(key)
        return JSON.parse(string)
    }
}