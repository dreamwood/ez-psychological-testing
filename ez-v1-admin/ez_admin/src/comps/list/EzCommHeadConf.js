export default function () {
    return{
        name:"",
        setName(name){this.name = name;return this;},

        title:"文本",
        setTitle(title){this.title = title;return this},

        width:0,
        setWidth(width){this.width = width;return this},

        align:"left",
        setAlignCenter(){this.align = "center";return this},
        setAlignLeft(){this.align = "left";return this},
        setAlignRight(){this.align = "right";return this},

        sortable:false,
        setCanSort(canSort){this.sortable = canSort;return this},

        show:true,
        setHide(){this.show=false;return this},

        showType:"text",
        showTypeKey:null,
        setIsPic(){this.showType = "pic";return this},
        setIsObject(key){this.showType = "object";this.showTypeKey=key;return this},
        setIsObjectName(){this.showType = "object";this.showTypeKey="name";return this},
        setIsObjectTitle(){this.showType = "object";this.showTypeKey="title";return this},
        setIsBool(){this.showType = "bool";return this},
        setIsFiles(){this.showType = "files";return this},
        setIsSwitch(){this.showType = "switch";return this},
        setIsStatus(){this.showType = "status";return this},
        setIsDate(){this.showType = "date";return this},
        setIsColor(){this.showType = "color";return this},
        setIsDateTime(){this.showType = "datetime";return this},
    }
}