import EzHeader from "./EzHeader";
export default function () {
    let headers = []

    function add(name,label,width) {
        let item = EzHeader()
        item.setName(name).setTitle(label).setWidth(width)
        headers.push(item)
        return item
    }
    function addAction(width) {
        let item = EzHeader()
        item.setName("_actions").setTitle("操作").setWidth(width)
        headers.push(item)
        return item
    }
    function addSort() {
        let item = EzHeader()
        item.setName("sort").setTitle("序号").setWidth(120)
        headers.push(item)
        return item
    }


    function addCreateAt() {
        let item = EzHeader()
        item.setName("createAt").setTitle("创建时间").setWidth(180)
            .setIsDateTime()
        headers.push(item)
        return item
    }

    return {
        headers,
        add,
        addSort,
        addCreateAt,
        addAction,
    }
}