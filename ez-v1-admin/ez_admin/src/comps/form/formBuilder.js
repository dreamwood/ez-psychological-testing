import rules from "./rules";
import EzArtConfig from "@/comps/form/EzArtConfig";
import EzTextConfig from "@/comps/form/EzTextConfig";
import EzBlankConfig from "@/comps/form/EzBlankConfig";
import EzRadioConfig from "@/comps/form/EzRadioConfig";
import EzCheckBoxConfig from "@/comps/form/EzCheckBoxConfig";
import EzSwitchConfig from "@/comps/form/EzSwitchConfig";
import EzSelectConfig from "@/comps/form/EzSelectConfig";
import EzDateTimeConfig from "@/comps/form/EzDateTimeConfig";
import EzCascadeConfig from "@/comps/form/EzCascadeConfig";
import EzFilesConfig from "@/comps/form/EzFilesConfig";
import EzSearchConfig from "@/comps/form/EzSearchConfig";
import EzColorConfig from "@/comps/form/EzColorConfig";

export default function () {
    let form = {
        labelPosition: undefined,//
        labelWidth: undefined,//标签宽度，切记undefined和0是两个概念
        spaces: 16,//间隙px单位,这里console汇报红，后面看看有没有什么BUG
    }
    const setLabelPosition = function (pos) {
        form.labelPosition = pos;
        return this
    }
    const setLabelWidth = function (width) {
        form.labelWidth = width;
        return this
    }

    let items = {}

    function addArt(key,label,span) {
        let item = EzArtConfig();
        item.setKey(key)
        if (label !== undefined){
            item.setLabel(label)
        }
        if (span !== undefined){
            item.setSpan(span)
        }
        items[key] = item;
        return item
    }

    function addText(key,label,span) {
        let item = EzTextConfig();
        item.setKey(key);
        if (label !== undefined){
            item.setLabel(label)
        }
        if (span !== undefined){
            item.setSpan(span)
        }
        items[key] = item;
        return item
    }

    function addBlank(key,label,span) {
        let item = EzBlankConfig();
        item.setKey(key);
        if (label !== undefined){
            item.setLabel(label)
        }
        if (span !== undefined){
            item.setSpan(span)
        }
        items[key] = item;
        return item
    }

    function addRadio(key,label,span) {
        let item = EzRadioConfig();
        item.setKey(key);
        if (label !== undefined){
            item.setLabel(label)
        }
        if (span !== undefined){
            item.setSpan(span)
        }
        items[key] = item;
        return item
    }

    function addCheckBox(key,label,span) {
        let item = EzCheckBoxConfig();
        item.setKey(key);
        if (label !== undefined){
            item.setLabel(label)
        }
        if (span !== undefined){
            item.setSpan(span)
        }
        items[key] = item;
        return item
    }

    function addSwitch(key,label,span) {
        let item = EzSwitchConfig();
        item.setKey(key);
        if (label !== undefined){
            item.setLabel(label)
        }
        if (span !== undefined){
            item.setSpan(span)
        }
        items[key] = item;
        return item
    }

    function addSelect(key,label,span) {
        let item = EzSelectConfig();
        item.setKey(key);
        if (label !== undefined){
            item.setLabel(label)
        }
        if (span !== undefined){
            item.setSpan(span)
        }
        items[key] = item;
        return item
    }

    function addDateTime(key,label,span) {
        let item = EzDateTimeConfig();
        item.setKey(key);
        if (label !== undefined){
            item.setLabel(label)
        }
        if (span !== undefined){
            item.setSpan(span)
        }
        items[key] = item;
        return item
    }

    function addCascade(key,label,span) {
        let item = EzCascadeConfig();
        item.setKey(key);
        if (label !== undefined){
            item.setLabel(label)
        }
        if (span !== undefined){
            item.setSpan(span)
        }
        items[key] = item;
        return item
    }

    function addFiles(key,label,span) {
        let item = EzFilesConfig();
        item.setKey(key);
        if (label !== undefined){
            item.setLabel(label)
        }
        if (span !== undefined){
            item.setSpan(span)
        }
        items[key] = item;
        return item
    }

    function addSearch(key,label,span) {
        let item = EzSearchConfig();
        item.setKey(key);
        if (label !== undefined){
            item.setLabel(label)
        }
        if (span !== undefined){
            item.setSpan(span)
        }
        items[key] = item;
        return item
    }

    function addColor(key,label,span) {
        let item = EzColorConfig();
        item.setKey(key);
        if (label !== undefined){
            item.setLabel(label)
        }
        if (span !== undefined){
            item.setSpan(span)
        }
        items[key] = item;
        return item
    }

    return {
        form,//表单配置
        setLabelPosition,
        setLabelWidth,

        rules,
        R: rules,

        items,//表单项
        addArt,
        addText,
        addBlank,
        addRadio,
        addCheckBox,
        addSwitch,
        addSelect,
        addDateTime,
        addCascade,
        addFiles,
        addSearch,
        addColor,
    }
}