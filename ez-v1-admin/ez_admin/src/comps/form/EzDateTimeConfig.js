import EzCommConfig from "@/comps/form/EzCommConfig";
export default function () {
    return{
        ...EzCommConfig(),
        type:"EzDateTime",

        //date/time/year/month/dateTime
        showType:"dateTime",
        setShowTypeDate(){this.showType = "date"; return this},
        setShowTypeTime(){this.showType = "time"; return this},
        setShowTypeYear(){this.showType = "year"; return this},
        setShowTypeMonth(){this.showType = "month"; return this},
    }
}