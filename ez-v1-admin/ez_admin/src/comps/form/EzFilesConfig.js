import EzCommConfig from "./EzCommConfig";
export default function () {
    return{
        ...EzCommConfig(),

        type:"EzFiles",

        fileType:"pic",
        setIsPic() {this.fileType = "pic";return this},
        setIsFile() {
            this.fileType = "file"
            this.setAcceptAny()
            return this
        },

        isMultiple:false,
        setIsMultiple(){this.isMultiple = true;return this},

        accept:".png,.jpeg,.jpg",
        setAccept(accept){this.accept = accept;return this},
        setAcceptPic(){this.accept = ".png,.jpeg,.jpg";return this},
        setAcceptAny(){this.accept = ".*";return this},

    }
}