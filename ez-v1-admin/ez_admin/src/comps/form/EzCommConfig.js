export default function () {
    return {
        type: "",
        setType(type) {
            this.type = type;
            return this
        },

        key: "",
        setKey(key) {
            this.key = key;
            return this
        },

        label: "",
        setLabel(label) {
            this.label = label;
            return this
        },

        labelWidth: undefined,
        setLabelWidth(labelWidth) {
            this.labelWidth = labelWidth;
            return this
        },

        labelFloat: false,
        setLabelFloat() {
            this.labelFloat = true;
            return this
        },

        tips: undefined,
        setTips(tips) {
            this.tips = tips;
            return this
        },

        err: undefined,
        setErr(err) {
            this.err = err;
            return this
        },

        show: true,
        setShow(show) {
            this.show = show;
            return this
        },
        // 布局相关
        span: 12,
        setSpan(span) {
            this.span = span;
            return this
        },
        offset: 0,
        setOffset(offset) {
            this.offset = offset;
            return this
        },

        sn: Date.now(),
        setSn(sn) {
            this.sn = sn;
            return this
        },
        createSn() {
            this.sn = Date.now();
            return this
        },

        isMust: true,
        setIsMust() {
            this.isMust = true;
            return this
        },

        rules: [],
        setRules(rules) {
            this.rules = rules;
            return this;
        },
    }
}