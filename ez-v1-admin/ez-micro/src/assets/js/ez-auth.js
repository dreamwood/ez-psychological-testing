import http from "@/assets/js/http";
import ezStore from "@/assets/js/ez-store";

/**
 正常数据如果后台没有配置，则认为他有权限
 key: "+core:User:add"
 以减号-开头的如果后台没有配置，则认为他没有权限
 key: "-core:User:del"
 * */
export default {
    regAuthList: [],
    userAuthList: {},
    postTimeOutIndex: 0,
    reg(key, name) {
        if (process.env["VUE_APP_DEBUG"] === '1') {
            //写入待注册
            this.regAuthList.push({key, name})
            //清除定时器
            clearTimeout(this.postTimeOutIndex)
            //重新开始计时器
            this.postTimeOutIndex = setTimeout(() => {
                this.post()
            }, 1000)
        }
        return key
    },
    post() {
        http.post("/api/core/admin/Page/_reg", {
            list: this.regAuthList,
            app: process.env["VUE_APP_EZ_APP_ID"]
        }, () => {
        })
    },
    checkAllow(key) {
        if (JSON.stringify(this.userAuthList)==="{}"){
            console.log(typeof this.userAuthList)
            this.getUserAuth()
        }
        if (this.userAuthList[key] !== undefined){
            return this.userAuthList[key]
        }else {
            return key.substr(0, 1) !== "-"
        }
    },
    getUserAuth() {
        this.userAuthList = ezStore.auth.getPages()
    }
}