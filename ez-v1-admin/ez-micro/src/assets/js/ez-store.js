import auth from "@/assets/js/stores/auth";
import counter from "@/assets/js/stores/counter";
export default {
    debug: false,
    auth,
    counter,
}