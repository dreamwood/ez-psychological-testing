import Vue from 'vue'
import App from './App.vue'
import './assets/js/use'
import qiankun from './assets/js/qiankun'

Vue.config.productionTip = false

//自动化路由
import Router from 'vue-router'
Vue.use(Router)
import pagesRegister from "./assets/js/route";
let router = new Router({
  routes: pagesRegister
})

let app = null
Vue.prototype.$isMicro = false
if (window.__POWERED_BY_QIANKUN__) {
  __webpack_public_path__ = window.__INJECTED_PUBLIC_PATH_BY_QIANKUN__;
  Vue.prototype.$isMicro = true
}

function render(props = {}) {
  const { container } = props;
  app = new Vue({
    router,
    render: h => h(App),
  }).$mount(container ? container.querySelector('#micro-app') : '#micro-app');
}



if (window.__POWERED_BY_WUJIE__) {
  let app;
  window.__WUJIE_MOUNT = () => {
    const router = new Router({ routes:pagesRegister });
    app = new Vue({ router, render: (h) => h(App) }).$mount("#app");
  };
  window.__WUJIE_UNMOUNT = () => {
    app.$destroy();
  };
}

// 独立运行时
if (!window.__POWERED_BY_QIANKUN__ && !window.__POWERED_BY_WUJIE__) {
  render();
}

export async function bootstrap() {
  // console.log('[vue] vue app bootstraped');
}
export async function mount(props) {
  render(props);
  props.onGlobalStateChange((state, prev) => {
    // state: 变更后的状态; prev 变更前的状态
    // console.log(state, prev);
  });
  qiankun.setProps(props)

}
export async function unmount() {
  // console.log("unmount")
  app.$destroy();
  app.$el.innerHTML = '';
  // vue = null;
  // router = null;
}
