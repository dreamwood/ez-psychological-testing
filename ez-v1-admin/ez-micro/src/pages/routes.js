export default [
  {
    "name": "root",
    "path": "/",
    "componentPath": "./layout.vue",
    "children": [
      {
        "name": "GGESHXHJVG",
        "path": "doc",
        "componentPath": "./doc/layout.vue",
        "children": [
          {
            "name": "TEMSPTJLTD",
            "path": "models",
            "componentPath": "./doc/models.vue"
          },
          {
            "name": "AXAKJIVRSF",
            "path": "welcome",
            "componentPath": "./doc/welcome.vue"
          }
        ]
      },
      {
        "name": "QHXHCGSLMJ",
        "path": "login",
        "componentPath": "./login.vue"
      },
      {
        "name": "DMGRSOJRRA",
        "path": "v1",
        "componentPath": "./v1/layout.vue",
        "children": [
          {
            "name": "",
            "path": "core",
            "componentPath": "./v1/core/layout.vue",
            "children": [
              {
                "name": "",
                "path": "Api",
                "componentPath": "./v1/core/Api/layout.vue",
                "children": [
                  {
                    "name": "NLKGGJUYNP",
                    "path": "edit",
                    "componentPath": "./v1/core/Api/edit.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/core/Api/js/layout.vue"
                  },
                  {
                    "name": "XFKASBYYHM",
                    "path": "list",
                    "componentPath": "./v1/core/Api/list.vue"
                  },
                  {
                    "name": "TYQQHWDVOS",
                    "path": "tree",
                    "componentPath": "./v1/core/Api/tree.vue"
                  }
                ]
              },
              {
                "name": "",
                "path": "GateWay",
                "componentPath": "./v1/core/GateWay/layout.vue",
                "children": [
                  {
                    "name": "BRLFSOIPBR",
                    "path": "edit",
                    "componentPath": "./v1/core/GateWay/edit.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/core/GateWay/js/layout.vue"
                  },
                  {
                    "name": "NXPDTMOWLE",
                    "path": "list",
                    "componentPath": "./v1/core/GateWay/list.vue"
                  },
                  {
                    "name": "TEIRXKPRYO",
                    "path": "tree",
                    "componentPath": "./v1/core/GateWay/tree.vue"
                  }
                ]
              },
              {
                "name": "",
                "path": "Host",
                "componentPath": "./v1/core/Host/layout.vue",
                "children": [
                  {
                    "name": "EVXGOUUYMN",
                    "path": "edit",
                    "componentPath": "./v1/core/Host/edit.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/core/Host/js/layout.vue"
                  },
                  {
                    "name": "VFITMNNRUT",
                    "path": "list",
                    "componentPath": "./v1/core/Host/list.vue"
                  },
                  {
                    "name": "FIMBEYEEOR",
                    "path": "tree",
                    "componentPath": "./v1/core/Host/tree.vue"
                  }
                ]
              },
              {
                "name": "",
                "path": "Menu",
                "componentPath": "./v1/core/Menu/layout.vue",
                "children": [
                  {
                    "name": "BYMPOSWKNU",
                    "path": "edit",
                    "componentPath": "./v1/core/Menu/edit.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/core/Menu/js/layout.vue"
                  },
                  {
                    "name": "QKXODWAQPO",
                    "path": "list",
                    "componentPath": "./v1/core/Menu/list.vue"
                  },
                  {
                    "name": "IXQNYITTIW",
                    "path": "tree",
                    "componentPath": "./v1/core/Menu/tree.vue"
                  }
                ]
              },
              {
                "name": "",
                "path": "Model",
                "componentPath": "./v1/core/Model/layout.vue",
                "children": [
                  {
                    "name": "IGMKQUAXXH",
                    "path": "edit",
                    "componentPath": "./v1/core/Model/edit.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/core/Model/js/layout.vue"
                  },
                  {
                    "name": "USVXWVMLQP",
                    "path": "list",
                    "componentPath": "./v1/core/Model/list.vue"
                  },
                  {
                    "name": "XASAJGXRHK",
                    "path": "tree",
                    "componentPath": "./v1/core/Model/tree.vue"
                  }
                ]
              },
              {
                "name": "",
                "path": "Page",
                "componentPath": "./v1/core/Page/layout.vue",
                "children": [
                  {
                    "name": "NRNEAHCRWL",
                    "path": "edit",
                    "componentPath": "./v1/core/Page/edit.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/core/Page/js/layout.vue"
                  },
                  {
                    "name": "UNWLKDOKJC",
                    "path": "list",
                    "componentPath": "./v1/core/Page/list.vue"
                  },
                  {
                    "name": "XWRYPSNESC",
                    "path": "tree",
                    "componentPath": "./v1/core/Page/tree.vue"
                  }
                ]
              },
              {
                "name": "",
                "path": "RequestLog",
                "componentPath": "./v1/core/RequestLog/layout.vue",
                "children": [
                  {
                    "name": "NQBTRAISAL",
                    "path": "edit",
                    "componentPath": "./v1/core/RequestLog/edit.vue"
                  },
                  {
                    "name": "SMESXTGRIS",
                    "path": "info",
                    "componentPath": "./v1/core/RequestLog/info.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/core/RequestLog/js/layout.vue"
                  },
                  {
                    "name": "DLIYEFDMGS",
                    "path": "list",
                    "componentPath": "./v1/core/RequestLog/list.vue"
                  },
                  {
                    "name": "MPFUBCYNDM",
                    "path": "tree",
                    "componentPath": "./v1/core/RequestLog/tree.vue"
                  }
                ]
              },
              {
                "name": "",
                "path": "Role",
                "componentPath": "./v1/core/Role/layout.vue",
                "children": [
                  {
                    "name": "CTJYUDPAFV",
                    "path": "edit",
                    "componentPath": "./v1/core/Role/edit.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/core/Role/js/layout.vue",
                    "children": [
                      {
                        "name": "INNAGSBJGJ",
                        "path": "ApiSelector",
                        "componentPath": "./v1/core/Role/js/ApiSelector.vue"
                      },
                      {
                        "name": "KLYDCTMNRT",
                        "path": "PageSelector",
                        "componentPath": "./v1/core/Role/js/PageSelector.vue"
                      },
                      {
                        "name": "XQKSTDACNW",
                        "path": "PageSelector_bkp",
                        "componentPath": "./v1/core/Role/js/PageSelector_bkp.vue"
                      }
                    ]
                  },
                  {
                    "name": "IBQMFMGHWU",
                    "path": "list",
                    "componentPath": "./v1/core/Role/list.vue"
                  },
                  {
                    "name": "KWITYRDYUQ",
                    "path": "tree",
                    "componentPath": "./v1/core/Role/tree.vue"
                  }
                ]
              },
              {
                "name": "",
                "path": "Route",
                "componentPath": "./v1/core/Route/layout.vue",
                "children": [
                  {
                    "name": "DBNTHFUVKV",
                    "path": "edit",
                    "componentPath": "./v1/core/Route/edit.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/core/Route/js/layout.vue"
                  },
                  {
                    "name": "QPJEMNAKRX",
                    "path": "list",
                    "componentPath": "./v1/core/Route/list.vue"
                  },
                  {
                    "name": "BECTWXMTFL",
                    "path": "tree",
                    "componentPath": "./v1/core/Route/tree.vue"
                  }
                ]
              },
              {
                "name": "",
                "path": "Top",
                "componentPath": "./v1/core/Top/layout.vue",
                "children": [
                  {
                    "name": "JMKQRLRXCK",
                    "path": "edit",
                    "componentPath": "./v1/core/Top/edit.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/core/Top/js/layout.vue"
                  },
                  {
                    "name": "GFWHBQGEBV",
                    "path": "list",
                    "componentPath": "./v1/core/Top/list.vue"
                  },
                  {
                    "name": "YNDLBHVJXI",
                    "path": "tree",
                    "componentPath": "./v1/core/Top/tree.vue"
                  }
                ]
              },
              {
                "name": "",
                "path": "User",
                "componentPath": "./v1/core/User/layout.vue",
                "children": [
                  {
                    "name": "IVSQUYQFPA",
                    "path": "edit",
                    "componentPath": "./v1/core/User/edit.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/core/User/js/layout.vue"
                  },
                  {
                    "name": "YSCCNQWAPU",
                    "path": "list",
                    "componentPath": "./v1/core/User/list.vue"
                  },
                  {
                    "name": "CSUYNJQNDV",
                    "path": "tree",
                    "componentPath": "./v1/core/User/tree.vue"
                  }
                ]
              }
            ]
          },
          {
            "name": "",
            "path": "dev",
            "componentPath": "./v1/dev/layout.vue",
            "children": [
              {
                "name": "",
                "path": "EzModel",
                "componentPath": "./v1/dev/EzModel/layout.vue",
                "children": [
                  {
                    "name": "TQAOYSCBWY",
                    "path": "dev",
                    "componentPath": "./v1/dev/EzModel/dev.vue"
                  },
                  {
                    "name": "VBRTQJRIER",
                    "path": "edit",
                    "componentPath": "./v1/dev/EzModel/edit.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/dev/EzModel/js/layout.vue"
                  },
                  {
                    "name": "QSTCSSLKFI",
                    "path": "list",
                    "componentPath": "./v1/dev/EzModel/list.vue"
                  },
                  {
                    "name": "WUCHJFMGWS",
                    "path": "tree",
                    "componentPath": "./v1/dev/EzModel/tree.vue"
                  }
                ]
              },
              {
                "name": "",
                "path": "EzModelField",
                "componentPath": "./v1/dev/EzModelField/layout.vue",
                "children": [
                  {
                    "name": "GGQBMCLQOB",
                    "path": "edit",
                    "componentPath": "./v1/dev/EzModelField/edit.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/dev/EzModelField/js/layout.vue"
                  },
                  {
                    "name": "NTPTVOHNCD",
                    "path": "list",
                    "componentPath": "./v1/dev/EzModelField/list.vue"
                  },
                  {
                    "name": "XSQKGPKYLM",
                    "path": "tree",
                    "componentPath": "./v1/dev/EzModelField/tree.vue"
                  }
                ]
              }
            ]
          },
          {
            "name": "",
            "path": "uc",
            "componentPath": "./v1/uc/layout.vue",
            "children": [
              {
                "name": "",
                "path": "Ucache",
                "componentPath": "./v1/uc/Ucache/layout.vue",
                "children": [
                  {
                    "name": "CHOAWJXBBV",
                    "path": "edit",
                    "componentPath": "./v1/uc/Ucache/edit.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/uc/Ucache/js/layout.vue"
                  },
                  {
                    "name": "OCBTSPVWPT",
                    "path": "list",
                    "componentPath": "./v1/uc/Ucache/list.vue"
                  },
                  {
                    "name": "TFHJIFOXNJ",
                    "path": "tree",
                    "componentPath": "./v1/uc/Ucache/tree.vue"
                  }
                ]
              }
            ]
          },
          {
            "name": "QUHXKQXQME",
            "path": "welcome",
            "componentPath": "./v1/welcome.vue"
          },
          {
            "name": "",
            "path": "xl",
            "componentPath": "./v1/xl/layout.vue",
            "children": [
              {
                "name": "",
                "path": "Answer",
                "componentPath": "./v1/xl/Answer/layout.vue",
                "children": [
                  {
                    "name": "DFAVWASENV",
                    "path": "QuestionAnswer",
                    "componentPath": "./v1/xl/Answer/QuestionAnswer.vue"
                  },
                  {
                    "name": "RCINMNEGKX",
                    "path": "edit",
                    "componentPath": "./v1/xl/Answer/edit.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/xl/Answer/js/layout.vue"
                  },
                  {
                    "name": "OGBVHPUGIY",
                    "path": "list",
                    "componentPath": "./v1/xl/Answer/list.vue"
                  },
                  {
                    "name": "IRYUTVSYDD",
                    "path": "tree",
                    "componentPath": "./v1/xl/Answer/tree.vue"
                  }
                ]
              },
              {
                "name": "",
                "path": "Choice",
                "componentPath": "./v1/xl/Choice/layout.vue",
                "children": [
                  {
                    "name": "FCXDLMVPCO",
                    "path": "QuestionChoice",
                    "componentPath": "./v1/xl/Choice/QuestionChoice.vue"
                  },
                  {
                    "name": "YXRMYGHVCN",
                    "path": "edit",
                    "componentPath": "./v1/xl/Choice/edit.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/xl/Choice/js/layout.vue"
                  },
                  {
                    "name": "INKPRRTJCO",
                    "path": "list",
                    "componentPath": "./v1/xl/Choice/list.vue"
                  },
                  {
                    "name": "HVWISBJQNA",
                    "path": "tree",
                    "componentPath": "./v1/xl/Choice/tree.vue"
                  }
                ]
              },
              {
                "name": "",
                "path": "Question",
                "componentPath": "./v1/xl/Question/layout.vue",
                "children": [
                  {
                    "name": "OSTAYHMGUO",
                    "path": "edit",
                    "componentPath": "./v1/xl/Question/edit.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/xl/Question/js/layout.vue"
                  },
                  {
                    "name": "GIUKQVWYYM",
                    "path": "list",
                    "componentPath": "./v1/xl/Question/list.vue"
                  },
                  {
                    "name": "GQUFHEXLVQ",
                    "path": "tree",
                    "componentPath": "./v1/xl/Question/tree.vue"
                  }
                ]
              },
              {
                "name": "",
                "path": "Test",
                "componentPath": "./v1/xl/Test/layout.vue",
                "children": [
                  {
                    "name": "HERCSMRHFN",
                    "path": "edit",
                    "componentPath": "./v1/xl/Test/edit.vue"
                  },
                  {
                    "name": "VULJGUMQSJ",
                    "path": "imports",
                    "componentPath": "./v1/xl/Test/imports.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/xl/Test/js/layout.vue"
                  },
                  {
                    "name": "SETEXGITFC",
                    "path": "list",
                    "componentPath": "./v1/xl/Test/list.vue"
                  },
                  {
                    "name": "IKCBSJQHSO",
                    "path": "tree",
                    "componentPath": "./v1/xl/Test/tree.vue"
                  }
                ]
              },
              {
                "name": "",
                "path": "TestAnswer",
                "componentPath": "./v1/xl/TestAnswer/layout.vue",
                "children": [
                  {
                    "name": "SWTIQJSQDE",
                    "path": "TestAnswer",
                    "componentPath": "./v1/xl/TestAnswer/TestAnswer.vue"
                  },
                  {
                    "name": "VWNFHCIOTT",
                    "path": "edit",
                    "componentPath": "./v1/xl/TestAnswer/edit.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/xl/TestAnswer/js/layout.vue"
                  },
                  {
                    "name": "HSLKYEYNPD",
                    "path": "list",
                    "componentPath": "./v1/xl/TestAnswer/list.vue"
                  },
                  {
                    "name": "ACCILUSYPM",
                    "path": "tree",
                    "componentPath": "./v1/xl/TestAnswer/tree.vue"
                  }
                ]
              },
              {
                "name": "",
                "path": "TestCategory",
                "componentPath": "./v1/xl/TestCategory/layout.vue",
                "children": [
                  {
                    "name": "QTDTWOVFLV",
                    "path": "edit",
                    "componentPath": "./v1/xl/TestCategory/edit.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/xl/TestCategory/js/layout.vue"
                  },
                  {
                    "name": "BKITBMVUKA",
                    "path": "list",
                    "componentPath": "./v1/xl/TestCategory/list.vue"
                  },
                  {
                    "name": "UTDUXENWOW",
                    "path": "tree",
                    "componentPath": "./v1/xl/TestCategory/tree.vue"
                  }
                ]
              },
              {
                "name": "",
                "path": "TestType",
                "componentPath": "./v1/xl/TestType/layout.vue",
                "children": [
                  {
                    "name": "ESVXGGGXAJ",
                    "path": "edit",
                    "componentPath": "./v1/xl/TestType/edit.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/xl/TestType/js/layout.vue"
                  },
                  {
                    "name": "NQJCXEQGIP",
                    "path": "list",
                    "componentPath": "./v1/xl/TestType/list.vue"
                  },
                  {
                    "name": "PCHJVLSDYI",
                    "path": "tree",
                    "componentPath": "./v1/xl/TestType/tree.vue"
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  }
]