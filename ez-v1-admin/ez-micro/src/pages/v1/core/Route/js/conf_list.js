import listBuilder from "@/comps/list/listBuilder";

/*
lb.add("appId","APPID",80)
lb.add("from","From",80)
lb.add("to","To",80)
lb.add("isOn","启用",80)
lb.add("sort","权重",80)
lb.add("gateWay","所属网关",80)

*/
export default function () {
    let lb = listBuilder()
    lb.add("id","#",80)

    lb.add("gateWay","所属网关",80).setListIsObject("port")
    lb.add("appId","APPID",180)
    lb.add("from","From",180)
    lb.add("to","To",180)
    lb.add("isOn","启用",100).setListIsSwitch()
    lb.add("sort","权重",80)
    lb.addAction(120).setAlignRight()
    return lb.headers
}