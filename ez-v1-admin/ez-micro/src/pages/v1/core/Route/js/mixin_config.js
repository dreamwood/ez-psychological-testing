export default {
    data(){
        return{
            title:"路由",
            //是否启用排序功能
            sortOpen:true,
            //排序选项配置
            sorts:[
                {label:"创建",key:"id"},
            ],

            //是否启用搜索功能
            searchOpen:false,

            //是否启用批量编辑功能
            editOpen:false,

            //是否启用自定义表头功能
            tableHeadCacheOpen:false,
            //自定义表头缓存键
            tableHeadCacheKey:"core.Route.list",

            //编辑配置，启用时，新增和编辑不会打开新的窗口，以弹出Dialog的形式编辑
            useModal:true,
            fullScreenEdit:false,

            //是否显示工具箱
            useTool: false,

            pageSizeSet:[10,20,50,100],
            settingOpen:false,
            askBeforeDelete:true,
        }
    }
}
