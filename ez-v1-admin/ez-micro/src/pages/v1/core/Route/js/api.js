import http from "@/assets/js/http";

export default {
    urlFind: '/core/admin/Route/info',
    urlFindToEdit: '/core/admin/Route/get',
    urlFindBy: '/core/admin/Route/list',
    urlSave: '/core/admin/Route/save',
    urlDelete: '/core/admin/Route/delete',
    urlDeleteAll: '/core/admin/Route/delete_many',
    urlCopy: '/core/admin/Route/copy',
    urlEditMany: '/core/admin/Route/edit_many',
    urlTree: '/core/admin/Route/tree',
    urlChoice: '/core/admin/Route/choice',
    urlReload: '/core/admin/Route/reload',
    reload(func) {
        http.get(this.urlReload, {}, res => {
            func(res)
        })
    },
    find(id, func) {
        http.get(this.urlFind, {id}, res => {
            func(res)
        })
    },
    findToEdit(id, func) {
        http.get(this.urlFindToEdit, {id}, res => {
            func(res)
        })
    },
    findBy(param, func) {
        http.post(this.urlFindBy, param, res => {
            func(res.data.lists, res.data.query)
        })
    },
    save(data, func) {
        http.post(this.urlSave, data, res => {
            func(res)
        })
    },
    delete(id, func) {
        http.get(this.urlDelete, {id}, res => {
            func(res)
        })
    },
    deleteAll(ids, func) {
        http.post(this.urlDeleteAll, {ids}, res => {
            func(res)
        })
    },
    copy(ids, func) {
        http.post(this.urlCopy, {ids}, res => {
            func(res)
        })
    },
    editMany(ids, data, func) {
        http.post(this.urlEditMany, {
            ids,
            _action: data
        }, res => {
            func(res)
        })
    },
}