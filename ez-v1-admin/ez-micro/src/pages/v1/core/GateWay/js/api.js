import http from "@/assets/js/http";

export default {
    urlFind: '/core/admin/GateWay/info',
    urlFindToEdit: '/core/admin/GateWay/get',
    urlFindBy: '/core/admin/GateWay/list',
    urlSave: '/core/admin/GateWay/save',
    urlDelete: '/core/admin/GateWay/delete',
    urlDeleteAll: '/core/admin/GateWay/delete_many',
    urlCopy: '/core/admin/GateWay/copy',
    urlEditMany: '/core/admin/GateWay/edit_many',
    urlTree: '/core/admin/GateWay/tree',
    urlChoice: '/core/admin/GateWay/choice',
    find(id, func) {
        http.get(this.urlFind, {id}, res => {
            func(res)
        })
    },
    findToEdit(id, func) {
        http.get(this.urlFindToEdit, {id}, res => {
            func(res)
        })
    },
    findBy(param, func) {
        http.post(this.urlFindBy, param, res => {
            func(res.data.lists, res.data.query)
        })
    },
    save(data, func) {
        http.post(this.urlSave, data, res => {
            func(res)
        })
    },
    delete(id, func) {
        http.get(this.urlDelete, {id}, res => {
            func(res)
        })
    },
    deleteAll(ids, func) {
        http.post(this.urlDeleteAll, {ids}, res => {
            func(res)
        })
    },
    copy(ids, func) {
        http.post(this.urlCopy, {ids}, res => {
            func(res)
        })
    },
    editMany(ids, data, func) {
        http.post(this.urlEditMany, {
            ids,
            _action: data
        }, res => {
            func(res)
        })
    },
}