import listBuilder from "@/comps/list/listBuilder";

/*
lb.add("ip","IP地址",80)
lb.add("port","端口",80)
lb.add("isOn","开启",80)
lb.add("hosts","主机",80)

*/
export default function () {
    let lb = listBuilder()
    lb.add("id","#",80)
    lb.add("ip","IP地址",280)
    lb.add("port","端口",180)
    lb.add("isOn","开启",180).setListIsSwitch()
    lb.addAction(120).setAlignRight()
    return lb.headers
}