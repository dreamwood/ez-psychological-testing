import http from "@/assets/js/http";

export default {
    urlFind: '/core/admin/Role/info',
    urlFindToEdit: '/core/admin/Role/get',
    urlFindBy: '/core/admin/Role/list',
    urlSave: '/core/admin/Role/save',
    urlDelete: '/core/admin/Role/delete',
    urlDeleteAll: '/core/admin/Role/delete_many',
    urlCopy: '/core/admin/Role/copy',
    urlEditMany: '/core/admin/Role/edit_many',
    urlTree: '/core/admin/Role/tree',
    urlChoice: '/core/admin/Role/choice',
    urlReload: '/core/admin/Role/reload',
    reload(func) {
        http.get(this.urlReload, {}, res => {
            func(res)
        })
    },
    find(id, func) {
        http.get(this.urlFind, {id}, res => {
            func(res)
        })
    },
    findToEdit(id, func) {
        http.get(this.urlFindToEdit, {id}, res => {
            func(res)
        })
    },
    findBy(param, func) {
        http.post(this.urlFindBy, param, res => {
            func(res.data.lists, res.data.query)
        })
    },
    save(data, func) {
        http.post(this.urlSave, data, res => {
            func(res)
        })
    },
    delete(id, func) {
        http.get(this.urlDelete, {id}, res => {
            func(res)
        })
    },
    deleteAll(ids, func) {
        http.post(this.urlDeleteAll, {ids}, res => {
            func(res)
        })
    },
    copy(ids, func) {
        http.post(this.urlCopy, {ids}, res => {
            func(res)
        })
    },
    editMany(ids, data, func) {
        http.post(this.urlEditMany, {
            ids,
            _action: data
        }, res => {
            func(res)
        })
    },
}