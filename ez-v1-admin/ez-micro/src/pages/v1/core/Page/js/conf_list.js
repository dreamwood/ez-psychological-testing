import listBuilder from "@/comps/list/listBuilder";

/*
lb.add("app","应用",80)
lb.add("name","名称",80)
lb.add("key","KEY",80)
lb.add("route","前端页面",80)
lb.add("db","数据模型",80)

*/
export default function () {
    let lb = listBuilder()
    lb.add("id","#",80)
    lb.addAction(120).setAlignRight()
    lb.add("app","应用",180)
    lb.add("name","名称",180)
    lb.add("key","KEY",280)
    lb.add("db","数据模型",180)
    lb.add("route","前端页面",0)
    return lb.headers
}