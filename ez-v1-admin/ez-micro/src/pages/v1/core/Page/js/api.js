import http from "@/assets/js/http";

export default {
    urlFind: '/core/admin/Page/info',
    urlFindToEdit: '/core/admin/Page/get',
    urlFindBy: '/core/admin/Page/list',
    urlSave: '/core/admin/Page/save',
    //urlDelete: '/core/admin/Page/delete',
    //urlDeleteAll: '/core/admin/Page/delete_many',
    urlDelete: '/core/admin/Page/destroy',
    urlDeleteAll: '/core/admin/Page/destroy_many',
    urlCopy: '/core/admin/Page/copy',
    urlEditMany: '/core/admin/Page/edit_many',
    urlTree: '/core/admin/Page/tree',
    urlChoice: '/core/admin/Page/choice',
    find(id, func) {
        http.get(this.urlFind, {id}, res => {
            func(res)
        })
    },
    findToEdit(id, func) {
        http.get(this.urlFindToEdit, {id}, res => {
            func(res)
        })
    },
    findBy(param, func) {
        http.post(this.urlFindBy, param, res => {
            func(res.data.lists, res.data.query)
        })
    },
    save(data, func) {
        http.post(this.urlSave, data, res => {
            func(res)
        })
    },
    delete(id, func) {
        http.get(this.urlDelete, {id}, res => {
            func(res)
        })
    },
    deleteAll(ids, func) {
        http.post(this.urlDeleteAll, {ids}, res => {
            func(res)
        })
    },
    copy(ids, func) {
        http.post(this.urlCopy, {ids}, res => {
            func(res)
        })
    },
    editMany(ids, data, func) {
        http.post(this.urlEditMany, {
            ids,
            _action: data
        }, res => {
            func(res)
        })
    },
}