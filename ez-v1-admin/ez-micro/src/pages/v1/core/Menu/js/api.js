import http from "@/assets/js/http";

export default {
    urlFind: '/core/admin/Menu/info',
    urlFindToEdit: '/core/admin/Menu/get',
    urlFindBy: '/core/admin/Menu/list',
    urlSave: '/core/admin/Menu/save',
    urlDelete: '/core/admin/Menu/delete',
    urlDeleteAll: '/core/admin/Menu/delete_many',
    urlCopy: '/core/admin/Menu/copy',
    urlEditMany: '/core/admin/Menu/edit_many',
    urlTree: '/core/admin/Menu/tree',
    urlChoice: '/core/admin/Menu/choice',
    urlExport: '/core/admin/Menu/export',
    urlImport: '/core/admin/Menu/import',
    import(content, func) {
        http.post(this.urlImport, {content}, res => {
            func(res)
        })
    },
    export(id, func) {
        http.post(this.urlExport, {id}, res => {
            func(res)
        })
    },
    find(id, func) {
        http.get(this.urlFind, {id}, res => {
            func(res)
        })
    },
    findToEdit(id, func) {
        http.get(this.urlFindToEdit, {id}, res => {
            func(res)
        })
    },
    findBy(param, func) {
        http.post(this.urlFindBy, param, res => {
            func(res.data.lists, res.data.query)
        })
    },
    save(data, func) {
        http.post(this.urlSave, data, res => {
            func(res)
        })
    },
    delete(id, func) {
        http.get(this.urlDelete, {id}, res => {
            func(res)
        })
    },
    deleteAll(ids, func) {
        http.post(this.urlDeleteAll, {ids}, res => {
            func(res)
        })
    },
    copy(ids, func) {
        http.post(this.urlCopy, {ids}, res => {
            func(res)
        })
    },
    editMany(ids, data, func) {
        http.post(this.urlEditMany, {
            ids,
            _action: data
        }, res => {
            func(res)
        })
    },
}