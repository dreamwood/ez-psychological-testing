import apiModel from "@/pages/v1/core/Model/js/apiModel";

export default {
    data(){
        return{
            models:[],
            curModelAppId:"",
        }
    },
    mounted() {
        this.getModels()
    },
    methods:{
        getModels(){
            apiModel.findBy({_order:["appId"]},res=>{
                this.models = res
            })
        }
    },
    props:{
        params:{},//列表初始且无法覆盖的筛选条件
        base:{},//数据新建的时候默认的值
    },
    watch:{
        curModelAppId(v){
            this.params = []
            if (v === ""){
                //啥也不干
            }else {
                this.params["app"] = v
            }
            this.getList()
        }
    }
}