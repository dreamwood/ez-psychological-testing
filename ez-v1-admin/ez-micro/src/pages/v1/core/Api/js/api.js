import http from "@/assets/js/http";

export default {
    urlFind: '/core/admin/Api/info',
    urlFindToEdit: '/core/admin/Api/get',
    urlFindBy: '/core/admin/Api/list',
    urlSave: '/core/admin/Api/save',
    urlDelete: '/core/admin/Api/delete',
    urlDeleteAll: '/core/admin/Api/delete_many',
    urlCopy: '/core/admin/Api/copy',
    urlEditMany: '/core/admin/Api/edit_many',
    urlTree: '/core/admin/Api/tree',
    urlChoice: '/core/admin/Api/choice',
    urlGroup: '/core/admin/Api/group',
    group(func) {
        http.get(this.urlGroup, {}, res => {
            func(res)
        })
    },
    find(id, func) {
        http.get(this.urlFind, {id}, res => {
            func(res)
        })
    },
    findToEdit(id, func) {
        http.get(this.urlFindToEdit, {id}, res => {
            func(res)
        })
    },
    findBy(param, func) {
        http.post(this.urlFindBy, param, res => {
            func(res.data.lists, res.data.query)
        })
    },
    save(data, func) {
        http.post(this.urlSave, data, res => {
            func(res)
        })
    },
    delete(id, func) {
        http.get(this.urlDelete,{id}, res => {
            func(res)
        })
    },
    deleteAll(ids, func) {
        http.post(this.urlDeleteAll, {ids}, res => {
            func(res)
        })
    },
    copy(ids, func) {
        http.post(this.urlCopy, {ids}, res => {
            func(res)
        })
    },
    editMany(ids, data, func) {
        http.post(this.urlEditMany, {
            ids,
            _action: data
        }, res => {
            func(res)
        })
    },
}