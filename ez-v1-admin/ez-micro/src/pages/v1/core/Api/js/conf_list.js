import listBuilder from "@/comps/list/listBuilder";

/*
lb.add("route","路由",80)
lb.add("app","应用",80)
lb.add("roles","允许角色",80)

*/
export default function () {
    let lb = listBuilder()
    lb.add("id","#",80)

    lb.add("app","应用",180)
    lb.add("name","名称",240)
    lb.add("route","路由",0)
    lb.add("isPublic","公开接口",120).setIsSwitch()
    lb.addAction(120).setAlignRight()
    return lb.headers
}