import http from "@/assets/js/http";

export default {
    urlFind: '/core/admin/Model/info',
    urlFindToEdit: '/core/admin/Model/get',
    urlFindBy: '/core/admin/Model/list',
    urlSave: '/core/admin/Model/save',
    urlDelete: '/core/admin/Model/delete',
    urlDeleteAll: '/core/admin/Model/delete_many',
    urlCopy: '/core/admin/Model/copy',
    urlEditMany: '/core/admin/Model/edit_many',
    urlTree: '/core/admin/Model/tree',
    urlChoice: '/core/admin/Model/choice',
    find(id, func) {
        http.get(this.urlFind, {id}, res => {
            func(res)
        })
    },
    findToEdit(id, func) {
        http.get(this.urlFindToEdit, {id}, res => {
            func(res)
        })
    },
    findBy(param, func) {
        http.post(this.urlFindBy, param, res => {
            func(res.data.lists, res.data.query)
        })
    },
    save(data, func) {
        http.post(this.urlSave, data, res => {
            func(res)
        })
    },
    delete(id, func) {
        http.get(this.urlDelete, {id}, res => {
            func(res)
        })
    },
    deleteAll(ids, func) {
        http.post(this.urlDeleteAll, {ids}, res => {
            func(res)
        })
    },
    copy(ids, func) {
        http.post(this.urlCopy, {ids}, res => {
            func(res)
        })
    },
    editMany(ids, data, func) {
        http.post(this.urlEditMany, {
            ids,
            _action: data
        }, res => {
            func(res)
        })
    },
}