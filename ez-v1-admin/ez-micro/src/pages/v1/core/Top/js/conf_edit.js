import formBuilder from "@/comps/form/formBuilder";
import api from "./api";
import apiModel from "@/pages/v1/core/Model/js/apiModel";
/*
fb.addText("model","所属模块",3)
fb.addText("name","名称",3)
fb.addText("url","URL",3)
fb.addText("icon","图标",3)
fb.addText("sort","排序",3)
fb.addText("l","L",3)
fb.addText("r","R",3)
fb.addText("level","Level",3)
fb.addText("link","link",3)
fb.addText("parent","父级菜单",3)
fb.addText("children","子菜单",3)

* */
export default {
    main:function () {
        let fb = formBuilder()
        fb.setLabelWidth(100)

        fb.addSelect("modelId","所属模块",3)
            .setApi(apiModel.urlChoice)
        fb.addCascade("parentId","父级菜单",3)
            .setApi(api.urlTree)
        fb.addText("name","名称",3)
        fb.addText("icon","图标",3)
        fb.addText("sort","排序",2).setTypeNumber()
        fb.addText("url","URL",10)
        return fb
    },
    tree:function () {
        let fb = formBuilder()
        fb.setLabelWidth(100)

        fb.addCascade("parentId").setLabel("上级分类").setSpan(3)
            .setApi(api.urlTree)

        fb.addText("name").setSpan(6).setLabel("分类名称")
            .setRules([fb.R.notNull("名称不能为空")])

        fb.addText("sort").setSpan(3).setLabel("排序").setTypeNumber()
        return fb
    },
    other:function () {
        let fb = formBuilder()
        fb.setLabelWidth(100).setLabelPosition("right")

        return fb
    }
}