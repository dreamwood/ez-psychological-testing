import listBuilder from "@/comps/list/listBuilder";

/*
lb.add("model","所属模型",80)
lb.add("name","字段名",80)
lb.add("type","数据类型",80)
lb.add("isRelation","是否",80)

*/
export default function () {
    let lb = listBuilder()
    lb.add("id","#",80)
    lb.addAction(120).setAlignRight()
    return lb.headers
}