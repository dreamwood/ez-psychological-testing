import listBuilder from "@/comps/list/listBuilder";

/*
lb.add("question","所属题目",80)
lb.add("sort","排序",80)
lb.add("title","选项内容",80)
lb.add("score","分数",80)
lb.add("analyze","选项分析",80)

*/
export default function () {
    let lb = listBuilder()
    // lb.add("questionId","所属题目",280)
    lb.add("sort","排序",110)
    lb.add("score","分数",110)
    lb.add("title","选项内容",380)
    lb.add("analyze","选项分析",0)
    lb.addAction(120).setAlignRight()
    lb.add("id","#",80)
    return lb.headers
}