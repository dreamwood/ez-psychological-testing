import formBuilder from "@/comps/form/formBuilder";
import api from "./api";
import apiTestCategory from "@/pages/v1/xl/TestCategory/js/apiTestCategory";
import apiTestType from "@/pages/v1/xl/TestType/js/apiTestType";
/*
fb.addText("title","问题标题",3)
fb.addText("short","简介",3)
fb.addText("introduction","详细书名",3)
fb.addText("note","备注",3)

* */
export default {
    main:function () {
        let fb = formBuilder()
        fb.setLabelWidth(100)
        fb.addFiles("cover","封面",12).setFileIsPic()
        fb.addRadio("catId","测试分类",6).setApi(apiTestCategory.urlChoice)
        fb.addRadio("typeId","解析类型",6).setApi(apiTestType.urlChoice)
        fb.addText("title","问题标题",12)
        fb.addText("short","简介",12).setTextMulti(true).setTextRows(5)
        fb.addText("introduction","详细说明",12).setTextMulti(true).setTextRows(7)
        fb.addText("note","备注",12).setTextMulti(true).setTextRows(5)

        return fb
    },
    tree:function () {
        let fb = formBuilder()
        fb.setLabelWidth(100)

        fb.addCascade("parentId").setLabel("上级分类").setSpan(3)
            .setApi(api.urlTree)

        fb.addText("name").setSpan(6).setLabel("分类名称")
            .setRules([fb.R.notNull("名称不能为空")])

        fb.addText("sort").setSpan(3).setLabel("排序").setTypeNumber()
        return fb
    },
    other:function () {
        let fb = formBuilder()
        fb.setLabelWidth(100).setLabelPosition("right")

        return fb
    }
}