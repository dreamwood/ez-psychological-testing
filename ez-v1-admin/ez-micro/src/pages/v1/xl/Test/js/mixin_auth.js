import ezAuth from "@/assets/js/ez-auth";

export default {
    data() {
        return {
            auth_add: ezAuth.reg("+xl.Test.add", "心理测试_新增"),//新增
            auth_copy: ezAuth.reg("+xl.Test.copy", "心理测试_复制"),//复制
            auth_edit: ezAuth.reg("+xl.Test.edit", "心理测试_编辑"),//编辑
            auth_del: ezAuth.reg("-xl.Test.del", "心理测试_删除"),//删除
            auth_view: ezAuth.reg("+xl.Test.view", "心理测试_查看"),//查看
        }
    },
    directives: {
        auth: {
            bind: ($el, binding, vnode, prevVnode) => {
                ezAuth.checkAllow(binding.value) ? $el.style.display = "normal" : $el.style.display = "none"
            }
        }
    }
}