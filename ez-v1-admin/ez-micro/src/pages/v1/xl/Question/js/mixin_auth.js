import ezAuth from "@/assets/js/ez-auth";

export default {
    data() {
        return {
            auth_add: ezAuth.reg("+xl.Question.add", "问题_新增"),//新增
            auth_copy: ezAuth.reg("+xl.Question.copy", "问题_复制"),//复制
            auth_edit: ezAuth.reg("+xl.Question.edit", "问题_编辑"),//编辑
            auth_del: ezAuth.reg("-xl.Question.del", "问题_删除"),//删除
            auth_view: ezAuth.reg("+xl.Question.view", "问题_查看"),//查看
        }
    },
    directives: {
        auth: {
            bind: ($el, binding, vnode, prevVnode) => {
                ezAuth.checkAllow(binding.value) ? $el.style.display = "normal" : $el.style.display = "none"
            }
        }
    }
}