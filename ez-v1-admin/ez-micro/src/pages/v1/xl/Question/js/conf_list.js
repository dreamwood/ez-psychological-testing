import listBuilder from "@/comps/list/listBuilder";

/*
lb.add("test","所属测试",80)
lb.add("title","问题标题",80)
lb.add("isMulti","是否多选",80)
lb.add("note","备注",80)

*/
export default function () {
    let lb = listBuilder()
    lb.add("id","#",80)
    lb.add("sort","题号",120)
    lb.add("test","所属测试",280).setListIsObject("title")
    lb.add("title","问题标题",0)
    lb.add("isMulti","是否多选",120).setListIsSwitch()
    lb.addAction(120).setAlignRight()
    return lb.headers
}