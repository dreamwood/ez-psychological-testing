import http from "@/assets/js/http";

export default {
    urlFind: '/xl/admin/TestCategory/get',
    urlFindToEdit: '/xl/admin/TestCategory/get',
    urlFindBy: '/xl/admin/TestCategory/list',
    urlSave: '/xl/admin/TestCategory/save',
    //urlDelete: '/xl/admin/TestCategory/delete',
    //urlDeleteAll: '/xl/admin/TestCategory/delete_many',
    urlDelete: '/xl/admin/TestCategory/destroy',
    urlDeleteAll: '/xl/admin/TestCategory/destroy_many',
    urlCopy: '/xl/admin/TestCategory/copy',
    urlEditMany: '/xl/admin/TestCategory/edit_many',
    urlTree: '/xl/admin/TestCategory/tree',
    urlChoice: '/xl/admin/TestCategory/choice',
    find(id, func) {
        http.get(this.urlFind, {id}, res => {
            func(res)
        })
    },
    findToEdit(id, func) {
        http.get(this.urlFindToEdit, {id}, res => {
            func(res)
        })
    },
    findBy(param, func) {
        http.post(this.urlFindBy, param, res => {
            func(res.data.lists, res.data.query)
        })
    },
    save(data, func) {
        http.post(this.urlSave, data, res => {
            func(res)
        })
    },
    delete(id, func) {
        http.get(this.urlDelete, {id}, res => {
            func(res)
        })
    },
    deleteAll(ids, func) {
        http.post(this.urlDeleteAll, {ids}, res => {
            func(res)
        })
    },
    copy(ids, func) {
        http.post(this.urlCopy, {ids}, res => {
            func(res)
        })
    },
    editMany(ids, data, func) {
        http.post(this.urlEditMany, {
            ids,
            _action: data
        }, res => {
            func(res)
        })
    },
}