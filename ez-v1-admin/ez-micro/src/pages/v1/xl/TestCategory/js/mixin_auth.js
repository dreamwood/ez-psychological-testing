import ezAuth from "@/assets/js/ez-auth";

export default {
    data() {
        return {
            auth_add: ezAuth.reg("+xl.TestCategory.add", "测试分类_新增"),//新增
            auth_copy: ezAuth.reg("+xl.TestCategory.copy", "测试分类_复制"),//复制
            auth_edit: ezAuth.reg("+xl.TestCategory.edit", "测试分类_编辑"),//编辑
            auth_del: ezAuth.reg("-xl.TestCategory.del", "测试分类_删除"),//删除
            auth_view: ezAuth.reg("+xl.TestCategory.view", "测试分类_查看"),//查看
        }
    },
    directives: {
        auth: {
            bind: ($el, binding, vnode, prevVnode) => {
                ezAuth.checkAllow(binding.value) ? $el.style.display = "normal" : $el.style.display = "none"
            }
        }
    }
}