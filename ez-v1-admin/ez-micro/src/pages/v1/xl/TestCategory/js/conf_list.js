import listBuilder from "@/comps/list/listBuilder";

/*
lb.add("name","分类名称",80)
lb.add("cover","封面",80)
lb.add("sort","排序",80)

*/
export default function () {
    let lb = listBuilder()
    lb.add("id","#",80)
    lb.addAction(120).setAlignRight()
    lb.add("name","分类名称",180)
    lb.add("cover","封面",280).setListIsPic()
    lb.add("sort","排序",180)
    return lb.headers
}