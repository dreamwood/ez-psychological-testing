import listBuilder from "@/comps/list/listBuilder";

/*
lb.add("question","所属题目",80)
lb.add("title","解析",80)
lb.add("scoreMin","分数下限",80)
lb.add("scoreMax","分数上限",80)

*/
export default function () {
    let lb = listBuilder()
    // lb.add("questionId","所属题目ID",110)
    lb.add("scoreMin","分数下限",110)
    lb.add("scoreMax","分数上限",110)
    lb.add("title","解析",0)
    lb.addAction(120).setAlignRight()
    lb.add("id","#",80)

    return lb.headers
}