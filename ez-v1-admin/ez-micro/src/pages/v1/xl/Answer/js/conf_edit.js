import formBuilder from "@/comps/form/formBuilder";
import api from "./api";
import apiQuestion from "@/pages/v1/xl/Question/js/apiQuestion";
/*
fb.addText("question","所属题目",3)
fb.addText("title","解析",3)
fb.addText("scoreMin","分数下限",3)
fb.addText("scoreMax","分数上限",3)

* */
export default {
    main:function () {
        let fb = formBuilder()
        fb.setLabelWidth(100)

        fb.addSearch("questionId","所属题目",12).setApi(apiQuestion.urlChoice)
        fb.addText("scoreMin","分数下限",3).setTextTypeNumber()
        fb.addText("scoreMax","分数上限",3).setTextTypeNumber()
        fb.addText("title","解析",12).setTextMulti(true).setRows(10)

        return fb
    },
    tree:function () {
        let fb = formBuilder()
        fb.setLabelWidth(100)

        fb.addCascade("parentId").setLabel("上级分类").setSpan(3)
            .setApi(api.urlTree)

        fb.addText("name").setSpan(6).setLabel("分类名称")
            .setRules([fb.R.notNull("名称不能为空")])

        fb.addText("sort").setSpan(3).setLabel("排序").setTypeNumber()
        return fb
    },
    other:function () {
        let fb = formBuilder()
        fb.setLabelWidth(100).setLabelPosition("right")

        return fb
    }
}