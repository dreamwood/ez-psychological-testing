import EzCommConfig from "@/comps/form/EzCommConfig";
export default function () {
    return{
        ...EzCommConfig(),
        type:"EzSwitch",
    }
}