export default {
    notNull(message){
        return { validate: (val) => !!val, message: message}
    },
    lenMustGt(len,message){
        return{ validate: (val) => val !== undefined && val.length >= len, message: message}
    },
    lenMustLt(len,message){
        return{ validate: (val) => val === undefined || val.length <= len, message: message}
    },
}
