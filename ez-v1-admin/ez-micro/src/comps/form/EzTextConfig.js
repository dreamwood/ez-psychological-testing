import EzCommConfig from "./EzCommConfig";
export default function () {
    return{
        ...EzCommConfig(),

        type:"EzText",

        editType:"text",
        setTypeNumber(){this.editType = 'number';return this},
        setTextTypeNumber(){this.editType = 'number';return this},

        multiLine:false,
        setMulti(multiLine){this.multiLine = multiLine;return this},
        setTextMulti(multiLine){this.multiLine = multiLine;return this},

        rows:1,
        setRows(rows){this.rows = rows;return this},
        setTextRows(rows){this.rows = rows;return this},

        maxLength:undefined,
        setMaxLength(maxLength){this.maxLength = maxLength;return this},
        setTextMaxLength(maxLength){this.maxLength = maxLength;return this},
    }
}