import EzCommConfig from "@/comps/form/EzCommConfig";
import EzCommApi from "@/comps/form/EzCommApi";
export default function () {
    return{
        ...EzCommConfig(),
        type:"EzSearch",

        choices:[],
        setChoices(choices){this.choices = choices;return this},
        addChoice(label,value){this.choices.push({label,value});return this},

        isMultiple:false,
        setIsMultiple(){this.isMultiple = true;return this},
        setSearchIsMultiple(){this.isMultiple = true;return this},

        inputPosition:"left",
        setSearchInputPositionRight(){this.inputPosition = "right";return this},

        ...EzCommApi(),
    }
}