import EzCommConfig from "@/comps/form/EzCommConfig";
import EzCommApi from "@/comps/form/EzCommApi";
export default function () {
    return{
        ...EzCommConfig(),
        type:"EzSelect",

        choices:[],
        setChoices(choices){this.choices = choices;return this},
        addChoice(label,value){this.choices.push({label,value});return this},

        search:false,
        setSelectSearch(canSearch){this.search = canSearch;return this},
        setSelectEnable(){this.search = true;return this},

         ...EzCommApi(),
    }
}