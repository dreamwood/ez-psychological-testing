package midleware

import (
	"gitee.com/dreamwood/ez-go/ez"
)

func init() {
	ez.AddPreparer("CORS", func(session *ez.Session) {
		session.Output.Response.Header().Set("Access-Control-Allow-Origin", "*")
		session.Output.Response.Header().Set("Access-Control-Allow-Headers", "Content-Type,AccessToken,X-CSRF-Token,Authorization,Set-Ez-Access-Token,Ez-Access-Token,Ez-Auth-Token")
		session.Output.Response.Header().Set("Access-Control-Expose-Headers", "Content-Length,Access-Control-Allow-Origin,Access-Control-Allow-Headers,Content-Type,Set-Ez-Access-Token")
		session.Output.Response.Header().Set("Access-Control-Allow-Credentials", "true")
		session.Output.Response.Header().Set("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS")
		if session.Input.Request.Method == "OPTIONS" {
			session.Stop()
		}
	})

}
