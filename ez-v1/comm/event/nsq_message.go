package event

import (
	"context"
	"gitee.com/dreamwood/ez-go/ez"
)

func SubScribePublicNsqMessage() {
	ez.Subscribe(ez.EventPublicKey, func(v interface{}, ctx context.Context) {
		data, ok := v.([]byte)
		if ok {
			ez.LogToConsole(string(data))
		}
	})
}

func SubScribePrivateNsqMessage() {
	ez.Subscribe(ez.EventPrivateKey, func(v interface{}, ctx context.Context) {
		data, ok := v.([]byte)
		if ok {
			ez.LogToConsole(string(data))
		}
	})
}
