package event

import (
	"context"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/task"
)

func init() {

	ez.Subscribe(ez.EventAfterServerRun, func(v interface{}, ctx context.Context) {
		ez.LogToConsoleNoTrace("****服务已启动****")
		//启动定时任务
		ez.LogToConsoleNoTrace("开始启动定时任务 Start")
		task.Start()
		ez.LogToConsoleNoTrace("定时任务启动完成 End")

		//执行服务信息注册
		ez.LogToConsoleNoTrace("开始注册服务信息 Start")
		ez.RegInfo()
		ez.LogToConsoleNoTrace("注册服务信息完成 End")

		//执行页面注册
		ez.LogToConsoleNoTrace("开始接口注册 Start")
		ez.RegApi()
		ez.LogToConsoleNoTrace("接口注册完成 End")

		//执行页面注册
		ez.LogToConsoleNoTrace("开始执行页面注册")
		//register.CreatePageConfigFile()
		//register.RegPages()

		//执行模块注册
		ez.LogToConsoleNoTrace("开始执行页面注册")
		//register.CreateModelConfigFile()
		//register.RegModels()

		SubScribePublicNsqMessage()
		SubScribePrivateNsqMessage()
	})
}
