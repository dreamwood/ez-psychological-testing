package task

import (
	"context"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/task"
)

func init() {
	ez.Subscribe(ez.EventAfterConfigReady, func(v interface{}, ctx context.Context) {
		task.NewTask("service_heart_beat", func() {
			ez.HeartBeat()
		}).SetEvery(ez.ConfigService.HeartBeatInterval)

		//hello public core TODO 正式环境应该删除，仅仅用于测试和当做DEMO使用
		//task.NewTask("hello_core—public", func() {
		//	e := ez.NsqProducer.Publish(
		//		ez.EzPublicTopic,
		//		[]byte("hello public core"))
		//	if e != nil {
		//		ez.LogToConsole(e.Error())
		//	}
		//}).SetEvery(60)
		//hello private core TODO 正式环境应该删除，仅仅用于测试和当做DEMO使用
		//task.NewTask("hello_core-private", func() {
		//	e := ez.NsqProducer.Publish(
		//		ez.ConfigService.AppId,
		//		[]byte("hello private core"))
		//	if e != nil {
		//		ez.LogToConsole(e.Error())
		//	}
		//}).SetEvery(60)
	})
}
