package main

import (
	_ "ez/apps"
	"gitee.com/dreamwood/ez-go/ez"
)

func main() {
	ez.Run()
}
