package ws

import (
	"context"
	"ez/config"
	"gitee.com/dreamwood/ez-go/ez"
	"net/http"
)

func Prepare() {
	hub := NewHub()
	http.HandleFunc("/ws/websocket", hub.Upgrade)
}

func init() {
	ez.Subscribe(ez.EventAfterConfigReady, func(v interface{}, ctx context.Context) {
		if config.ConfigWebSocket.Open {
			ez.LogToConsoleNoTrace("设置websocket")
			Prepare()
		}
	})
}
