package ws

import (
	"github.com/gorilla/websocket"
	"sync"
)

type Session struct {
	Id            string //SessionId
	User          string //用户的Token
	Conn          *websocket.Conn
	Closer        chan int
	HeartBeatTime int64
	Locker        sync.Mutex
}

func (s *Session) close() {
	s.Closer <- 0
}

func (hub *Hub) RegSession(session *Session) {
	hub.LK.Lock()
	hub.Sessions[session.Id] = session
	hub.LK.Unlock()
}

func (hub *Hub) RemoveSession(session *Session) {
	hub.LK.Lock()
	delete(hub.Sessions, session.Id)
	hub.LK.Unlock()
}

func (hub *Hub) GetSession(sessionId string) *Session {
	hub.LK.Lock()
	defer hub.LK.Unlock()
	session, ok := hub.Sessions[sessionId]
	if !ok {
		return nil
	}
	return session
}
