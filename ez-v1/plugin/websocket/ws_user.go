package ws

import (
	"fmt"
	"gitee.com/dreamwood/ez-go/ez"
	"github.com/gorilla/websocket"
	"time"
)

/**
使用map[string]*Session来存储临时的会话
使用redis 存储键值对 Set
指定用户发送
redisKey:
	UserSessions::__USER_TOKEN__ 存储用户所在的session,根据项目决定是否云讯一对多的存在

群组发送
redisKey:
	GroupSessions::__GROUP_KEY__ 存储组中所有的用户session

过期机制
//todo 暂时不使用短期过期机制，设置过期时间为24小时
*/

func CreateRdsKeyUserSessions(token string) string {
	return fmt.Sprintf("UserSessions:%s", token)
}

func (hub *Hub) RegUser(session *Session) {
	if session.User == "" {
		return
	}
	k := CreateRdsKeyUserSessions(session.User)
	//将SessionId 写入UserSessions::__USER_TOKEN__ Set中
	cmd := ez.GetRedis().SAdd(k, session.Id)
	if cmd.Err() != nil {
		ez.Debug(cmd.Err())
	}
	ez.GetRedis().Expire(k, time.Hour*12)
}

// checkUserExist 检查用户是否存在
func (hub *Hub) checkUserExist(session *Session) bool {
	if session.User == "" {
		return false
	}
	//检查user是否存在
	cmd := ez.GetRedis().SCard(CreateRdsKeyUserSessions(session.User))
	ez.JsonLog(cmd.String())
	return false
}

func GetUserSessions(token string) []string {
	cmd := ez.GetRedis().SMembers(CreateRdsKeyUserSessions(token))
	if cmd.Err() != nil {
		ez.Debug(cmd.Err())
		return nil
	}
	wanted := make([]string, 0)
	e := cmd.ScanSlice(&wanted)
	if e != nil {
		ez.Debug(e)
		return nil
	}
	return wanted
}

func RemoveSessionFromUser(session *Session) {
	if session.User == "" {
		return
	}
	cmd := ez.GetRedis().SRem(CreateRdsKeyUserSessions(session.User), session.Id)
	if cmd.Err() != nil {
		ez.Debug(cmd.Err())
	}
}

// checkSessionExist 检查session是否存在
func (hub *Hub) checkSessionExist(session *Session) bool {
	if session.Id == "" {
		return false
	}
	return false
}

func PushToUser(token string, data []byte) {
	sessionIds := GetUserSessions(token)
	for _, sessionId := range sessionIds {
		session := ezhub.GetSession(sessionId)
		if session != nil {
			err := session.Conn.WriteMessage(websocket.TextMessage, data)
			if err != nil {
				//断开的session 从全局中删除
				ezhub.RemoveSession(session)
			}
		} else {
			//无效的sessionId需要从用户列表中清除
			tmpSess := &Session{
				Id:   sessionId,
				User: token,
			}
			RemoveSessionFromUser(tmpSess)
		}
	}
}
