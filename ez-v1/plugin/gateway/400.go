package gateway

import (
	"ez/config/code"
	"ez/config/r"
	"gitee.com/dreamwood/ez-go/ez"
	"strings"
)

func init() {

	ez.CreateApi(r.R("/_server_404"), func(session *ez.Session) {
		Accept := session.Input.Request.Header.Get("Accept")
		if strings.ContainsAny(Accept, "application/json") {
			jsonData := make(map[string]interface{})
			jsonData["code"] = code.ErrorServer
			jsonData["message"] = "服务繁忙"
			jsonData["data"] = ""
			session.JsonOut(jsonData)
			return
		}
		session.Html("404")
	})
	ez.CreateApi(r.R("/_server_403"), func(session *ez.Session) {
		errCode := session.Get("code").IsString()
		ez.LogToConsoleNoTrace(errCode)
		Accept := session.Input.Request.Header.Get("Accept")
		if strings.ContainsAny(Accept, "application/json") {
			jsonData := make(map[string]interface{})
			switch errCode {
			case "TIME_OUT":
				jsonData["code"] = code.ErrorAuth
				jsonData["message"] = "请登录"
				jsonData["data"] = ""
			case "ACCESS_DENY":
				jsonData["code"] = code.ErrorAccess
				jsonData["message"] = "访问受限"
				jsonData["data"] = ""
			}
			session.JsonOut(jsonData)
			return
		}
		session.Html("403")
	})

}
