package config

import (
	"gitee.com/dreamwood/ez-go/tools"
)

type LoginConfig struct {
	LoginSuccessRedirect string `yaml:"login-success-redirect"`
}

var ConfigLogin *LoginConfig

func init() {
	tools.CreateConfigFromYml("./app.yaml", "login", &ConfigLogin)
}
