package config

import (
	"gitee.com/dreamwood/ez-go/tools"
)

type WebSocketConfig struct {
	Open bool `yaml:"open"`
	// HeartBeatInterval 心跳间隔，单位s 默认10s
	HeartBeatInterval int64 `yaml:"heart-beat-interval"`
	// HeartBeatTimeKill 心跳超时阈值，单位s 默认30s,超过这个时间则认为这个链接已经废弃，应该进行关闭相关的处理
	HeartBeatTimeKill int64 `yaml:"heart-beat-time-kill"`
	// HeartBeatTimeOut 心跳间隔超时阈值，单位s 默认10s,超过这个时间则应该主动发起心跳进行检测
	HeartBeatTimeOut int64 `yaml:"heart-beat-time-out"`

	//群发时，设定一个最大值，当群发超过这个最大值时，分批群发，并且适当延迟
	MaxBroadCastCount int `yaml:"max-broad-cast-count"` //群发阈值
	MaxBroadCastSleep int `yaml:"max-broad-cast-sleep"` //群发休眠值，单位毫秒，ms
}

var ConfigWebSocket *WebSocketConfig

func init() {
	tools.CreateConfigFromYml("./app.yaml", "websocket", &ConfigWebSocket)
}
