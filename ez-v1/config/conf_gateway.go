package config

import (
	"gitee.com/dreamwood/ez-go/tools"
)

type GateWayConfig struct {
	AccessControlDefault bool     `yaml:"access-control-default"`
	AccessControlDev     bool     `yaml:"access-control-dev"`
	Ignores              []string `yaml:"ignores"`
}

var ConfigGateWay *GateWayConfig

func init() {
	tools.CreateConfigFromYml("./app.yaml", "gateway", &ConfigGateWay)
}
