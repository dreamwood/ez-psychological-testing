package uc

import (
	"context"
	"ez/apps/uc/auto/mc"
	"fmt"
	"gitee.com/dreamwood/ez-go/db/mgo"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"go.mongodb.org/mongo-driver/mongo"
)

func (this *Ucache) MakeChoice() *ss.M {
	return &ss.M{
		"value": this.Id,
		"label": this.Id,
	}
}

type UcacheCrud struct {
	Factory *mgo.Factory
}

func NewUcacheCrud(args ...interface{}) *UcacheCrud {
	factory := mgo.NewFactory(args...)
	factory.SetDoc(&Ucache{})
	factory.SetArgus(mc.GetUcacheConfig())
	crud := &UcacheCrud{
		Factory: factory,
	}
	return crud
}
func (this *UcacheCrud) GetCollection() *mongo.Collection {
	return this.Factory.GetCollection()
}
func (this *UcacheCrud) FindId(id int64) (*Ucache, error) {
	md := new(Ucache)
	e := this.Factory.FindId(md, id)
	return md, e
}
func (this *UcacheCrud) FindBy(where ss.M, order []string, page int, limit int) ([]*Ucache, error) {
	list := make([]*Ucache, 0)
	qb := &ez.HttpQuery{
		Conditions: where,
		Order:      order,
		Page:       page,
		Limit:      limit,
	}
	e := this.Factory.FindBy(&list, qb)
	return list, e
}
func (this *UcacheCrud) FindOneBy(where ss.M, order []string) (*Ucache, error) {
	md := new(Ucache)
	qb := &ez.HttpQuery{
		Conditions: where,
		Order:      order,
	}
	e := this.Factory.FindOneBy(md, qb)
	return md, e
}
func neverUsed_Ucache_func() {
	//导入ss包
	a := ss.M{}
	fmt.Printf("%v", a)
	fmt.Printf("%v", context.TODO())
}
