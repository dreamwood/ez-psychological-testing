package controller

import (
	uc "ez/apps/uc/document"
	"ez/config/code"
	"ez/custom/cc"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ezc"
	"gitee.com/dreamwood/ez-go/ss"
)

type UcacheAutoController struct {
	ezc.BaseAdminController
}

func (c UcacheAutoController) AccessControl(session *ez.Session) {
	//session.StopHandle()
}
func (c UcacheAutoController) SaveAction(session *ez.Session) {
	this := cc.New(session)
	model := new(uc.Ucache)
	if this.Try(this.FillJson(model)) {
		return
	}
	if ac := uc.NewUcacheAccessControl(model, "save", session); !ac.Access {
		this.ReturnError(code.ErrorAccess, ac.Message, "")
		return
	}
	model.SetEvent(true)
	model.SetFactoryParams(session)
	if this.Try(model.Save()) {
		return
	}
	this.ReturnSuccess("OK", model)
}
func (c UcacheAutoController) GetAction(session *ez.Session) {
	this := cc.New(session)
	model, err := uc.NewUcacheCrud(session).FindId(this.Get("id").IsInt64())
	if this.Try(err) {
		return
	}
	if ac := uc.NewUcacheAccessControl(model, "get", session); !ac.Access {
		this.ReturnError(code.ErrorAccess, ac.Message, "")
		return
	}
	this.ReturnSuccess("OK", model)
}
func (c UcacheAutoController) ChoiceAction(session *ez.Session) {
	this := cc.New(session)
	this.GetHttpQuery()
	list := make([]*uc.Ucache, 0)
	err := uc.NewUcacheCrud(session).Factory.AggregateFind(&list, this.HttpQuery)
	if this.Try(err) {
		return
	}
	count, err := uc.NewUcacheCrud(session).Factory.AggregateCount(this.HttpQuery)
	if this.Try(err) {
		return
	}
	choices := make([]*ss.M, 0)
	for _, row := range list {
		choices = append(choices, row.MakeChoice())
	}
	this.ReturnSuccess("OK", ss.M{
		"lists": choices,
		"query": ss.M{"page": this.HttpQuery.Page, "limit": this.HttpQuery.Limit, "total": count},
	})
}
func (c UcacheAutoController) ListAction(session *ez.Session) {
	this := cc.New(session)
	this.GetHttpQuery()
	list := make([]*uc.Ucache, 0)
	err := uc.NewUcacheCrud(session).Factory.AggregateFind(&list, this.HttpQuery)
	if this.Try(err) {
		return
	}
	count, err := uc.NewUcacheCrud(session).Factory.AggregateCount(this.HttpQuery)
	if this.Try(err) {
		return
	}
	this.ReturnSuccess("ok", ss.M{
		"lists": list,
		"query": ss.M{"page": this.HttpQuery.Page, "limit": this.HttpQuery.Limit, "total": count},
	})
}
func (c UcacheAutoController) DeleteAction(session *ez.Session) {
	this := cc.New(session)
	model, err := uc.NewUcacheCrud(session).FindId(this.Get("id").IsInt64())
	if this.Try(err) {
		return
	}
	if ac := uc.NewUcacheAccessControl(model, "delete", session); !ac.Access {
		this.ReturnError(code.ErrorAccess, ac.Message, "")
		return
	}
	model.SetEvent(true)
	if this.Try(model.Delete()) {
		return
	}
	this.ReturnSuccess("OK", model)
}
func (c UcacheAutoController) UnDeleteAction(session *ez.Session) {
	this := cc.New(session)
	model, err := uc.NewUcacheCrud(session).FindId(this.Get("id").IsInt64())
	if this.Try(err) {
		return
	}
	model.SetEvent(true)
	if this.Try(model.UnDelete()) {
		return
	}
	this.ReturnSuccess("OK", model)
}
func (c UcacheAutoController) DeleteManyAction(session *ez.Session) {
	this := cc.New(session)
	ids := ss.NewIds()
	if this.Try(this.FillJson(ids)) {
		return
	}
	list, err := uc.NewUcacheCrud(session).FindBy(ss.M{
		"id__in": ids.Ids,
	}, nil, 0, 0)
	if this.Try(err) {
		return
	}
	for _, row := range list {
		row.SetEvent(true)
		if ac := uc.NewUcacheAccessControl(row, "delete", session); !ac.Access {
			this.ReturnError(code.ErrorAccess, ac.Message, "")
			return
		}
		if this.Try(row.Delete()) {
			return
		}
	}
	this.ReturnSuccess("OK", "删除成功")
}
func (c UcacheAutoController) DestroyAction(session *ez.Session) {
	this := cc.New(session)
	model, err := uc.NewUcacheCrud(session).FindId(this.Get("id").IsInt64())
	if this.Try(err) {
		return
	}
	model.SetEvent(true)
	if this.Try(model.Destroy()) {
		return
	}
	this.ReturnSuccess("OK", model)
}
func (c UcacheAutoController) DestroyManyAction(session *ez.Session) {
	this := cc.New(session)
	ids := ss.NewIds()
	if this.Try(this.FillJson(ids)) {
		return
	}
	list, err := uc.NewUcacheCrud(session).FindBy(ss.M{
		"id__in": ids.Ids,
	}, nil, 0, 0)
	if this.Try(err) {
		return
	}
	for _, row := range list {
		if ac := uc.NewUcacheAccessControl(row, "destroy", session); !ac.Access {
			this.ReturnError(code.ErrorAccess, ac.Message, "")
			return
		}
		row.SetEvent(true)
		if this.Try(row.Destroy()) {
			return
		}
	}
	this.ReturnSuccess("OK", "销毁成功")
}
func (c UcacheAutoController) CopyAction(session *ez.Session) {
	this := cc.New(session)
	idVo := new(ss.DocIds)
	if this.Try(this.FillJson(idVo)) {
		return
	}
	for _, id := range idVo.Ids {
		model, err := uc.NewUcacheCrud(session).FindId(id)
		if this.Try(err) {
			return
		}
		if ac := uc.NewUcacheAccessControl(model, "copy", session); !ac.Access {
			this.ReturnError(code.ErrorAccess, ac.Message, "")
			return
		}
		model.Id = 0
		model.SetEvent(true)
		if this.Try(model.Save()) {
			return
		}
	}
	this.ReturnSuccess("OK", idVo)
}
func (c UcacheAutoController) UpdateAction(session *ez.Session) {
	this := cc.New(session)
	updater := ss.NewDocUpdater()
	if this.Try(this.FillJson(updater)) {
		return
	}
	//id, _ := primitive.ObjectIDFromHex(updater.Id)
	doc := &uc.Ucache{Id: updater.Id}
	if ac := uc.NewUcacheAccessControl(doc, "update", session); !ac.Access {
		this.ReturnError(code.ErrorAccess, ac.Message, "")
		return
	}
	if this.Try(uc.NewUcacheCrud(session).Factory.Update(doc, updater.Model)) {
		return
	}
	this.ReturnSuccess("OK", updater)
}
func (c UcacheAutoController) UpdateManyAction(session *ez.Session) {
	this := cc.New(session)
	updater := ss.NewDocUpdater()
	if this.Try(this.FillJson(updater)) {
		return
	}
	for _, id := range updater.Ids {
		doc := &uc.Ucache{Id: id}
		if ac := uc.NewUcacheAccessControl(doc, "update", session); !ac.Access {
			this.ReturnError(code.ErrorAccess, ac.Message, "")
			return
		}
		if this.Try(uc.NewUcacheCrud(session).Factory.Update(doc, updater.Model)) {
			return
		}
	}
	this.ReturnSuccess("OK", updater)
}
