package db

import (
	"context"
	"encoding/json"
	uc "ez/apps/uc/document"
	"fmt"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"gitee.com/dreamwood/ez-go/tools"
	"go.mongodb.org/mongo-driver/bson"
	"os"
	"testing"
)

func DropUcache() {
	ez.DBMongo.Collection("Ucache").Drop(context.TODO())
}
func TestDropUcache(t *testing.T) {
	ez.DefaultInit()
	DropUcache()
}
func BackUpUcache(targetFile string) {
	if targetFile == "" {
		targetFile = "./Ucache.json"
	}
	ez.DefaultInit()
	//计数
	total, e := ez.DBMongo.Collection("Ucache").CountDocuments(context.TODO(), bson.M{})
	if e != nil {
		ez.LogToConsole(e.Error())
	}
	f, e := os.OpenFile(targetFile, os.O_CREATE|os.O_WRONLY|os.O_RDWR, 0666)
	if e != nil {
		ez.LogToConsole(e.Error())
	}
	defer f.Close()
	crud := uc.NewUcacheCrud()
	data := make([]uc.Ucache, 0)
	for i := 0; i <= int(total/1000); i++ {
		rows, e := crud.FindBy(ss.M{}, nil, i, 1000)
		if e != nil {
			ez.LogToConsole(e.Error())
		}
		for _, row := range rows {
			data = append(data, *row)
		}
	}
	content, e := json.Marshal(data)
	f.Write(content)
}
func TestBackUpUcache(t *testing.T) {
	ez.DefaultInit()
	targetFile := fmt.Sprintf("./Ucache_%s.json", tools.GetDateYMDHIS("", "", "_"))
	BackUpUcache(targetFile)
}
func RecoverUcache(filePath string) {
	if filePath == "" {
		filePath = "./Ucache.json"
	}
	ez.DefaultInit()
	ez.DBMongo.Collection("Ucache").Drop(context.TODO())
	content := tools.ReadFile(filePath)
	data := make([]uc.Ucache, 0)
	e := json.Unmarshal(content, &data)
	if e != nil {
		ez.LogToConsole(e.Error())
	}
	for _, row := range data {
		e = row.Create()
		if e != nil {
			ez.LogToConsole(e.Error())
		}
	}
	ez.LogToConsole(fmt.Sprintf("写入数据%d条", len(data)))
}
func TestRecoverUcache(t *testing.T) {
	filePath := "./Ucache_20240107_190550.json"
	ez.DefaultInit()
	RecoverUcache(filePath)
}
