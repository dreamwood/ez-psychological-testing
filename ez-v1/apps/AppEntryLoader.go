package app

//业务模块
import (
	_ "ez/apps/core"
	//用户缓存
	_ "ez/apps/uc"
	//开发助手
	_ "ez/apps/dev"
	//开发助手
	_ "ez/apps/xl"

	//网关模块
	_ "ez/plugin/gateway"
	// ftp文件服务
	_ "ez/plugin/ftp"

	//物联网模块
	_ "ez/apps/lot/env"
	// websocket模块
	_ "ez/plugin/websocket"
	// websocket模块
	_ "ez/plugin/request_log"
)
