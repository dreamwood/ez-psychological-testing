package env

import "fmt"

type Data struct {
	CO2       float64
	PM25      float64
	O2        float64
	TVOC      float64
	YANWU     float64
	WENDU     float64
	SHIDU     float64
	GUANGZHAO float64
	JIAQUAN   float64
	CO        float64
	CH4       float64
	ZAOYIN    float64
	PM10      float64
}
type DataCell struct {
	Label string
	Note  string
	Unit  string
	Value float64
	Rate  float64
}

func (this Data) GetArray() []*DataCell {
	return []*DataCell{
		{Label: "CO2", Note: "0~5000ppm", Unit: "ppm", Value: this.CO2, Rate: 0},
		{Label: "PM2.5", Note: "0~1000µg/m³", Unit: "µg/m³", Value: this.PM25, Rate: 0},
		{Label: "O2", Note: "0~30.0%vol", Unit: "vol%", Value: this.O2, Rate: 10},
		{Label: "TVOC", Note: "0~5.00µg/m³", Unit: "µg/m³", Value: this.TVOC, Rate: 100},
		{Label: "烟雾", Note: "0~2000ppm", Unit: "ppm", Value: this.YANWU, Rate: 0},
		{Label: "温度", Note: "-40.0~80.0℃", Unit: "℃", Value: this.WENDU, Rate: 10},
		{Label: "湿度", Note: "15~100.0%RH", Unit: "%RH", Value: this.SHIDU, Rate: 10},
		{Label: "光照度", Note: "0~2000Lux", Unit: "Lux", Value: this.GUANGZHAO, Rate: 0},
		{Label: "甲醛", Note: "0~2.00ppm", Unit: "ppm", Value: this.JIAQUAN, Rate: 100},
		{Label: "一氧化碳", Note: "0~500ppm", Unit: "ppm", Value: this.CO, Rate: 0},
		{Label: "CH4", Note: "0~100.0%LEL", Unit: "%LEL", Value: this.CH4, Rate: 10},
		{Label: "噪音", Note: "35.0~100.0dB", Unit: "dB", Value: this.ZAOYIN, Rate: 10},
		{Label: "PM10", Note: "0~2000µg/m³", Unit: "µg/m³", Value: this.PM10, Rate: 0},
	}
}

func (this Data) GetString() []string {
	data := this.GetArray()
	wanted := make([]string, 0)
	for _, v := range data {
		if v.Rate > 0 {
			v.Value /= v.Rate
		}
		wanted = append(wanted, fmt.Sprintf("%s:%g %s\t[%s]", v.Label, v.Value, v.Unit, v.Note))
	}
	return wanted
}
