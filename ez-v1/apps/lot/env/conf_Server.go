package env

import (
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/tools"
)

type TcpConfig struct {
	Port int    `yaml:"port"`
	Host string `yaml:"host"`
}

var tcpConfig *TcpConfig

func InitTcpConfig() {
	ez.LogToConsoleNoTrace("TcpConfig 开始初始化")
	tcpConfig = new(TcpConfig)
	tools.CreateConfigFromYml("./app.yaml", "tcp", &tcpConfig)
}

func GetTcpConfig() *TcpConfig {
	if tcpConfig == nil {
		InitTcpConfig()
	}
	return tcpConfig
}
