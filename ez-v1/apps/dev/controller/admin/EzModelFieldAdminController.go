package controller

import (
	"ez/apps/dev/auto/controller"
	"gitee.com/dreamwood/ez-go/ez"
)

type EzModelFieldController struct {
	controller.EzModelFieldAutoController
}

func init() {
	c := &EzModelFieldController{}
	c.SetRouteParam("/dev", "/admin", "EzModelField")
	ez.CreateApi(c.AdminGet(), c.GetAction).SetRouterName("[dev]模型字段_创建")
	ez.CreateApi(c.AdminList(), c.ListAction).SetRouterName("[dev]模型字段_列表")
	ez.CreateApi(c.AdminSave(), c.SaveAction).SetRouterName("[dev]模型字段_保存")
	ez.CreateApi(c.AdminCopy(), c.CopyAction).SetRouterName("[dev]模型字段_复制")
	ez.CreateApi(c.AdminUpdate(), c.UpdateAction).SetRouterName("[dev]模型字段_更新")
	ez.CreateApi(c.AdminChoice(), c.ChoiceAction).SetRouterName("[dev]模型字段_选项")
	ez.CreateApi(c.AdminDelete(), c.DeleteAction).SetRouterName("[dev]模型字段_删除")
	ez.CreateApi(c.AdminUnDelete(), c.UnDeleteAction).SetRouterName("[dev]模型字段_恢复")
	ez.CreateApi(c.AdminDestroy(), c.DestroyAction).SetRouterName("[dev]模型字段_销毁")
	ez.CreateApi(c.AdminUpdateMany(), c.UpdateManyAction).SetRouterName("[dev]模型字段_更新_批量")
	ez.CreateApi(c.AdminDeleteMany(), c.DeleteManyAction).SetRouterName("[dev]模型字段_删除_批量")
	ez.CreateApi(c.AdminDestroyMany(), c.DestroyManyAction).SetRouterName("[dev]模型字段_销毁_批量")
}

//func (c EzModelFieldController) AccessControl(session *ez.Session) { /* 在这里面重构 */ }
//func (c EzModelFieldController) GetAction(session *ez.Session) { /* 在这里面重构 */ }
//func (c EzModelFieldController) ListAction(session *ez.Session) { /* 在这里面重构 */ }
