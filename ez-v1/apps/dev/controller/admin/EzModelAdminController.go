package controller

import (
	"ez/apps/dev/auto/controller"
	"gitee.com/dreamwood/ez-go/ez"
)

type EzModelController struct {
	controller.EzModelAutoController
}

func init() {
	c := &EzModelController{}
	c.SetRouteParam("/dev", "/admin", "EzModel")
	ez.CreateApi(c.AdminGet(), c.GetAction).SetRouterName("[dev]数据模型_创建")
	ez.CreateApi(c.AdminList(), c.ListAction).SetRouterName("[dev]数据模型_列表")
	ez.CreateApi(c.AdminSave(), c.SaveAction).SetRouterName("[dev]数据模型_保存")
	ez.CreateApi(c.AdminCopy(), c.CopyAction).SetRouterName("[dev]数据模型_复制")
	ez.CreateApi(c.AdminUpdate(), c.UpdateAction).SetRouterName("[dev]数据模型_更新")
	ez.CreateApi(c.AdminChoice(), c.ChoiceAction).SetRouterName("[dev]数据模型_选项")
	ez.CreateApi(c.AdminDelete(), c.DeleteAction).SetRouterName("[dev]数据模型_删除")
	ez.CreateApi(c.AdminUnDelete(), c.UnDeleteAction).SetRouterName("[dev]数据模型_恢复")
	ez.CreateApi(c.AdminDestroy(), c.DestroyAction).SetRouterName("[dev]数据模型_销毁")
	ez.CreateApi(c.AdminUpdateMany(), c.UpdateManyAction).SetRouterName("[dev]数据模型_更新_批量")
	ez.CreateApi(c.AdminDeleteMany(), c.DeleteManyAction).SetRouterName("[dev]数据模型_删除_批量")
	ez.CreateApi(c.AdminDestroyMany(), c.DestroyManyAction).SetRouterName("[dev]数据模型_销毁_批量")
}

// func (c EzModelController) AccessControl(session *ez.Session) { /* 在这里面重构 */ }
// func (c EzModelController) GetAction(session *ez.Session) { /* 在这里面重构 */ }
// func (c EzModelController) ListAction(session *ez.Session) { /* 在这里面重构 */ }
