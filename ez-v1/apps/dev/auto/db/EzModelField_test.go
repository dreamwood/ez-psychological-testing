package db

import (
	"context"
	"encoding/json"
	dev "ez/apps/dev/document"
	"fmt"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"gitee.com/dreamwood/ez-go/tools"
	"go.mongodb.org/mongo-driver/bson"
	"os"
	"testing"
)

func DropEzModelField() {
	ez.DBMongo.Collection("EzModelField").Drop(context.TODO())
}
func TestDropEzModelField(t *testing.T) {
	ez.DefaultInit()
	DropEzModelField()
}
func BackUpEzModelField(targetFile string) {
	if targetFile == "" {
		targetFile = "./EzModelField.json"
	}
	ez.DefaultInit()
	//计数
	total, e := ez.DBMongo.Collection("EzModelField").CountDocuments(context.TODO(), bson.M{})
	if e != nil {
		ez.LogToConsole(e.Error())
	}
	f, e := os.OpenFile(targetFile, os.O_CREATE|os.O_WRONLY|os.O_RDWR, 0666)
	if e != nil {
		ez.LogToConsole(e.Error())
	}
	defer f.Close()
	crud := dev.NewEzModelFieldCrud()
	data := make([]dev.EzModelField, 0)
	for i := 0; i <= int(total/1000); i++ {
		rows, e := crud.FindBy(ss.M{}, nil, i, 1000)
		if e != nil {
			ez.LogToConsole(e.Error())
		}
		for _, row := range rows {
			data = append(data, *row)
		}
	}
	content, e := json.Marshal(data)
	f.Write(content)
}
func TestBackUpEzModelField(t *testing.T) {
	ez.DefaultInit()
	targetFile := fmt.Sprintf("./EzModelField_%s.json", tools.GetDateYMDHIS("", "", "_"))
	BackUpEzModelField(targetFile)
}
func RecoverEzModelField(filePath string) {
	if filePath == "" {
		filePath = "./EzModelField.json"
	}
	ez.DefaultInit()
	ez.DBMongo.Collection("EzModelField").Drop(context.TODO())
	content := tools.ReadFile(filePath)
	data := make([]dev.EzModelField, 0)
	e := json.Unmarshal(content, &data)
	if e != nil {
		ez.LogToConsole(e.Error())
	}
	for _, row := range data {
		e = row.Create()
		if e != nil {
			ez.LogToConsole(e.Error())
		}
	}
	ez.LogToConsole(fmt.Sprintf("写入数据%d条", len(data)))
}
func TestRecoverEzModelField(t *testing.T) {
	filePath := "./EzModelField_20240107_190550.json"
	ez.DefaultInit()
	RecoverEzModelField(filePath)
}
