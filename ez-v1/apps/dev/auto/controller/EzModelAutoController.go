package controller

import (
	dev "ez/apps/dev/document"
	"ez/config/code"
	"ez/custom/cc"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ezc"
	"gitee.com/dreamwood/ez-go/ss"
)

type EzModelAutoController struct {
	ezc.BaseAdminController
}

func (c EzModelAutoController) AccessControl(session *ez.Session) {
	//session.StopHandle()
}
func (c EzModelAutoController) SaveAction(session *ez.Session) {
	this := cc.New(session)
	model := new(dev.EzModel)
	if this.Try(this.FillJson(model)) {
		return
	}
	if ac := dev.NewEzModelAccessControl(model, "save", session); !ac.Access {
		this.ReturnError(code.ErrorAccess, ac.Message, "")
		return
	}
	model.SetEvent(true)
	model.SetFactoryParams(session)
	if this.Try(model.Save()) {
		return
	}
	this.ReturnSuccess("OK", model)
}
func (c EzModelAutoController) GetAction(session *ez.Session) {
	this := cc.New(session)
	model, err := dev.NewEzModelCrud(session).FindId(this.Get("id").IsInt64())
	if this.Try(err) {
		return
	}
	if ac := dev.NewEzModelAccessControl(model, "get", session); !ac.Access {
		this.ReturnError(code.ErrorAccess, ac.Message, "")
		return
	}
	this.ReturnSuccess("OK", model)
}
func (c EzModelAutoController) ChoiceAction(session *ez.Session) {
	this := cc.New(session)
	this.GetHttpQuery()
	list := make([]*dev.EzModel, 0)
	err := dev.NewEzModelCrud(session).Factory.AggregateFind(&list, this.HttpQuery)
	if this.Try(err) {
		return
	}
	count, err := dev.NewEzModelCrud(session).Factory.AggregateCount(this.HttpQuery)
	if this.Try(err) {
		return
	}
	choices := make([]*ss.M, 0)
	for _, row := range list {
		choices = append(choices, row.MakeChoice())
	}
	this.ReturnSuccess("OK", ss.M{
		"lists": choices,
		"query": ss.M{"page": this.HttpQuery.Page, "limit": this.HttpQuery.Limit, "total": count},
	})
}
func (c EzModelAutoController) ListAction(session *ez.Session) {
	this := cc.New(session)
	this.GetHttpQuery()
	list := make([]*dev.EzModel, 0)
	err := dev.NewEzModelCrud(session).Factory.AggregateFind(&list, this.HttpQuery)
	if this.Try(err) {
		return
	}
	count, err := dev.NewEzModelCrud(session).Factory.AggregateCount(this.HttpQuery)
	if this.Try(err) {
		return
	}
	this.ReturnSuccess("ok", ss.M{
		"lists": list,
		"query": ss.M{"page": this.HttpQuery.Page, "limit": this.HttpQuery.Limit, "total": count},
	})
}
func (c EzModelAutoController) DeleteAction(session *ez.Session) {
	this := cc.New(session)
	model, err := dev.NewEzModelCrud(session).FindId(this.Get("id").IsInt64())
	if this.Try(err) {
		return
	}
	if ac := dev.NewEzModelAccessControl(model, "delete", session); !ac.Access {
		this.ReturnError(code.ErrorAccess, ac.Message, "")
		return
	}
	model.SetEvent(true)
	if this.Try(model.Delete()) {
		return
	}
	this.ReturnSuccess("OK", model)
}
func (c EzModelAutoController) UnDeleteAction(session *ez.Session) {
	this := cc.New(session)
	model, err := dev.NewEzModelCrud(session).FindId(this.Get("id").IsInt64())
	if this.Try(err) {
		return
	}
	model.SetEvent(true)
	if this.Try(model.UnDelete()) {
		return
	}
	this.ReturnSuccess("OK", model)
}
func (c EzModelAutoController) DeleteManyAction(session *ez.Session) {
	this := cc.New(session)
	ids := ss.NewIds()
	if this.Try(this.FillJson(ids)) {
		return
	}
	list, err := dev.NewEzModelCrud(session).FindBy(ss.M{
		"id__in": ids.Ids,
	}, nil, 0, 0)
	if this.Try(err) {
		return
	}
	for _, row := range list {
		row.SetEvent(true)
		if ac := dev.NewEzModelAccessControl(row, "delete", session); !ac.Access {
			this.ReturnError(code.ErrorAccess, ac.Message, "")
			return
		}
		if this.Try(row.Delete()) {
			return
		}
	}
	this.ReturnSuccess("OK", "删除成功")
}
func (c EzModelAutoController) DestroyAction(session *ez.Session) {
	this := cc.New(session)
	model, err := dev.NewEzModelCrud(session).FindId(this.Get("id").IsInt64())
	if this.Try(err) {
		return
	}
	model.SetEvent(true)
	if this.Try(model.Destroy()) {
		return
	}
	this.ReturnSuccess("OK", model)
}
func (c EzModelAutoController) DestroyManyAction(session *ez.Session) {
	this := cc.New(session)
	ids := ss.NewIds()
	if this.Try(this.FillJson(ids)) {
		return
	}
	list, err := dev.NewEzModelCrud(session).FindBy(ss.M{
		"id__in": ids.Ids,
	}, nil, 0, 0)
	if this.Try(err) {
		return
	}
	for _, row := range list {
		if ac := dev.NewEzModelAccessControl(row, "destroy", session); !ac.Access {
			this.ReturnError(code.ErrorAccess, ac.Message, "")
			return
		}
		row.SetEvent(true)
		if this.Try(row.Destroy()) {
			return
		}
	}
	this.ReturnSuccess("OK", "销毁成功")
}
func (c EzModelAutoController) CopyAction(session *ez.Session) {
	this := cc.New(session)
	idVo := new(ss.DocIds)
	if this.Try(this.FillJson(idVo)) {
		return
	}
	for _, id := range idVo.Ids {
		model, err := dev.NewEzModelCrud(session).FindId(id)
		if this.Try(err) {
			return
		}
		if ac := dev.NewEzModelAccessControl(model, "copy", session); !ac.Access {
			this.ReturnError(code.ErrorAccess, ac.Message, "")
			return
		}
		model.Id = 0
		model.SetEvent(true)
		if this.Try(model.Save()) {
			return
		}
	}
	this.ReturnSuccess("OK", idVo)
}
func (c EzModelAutoController) UpdateAction(session *ez.Session) {
	this := cc.New(session)
	updater := ss.NewDocUpdater()
	if this.Try(this.FillJson(updater)) {
		return
	}
	//id, _ := primitive.ObjectIDFromHex(updater.Id)
	doc := &dev.EzModel{Id: updater.Id}
	if ac := dev.NewEzModelAccessControl(doc, "update", session); !ac.Access {
		this.ReturnError(code.ErrorAccess, ac.Message, "")
		return
	}
	if this.Try(dev.NewEzModelCrud(session).Factory.Update(doc, updater.Model)) {
		return
	}
	this.ReturnSuccess("OK", updater)
}
func (c EzModelAutoController) UpdateManyAction(session *ez.Session) {
	this := cc.New(session)
	updater := ss.NewDocUpdater()
	if this.Try(this.FillJson(updater)) {
		return
	}
	for _, id := range updater.Ids {
		doc := &dev.EzModel{Id: id}
		if ac := dev.NewEzModelAccessControl(doc, "update", session); !ac.Access {
			this.ReturnError(code.ErrorAccess, ac.Message, "")
			return
		}
		if this.Try(dev.NewEzModelCrud(session).Factory.Update(doc, updater.Model)) {
			return
		}
	}
	this.ReturnSuccess("OK", updater)
}
