package mc

import (
	"gitee.com/dreamwood/ez-go/db/mgo"
)

const (
	EzModelFieldEventNew           = "dev.EzModelFieldNew"
	EzModelFieldEventBeforeCreate  = "dev.EzModelFieldBeforeCreate"
	EzModelFieldEventBeforeUpdate  = "dev.EzModelFieldBeforeUpdate"
	EzModelFieldEventBeforeSave    = "dev.EzModelFieldBeforeCreate dev.EzModelFieldBeforeUpdate"
	EzModelFieldEventAfterCreate   = "dev.EzModelFieldAfterCreate"
	EzModelFieldEventAfterUpdate   = "dev.EzModelFieldAfterUpdate"
	EzModelFieldEventAfterSave     = "dev.EzModelFieldAfterCreate dev.EzModelFieldAfterUpdate"
	EzModelFieldEventDelete        = "dev.EzModelFieldDelete"
	EzModelFieldAccessControlEvent = "dev.EzModelFieldAccessControl"
)

func GetEzModelFieldConfig() *mgo.DocConfig {
	return EzModelField_Config
}

var EzModelField_Config *mgo.DocConfig

func init() {
	EzModelField_Config = NewEzModelFieldConfig()
}
func NewEzModelFieldConfig() *mgo.DocConfig {
	return &mgo.DocConfig{
		ContainerKey: "dev.EzModelField",
		Fields: []string{
			"model", "name", "cnName", "type", "isRelation", "src",
		},
		RelationFields: []string{
			"model",
		},
		RelationConfigs: map[string]*mgo.DocRelation{
			"model": {
				Config:     GetEzModelConfig,
				DocName:    "EzModel",
				JoinType:   "O",
				KeyInside:  "modelId",
				KeyOutSide: "id",
			},
		},
		FieldFilter: map[string]*mgo.DocFieldFilter{
			"default": {
				Select: []string{},
				Omit:   []string{},
			},
		},
	}
}
