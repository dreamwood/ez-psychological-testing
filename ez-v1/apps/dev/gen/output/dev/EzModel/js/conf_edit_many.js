import formBuilder from "@/comps/form/formBuilder";

export default function () {
    let fb = formBuilder().setLabelPosition("right").setLabelWidth(100)
    fb.addText("appId").setSpan(3).setLabel("AppId")
    return fb
}