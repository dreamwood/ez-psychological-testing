import http from "@/assets/js/http";

export default {
    urlFind: '/dev/admin/EzModelField/get',
    urlFindToEdit: '/dev/admin/EzModelField/get',
    urlFindBy: '/dev/admin/EzModelField/list',
    urlSave: '/dev/admin/EzModelField/save',
    //urlDelete: '/dev/admin/EzModelField/delete',
    //urlDeleteAll: '/dev/admin/EzModelField/delete_many',
    urlDelete: '/dev/admin/EzModelField/destroy',
    urlDeleteAll: '/dev/admin/EzModelField/destroy_many',
    urlCopy: '/dev/admin/EzModelField/copy',
    urlEditMany: '/dev/admin/EzModelField/edit_many',
    urlTree: '/dev/admin/EzModelField/tree',
    urlChoice: '/dev/admin/EzModelField/choice',
    find(id, func) {
        http.get(this.urlFind, {id}, res => {
            func(res)
        })
    },
    findToEdit(id, func) {
        http.get(this.urlFindToEdit, {id}, res => {
            func(res)
        })
    },
    findBy(param, func) {
        http.post(this.urlFindBy, param, res => {
            func(res.data.lists, res.data.query)
        })
    },
    save(data, func) {
        http.post(this.urlSave, data, res => {
            func(res)
        })
    },
    delete(id, func) {
        http.get(this.urlDelete, {id}, res => {
            func(res)
        })
    },
    deleteAll(ids, func) {
        http.post(this.urlDeleteAll, {ids}, res => {
            func(res)
        })
    },
    copy(ids, func) {
        http.post(this.urlCopy, {ids}, res => {
            func(res)
        })
    },
    editMany(ids, data, func) {
        http.post(this.urlEditMany, {
            ids,
            _action: data
        }, res => {
            func(res)
        })
    },
}