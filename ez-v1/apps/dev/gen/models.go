package gen

import "gitee.com/dreamwood/ez-go/maker"

func EzModel() {
	doc := maker.CreateDoc("EzModel", "数据模型", "dev")
	doc.Add("app", "APP").IsString()
	doc.Add("dir", "域").IsString()
	doc.Add("name", "名称").IsString()
	doc.Add("cnName", "中文名称").IsString()
	doc.Add("fields", "字段").IsJoinO2M("EzModelField", "id", "modelId")
	doc.Generate()
}
func EzModelField() {
	doc := maker.CreateDoc("EzModelField", "模型字段", "dev")
	doc.Add("model", "所属模型").IsJoinM2O("EzModel")
	doc.Add("name", "字段名").IsString()
	doc.Add("cnName", "中文名称").IsString()
	doc.Add("type", "数据类型").IsString()
	doc.Add("isRelation", "是否").IsBool()
	doc.Add("src", "关联模型").IsString()
	doc.Generate()
}
