package dev

import (
	"context"
	"ez/apps/dev/auto/mc"
	"fmt"
	"gitee.com/dreamwood/ez-go/db/mgo"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"go.mongodb.org/mongo-driver/mongo"
)

func (this *EzModel) MakeChoice() *ss.M {
	return &ss.M{
		"value": this.Id,
		"label": this.Id,
	}
}

type EzModelCrud struct {
	Factory *mgo.Factory
}

func NewEzModelCrud(args ...interface{}) *EzModelCrud {
	factory := mgo.NewFactory(args...)
	factory.SetDoc(&EzModel{})
	factory.SetArgus(mc.GetEzModelConfig())
	crud := &EzModelCrud{
		Factory: factory,
	}
	return crud
}
func (this *EzModelCrud) GetCollection() *mongo.Collection {
	return this.Factory.GetCollection()
}
func (this *EzModelCrud) FindId(id int64) (*EzModel, error) {
	md := new(EzModel)
	e := this.Factory.FindId(md, id)
	return md, e
}
func (this *EzModelCrud) FindBy(where ss.M, order []string, page int, limit int) ([]*EzModel, error) {
	list := make([]*EzModel, 0)
	qb := &ez.HttpQuery{
		Conditions: where,
		Order:      order,
		Page:       page,
		Limit:      limit,
	}
	e := this.Factory.FindBy(&list, qb)
	return list, e
}
func (this *EzModelCrud) FindOneBy(where ss.M, order []string) (*EzModel, error) {
	md := new(EzModel)
	qb := &ez.HttpQuery{
		Conditions: where,
		Order:      order,
	}
	e := this.Factory.FindOneBy(md, qb)
	return md, e
}
func neverUsed_EzModel_func() {
	//导入ss包
	a := ss.M{}
	fmt.Printf("%v", a)
	fmt.Printf("%v", context.TODO())
}
