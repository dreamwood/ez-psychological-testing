package dev

import (
	"context"
	"encoding/json"
	"ez/apps/dev/auto/mc"
	"gitee.com/dreamwood/ez-go/db/mgo"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"time"
)

type EzModelField struct {
	mgo.BaseDoc `bson:"-" json:"-"`
	Id          int64      `json:"id" bson:"id,omitempty"`
	Model       *EzModel   `json:"model" bson:"model"`           //所属模型
	ModelId     int64      `json:"modelId" bson:"modelId"`       //所属模型
	Name        string     `json:"name" bson:"name"`             //字段名
	CnName      string     `json:"cnName" bson:"cnName"`         //中文名称
	Type        string     `json:"type" bson:"type"`             //数据类型
	IsRelation  bool       `json:"isRelation" bson:"isRelation"` //是否
	Src         string     `json:"src" bson:"src"`               //关联模型
	CreateAt    time.Time  `json:"createAt" bson:"createAt"`
	UpdateAt    time.Time  `json:"updateAt" bson:"updateAt"`
	DeleteAt    *time.Time `json:"deleteAt" bson:"deleteAt"`
}

func (this *EzModelField) DocName() string { return "EzModelField" }
func (this *EzModelField) GetId() int64    { return this.Id }
func (this *EzModelField) SetId(id int64)  { this.Id = id }
func (this *EzModelField) Create() error {
	return this.GetFactory().Create(this)
}
func (this *EzModelField) Replace() error {
	return this.GetFactory().Replace(this)
}
func (this *EzModelField) Save() error {
	if this.Id == 0 {
		this.CreateAt = time.Now()
		this.UpdateAt = time.Now()
		return this.GetFactory().Create(this)
	} else {
		this.UpdateAt = time.Now()
		return this.GetFactory().Replace(this)
	}
}

// 伪删除
func (this *EzModelField) Delete() error {
	return this.GetFactory().Delete(this)
}
func (this *EzModelField) UnDelete() error {
	return this.GetFactory().UnDelete(this)
}

// 真删除
func (this *EzModelField) Destroy() error {
	return this.GetFactory().Destroy(this)
}
func (this *EzModelField) ToString() string {
	return string(this.ToBytes())
}
func (this *EzModelField) ToBytes() []byte {
	data, e := json.Marshal(this)
	ez.Try(e)
	return data
}
func (this *EzModelField) Serialize() []byte {
	data, e := json.Marshal(this)
	ez.Try(e)
	return data
}
func (this *EzModelField) UnSerialize(data []byte) []byte {
	ez.Try(json.Unmarshal(data, this))
	return data
}
func (this *EzModelField) LoadModel() {
	if this.ModelId == 0 {
		return
	}
	this.Model, _ = NewEzModelCrud().FindId(this.ModelId)
}
func (this *EzModelField) ClearRelationsBeforeSave() mgo.Doc {
	this.Model = nil
	return this
}
func neverUsed_EzModelField() {
	//导入ss包
	a := ss.M{}
	ez.Debug(a)
}

type EzModelFieldAccessControl struct {
	Access  bool
	Message string
	Action  string //控制器Action,小写开头，如c,u,r,d
	Model   *EzModelField
	Session *ez.Session
}

func NewEzModelFieldAccessControl(model *EzModelField, action string, session *ez.Session) *EzModelFieldAccessControl {
	ctrl := &EzModelFieldAccessControl{
		Access:  true,
		Model:   model,
		Action:  action,
		Session: session,
	}
	ez.DispatchToMany(mc.EzModelFieldAccessControlEvent, ctrl, context.TODO())
	return ctrl
}
