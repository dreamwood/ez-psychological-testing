package service

import (
	core "ez/apps/core/document"
	"gitee.com/dreamwood/ez-go/ss"
)

func GetMyModel(uid int64) ([]*core.Model, error) {
	crudUser := core.NewUserCrud()
	user, err := crudUser.FindId(uid)
	if err != nil {
		return nil, err
	}
	user.LoadRoles()
	ids := []int64{}
	for _, role := range user.Roles {
		ids = append(ids, role.ModelIds...)
	}
	crudModel := core.NewModelCrud()
	cond := ss.M{"isOn": true}
	if uid > 1 {
		cond["id__in"] = ids
	}

	return crudModel.FindBy(cond, []string{"l"}, 0, 0)
}

func GetMyTop(uid int64) ([]*core.Top, error) {
	crudUser := core.NewUserCrud()
	user, err := crudUser.FindId(uid)
	if err != nil {
		return nil, err
	}
	user.LoadRoles()
	ids := []int64{}
	for _, role := range user.Roles {
		ids = append(ids, role.TopIds...)
	}
	crudTop := core.NewTopCrud()
	cond := ss.M{}
	if uid > 1 {
		cond["id__in"] = ids
	}

	rows, e := crudTop.FindBy(cond, []string{"l"}, 0, 0)
	return RebuildTops(rows), e
}

func RebuildTops(tops []*core.Top) []*core.Top {
	//find := false
	var maxLevel int64 = 0
	var minLevel int64 = 99
	for _, top := range tops {
		if top.Level > maxLevel {
			maxLevel = top.Level
		}
		if top.Level < minLevel {
			minLevel = top.Level
		}
	}
	if maxLevel == minLevel {
		return tops
	}
	return fillTopChildren(tops, 0)
}
func fillTopChildren(tops []*core.Top, parentId int64) []*core.Top {
	children := make([]*core.Top, 0)
	for _, top := range tops {
		if top.ParentId == parentId {
			top.LoadModel()
			top.Children = fillTopChildren(tops, top.Id)
			children = append(children, top)
		}
	}
	return children
}

func GetMyMenu(uid int64) ([]*core.Menu, error) {
	crudUser := core.NewUserCrud()
	user, err := crudUser.FindId(uid)
	if err != nil {
		return nil, err
	}
	user.LoadRoles()
	ids := []int64{}
	for _, role := range user.Roles {
		ids = append(ids, role.MenuIds...)
	}
	crudMenu := core.NewMenuCrud()

	cond := ss.M{}
	if uid > 1 {
		cond["id__in"] = ids
	}

	rows, e := crudMenu.FindBy(cond, []string{"l"}, 0, 0)
	return RebuildMenus(rows), e
}

func RebuildMenus(tops []*core.Menu) []*core.Menu {
	//find := false
	var maxLevel int64 = 0
	var minLevel int64 = 99
	for _, top := range tops {
		if top.Level > maxLevel {
			maxLevel = top.Level
		}
		if top.Level < minLevel {
			minLevel = top.Level
		}
	}
	if maxLevel == minLevel {
		return tops
	}
	return fillMenuChildren(tops, 0)
}
func fillMenuChildren(tops []*core.Menu, parentId int64) []*core.Menu {
	children := make([]*core.Menu, 0)
	for _, top := range tops {

		if top.ParentId == parentId {
			top.LoadModel()
			top.Children = fillMenuChildren(tops, top.Id)
			children = append(children, top)
		}
	}
	return children
}

func GetMyPage(uid int64) map[string]bool {
	crudUser := core.NewUserCrud()
	pagesMap := make(map[string]bool)
	if uid == 1 {
		allPages, _ := core.NewPageCrud().FindBy(ss.M{}, nil, 1, 0)
		for _, page := range allPages {
			pagesMap[page.Key] = true
		}
		return pagesMap
	}
	user, err := crudUser.FindId(uid)
	if err != nil {
		return nil
	}
	user.LoadRoles()
	for _, role := range user.Roles {
		for _, key := range role.PageAllowKeys {
			_, ok := pagesMap[key]
			if !ok {
				pagesMap[key] = true
			}
		}
		for _, key := range role.PageDenyKeys {
			pagesMap[key] = false
		}
	}
	return pagesMap
}
