package core

import (
	"context"
	"ez/apps/core/auto/mc"
	"fmt"
	"gitee.com/dreamwood/ez-go/db/mgo"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"go.mongodb.org/mongo-driver/mongo"
)

func (this *Route) MakeChoice() *ss.M {
	return &ss.M{"value": this.Id, "label": this.Id}
}

type RouteCrud struct {
	Factory *mgo.Factory
}

func NewRouteCrud(args ...interface{}) *RouteCrud {
	factory := mgo.NewFactory(args...)
	factory.SetDoc(&Route{})
	factory.SetArgus(mc.GetRouteConfig())
	crud := &RouteCrud{
		Factory: factory,
	}
	return crud
}
func (this *RouteCrud) GetCollection() *mongo.Collection {
	return this.Factory.GetCollection()
}
func (this *RouteCrud) FindId(id int64) (*Route, error) {
	md := new(Route)
	e := this.Factory.FindId(md, id)
	return md, e
}
func (this *RouteCrud) FindBy(where ss.M, order []string, page int, limit int) ([]*Route, error) {
	list := make([]*Route, 0)
	qb := &ez.HttpQuery{
		Conditions: where,
		Order:      order,
		Page:       page,
		Limit:      limit,
	}
	e := this.Factory.FindBy(&list, qb)
	return list, e
}
func (this *RouteCrud) FindOneBy(where ss.M, order []string) (*Route, error) {
	md := new(Route)
	qb := &ez.HttpQuery{
		Conditions: where,
		Order:      order,
	}
	e := this.Factory.FindOneBy(md, qb)
	return md, e
}
func neverUsed_Route_func() {
	//导入ss包
	a := ss.M{}
	fmt.Printf("%v", a)
	fmt.Printf("%v", context.TODO())
}
