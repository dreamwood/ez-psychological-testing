package core

import (
	"context"
	"ez/apps/core/auto/mc"
	"fmt"
	"gitee.com/dreamwood/ez-go/db/mgo"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"go.mongodb.org/mongo-driver/mongo"
)

func (this *GateWay) MakeChoice() *ss.M {
	return &ss.M{"value": this.Id, "label": this.Port}
}

type GateWayCrud struct {
	Factory *mgo.Factory
}

func NewGateWayCrud(args ...interface{}) *GateWayCrud {
	factory := mgo.NewFactory(args...)
	factory.SetDoc(&GateWay{})
	factory.SetArgus(mc.GetGateWayConfig())
	crud := &GateWayCrud{
		Factory: factory,
	}
	return crud
}
func (this *GateWayCrud) GetCollection() *mongo.Collection {
	return this.Factory.GetCollection()
}
func (this *GateWayCrud) FindId(id int64) (*GateWay, error) {
	md := new(GateWay)
	e := this.Factory.FindId(md, id)
	return md, e
}
func (this *GateWayCrud) FindBy(where ss.M, order []string, page int, limit int) ([]*GateWay, error) {
	list := make([]*GateWay, 0)
	qb := &ez.HttpQuery{
		Conditions: where,
		Order:      order,
		Page:       page,
		Limit:      limit,
	}
	e := this.Factory.FindBy(&list, qb)
	return list, e
}
func (this *GateWayCrud) FindOneBy(where ss.M, order []string) (*GateWay, error) {
	md := new(GateWay)
	qb := &ez.HttpQuery{
		Conditions: where,
		Order:      order,
	}
	e := this.Factory.FindOneBy(md, qb)
	return md, e
}
func neverUsed_GateWay_func() {
	//导入ss包
	a := ss.M{}
	fmt.Printf("%v", a)
	fmt.Printf("%v", context.TODO())
}
