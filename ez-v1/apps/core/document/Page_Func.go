package core

import (
	"context"
	"ez/apps/core/auto/mc"
	"fmt"
	"gitee.com/dreamwood/ez-go/db/mgo"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"go.mongodb.org/mongo-driver/mongo"
)

func (this *Page) MakeChoice() *ss.M {
	return &ss.M{
		"value": this.Key,
		"label": this.Name,
	}
}

type PageCrud struct {
	Factory *mgo.Factory
}

func NewPageCrud(args ...interface{}) *PageCrud {
	factory := mgo.NewFactory(args...)
	factory.SetDoc(&Page{})
	factory.SetArgus(mc.GetPageConfig())
	crud := &PageCrud{
		Factory: factory,
	}
	return crud
}
func (this *PageCrud) GetCollection() *mongo.Collection {
	return this.Factory.GetCollection()
}
func (this *PageCrud) FindId(id int64) (*Page, error) {
	md := new(Page)
	e := this.Factory.FindId(md, id)
	return md, e
}
func (this *PageCrud) FindBy(where ss.M, order []string, page int, limit int) ([]*Page, error) {
	list := make([]*Page, 0)
	qb := &ez.HttpQuery{
		Conditions: where,
		Order:      order,
		Page:       page,
		Limit:      limit,
	}
	e := this.Factory.FindBy(&list, qb)
	return list, e
}
func (this *PageCrud) FindOneBy(where ss.M, order []string) (*Page, error) {
	md := new(Page)
	qb := &ez.HttpQuery{
		Conditions: where,
		Order:      order,
	}
	e := this.Factory.FindOneBy(md, qb)
	return md, e
}
func neverUsed_Page_func() {
	//导入ss包
	a := ss.M{}
	fmt.Printf("%v", a)
	fmt.Printf("%v", context.TODO())
}
