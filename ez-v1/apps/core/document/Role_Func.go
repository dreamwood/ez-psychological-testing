package core

import (
	"context"
	"ez/apps/core/auto/mc"
	"fmt"
	"gitee.com/dreamwood/ez-go/db/mgo"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"go.mongodb.org/mongo-driver/mongo"
)

func (this *Role) MakeChoice() *ss.M {
	return &ss.M{"value": this.Id, "label": this.Name}
}

type RoleCrud struct {
	Factory *mgo.Factory
}

func NewRoleCrud(args ...interface{}) *RoleCrud {
	factory := mgo.NewFactory(args...)
	factory.SetDoc(&Role{})
	factory.SetArgus(mc.GetRoleConfig())
	crud := &RoleCrud{
		Factory: factory,
	}
	return crud
}
func (this *RoleCrud) GetCollection() *mongo.Collection {
	return this.Factory.GetCollection()
}
func (this *RoleCrud) FindId(id int64) (*Role, error) {
	md := new(Role)
	e := this.Factory.FindId(md, id)
	return md, e
}
func (this *RoleCrud) FindBy(where ss.M, order []string, page int, limit int) ([]*Role, error) {
	list := make([]*Role, 0)
	qb := &ez.HttpQuery{
		Conditions: where,
		Order:      order,
		Page:       page,
		Limit:      limit,
	}
	e := this.Factory.FindBy(&list, qb)
	return list, e
}
func (this *RoleCrud) FindOneBy(where ss.M, order []string) (*Role, error) {
	md := new(Role)
	qb := &ez.HttpQuery{
		Conditions: where,
		Order:      order,
	}
	e := this.Factory.FindOneBy(md, qb)
	return md, e
}
func neverUsed_Role_func() {
	//导入ss包
	a := ss.M{}
	fmt.Printf("%v", a)
	fmt.Printf("%v", context.TODO())
}
