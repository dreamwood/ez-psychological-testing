package core

import (
	"context"
	"ez/apps/core/auto/mc"
	"fmt"
	"gitee.com/dreamwood/ez-go/db/mgo"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"go.mongodb.org/mongo-driver/mongo"
)

func (this *User) MakeChoice() *ss.M {
	return &ss.M{"value": this.Id, "label": this.Id}
}

type UserCrud struct {
	Factory *mgo.Factory
}

func NewUserCrud(args ...interface{}) *UserCrud {
	factory := mgo.NewFactory(args...)
	factory.SetDoc(&User{})
	factory.SetArgus(mc.GetUserConfig())
	crud := &UserCrud{
		Factory: factory,
	}
	return crud
}
func (this *UserCrud) GetCollection() *mongo.Collection {
	return this.Factory.GetCollection()
}
func (this *UserCrud) FindId(id int64) (*User, error) {
	md := new(User)
	e := this.Factory.FindId(md, id)
	return md, e
}
func (this *UserCrud) FindBy(where ss.M, order []string, page int, limit int) ([]*User, error) {
	list := make([]*User, 0)
	qb := &ez.HttpQuery{
		Conditions: where,
		Order:      order,
		Page:       page,
		Limit:      limit,
	}
	e := this.Factory.FindBy(&list, qb)
	return list, e
}
func (this *UserCrud) FindOneBy(where ss.M, order []string) (*User, error) {
	md := new(User)
	qb := &ez.HttpQuery{
		Conditions: where,
		Order:      order,
	}
	e := this.Factory.FindOneBy(md, qb)
	return md, e
}
func neverUsed_User_func() {
	//导入ss包
	a := ss.M{}
	fmt.Printf("%v", a)
	fmt.Printf("%v", context.TODO())
}
