package core

import (
	"context"
	"encoding/json"
	"ez/apps/core/auto/mc"
	"gitee.com/dreamwood/ez-go/db/mgo"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"time"
)

type Top struct {
	mgo.BaseDoc `bson:"-" json:"-"`
	Id          int64      `json:"id" bson:"id,omitempty"`
	Model       *Model     `json:"model" bson:"model"`       //所属模块
	ModelId     int64      `json:"modelId" bson:"modelId"`   //所属模块
	Name        string     `json:"name" bson:"name"`         //名称
	Url         string     `json:"url" bson:"url"`           //URL
	Icon        string     `json:"icon" bson:"icon"`         //图标
	Sort        int64      `json:"sort" bson:"sort"`         //排序
	L           int64      `json:"l" bson:"l"`               //L
	R           int64      `json:"r" bson:"r"`               //R
	Level       int64      `json:"level" bson:"level"`       //Level
	Link        string     `json:"link" bson:"link"`         //link
	Parent      *Top       `json:"parent" bson:"parent"`     //父级菜单
	ParentId    int64      `json:"parentId" bson:"parentId"` //父级菜单
	Children    []*Top     `json:"children" bson:"children"` //子菜单
	CreateAt    time.Time  `json:"createAt" bson:"createAt"`
	UpdateAt    time.Time  `json:"updateAt" bson:"updateAt"`
	DeleteAt    *time.Time `json:"deleteAt" bson:"deleteAt"`
}

func (this *Top) DocName() string { return "Top" }
func (this *Top) GetId() int64    { return this.Id }
func (this *Top) SetId(id int64)  { this.Id = id }
func (this *Top) Create() error {
	return this.GetFactory().Create(this)
}
func (this *Top) Replace() error {
	return this.GetFactory().Replace(this)
}
func (this *Top) Save() error {
	if this.Id == 0 {
		this.CreateAt = time.Now()
		this.UpdateAt = time.Now()
		return this.GetFactory().Create(this)
	} else {
		this.UpdateAt = time.Now()
		return this.GetFactory().Replace(this)
	}
}

// 伪删除
func (this *Top) Delete() error {
	return this.GetFactory().Delete(this)
}
func (this *Top) UnDelete() error {
	return this.GetFactory().UnDelete(this)
}

// 真删除
func (this *Top) Destroy() error {
	return this.GetFactory().Destroy(this)
}
func (this *Top) ToString() string {
	return string(this.ToBytes())
}
func (this *Top) ToBytes() []byte {
	data, e := json.Marshal(this)
	ez.Try(e)
	return data
}
func (this *Top) Serialize() []byte {
	data, e := json.Marshal(this)
	ez.Try(e)
	return data
}
func (this *Top) UnSerialize(data []byte) []byte {
	ez.Try(json.Unmarshal(data, this))
	return data
}
func (this *Top) LoadModel() {
	if this.ModelId == 0 {
		return
	}
	this.Model, _ = NewModelCrud().FindId(this.ModelId)
}
func (this *Top) LoadParent() {
	if this.ParentId == 0 {
		return
	}
	this.Parent, _ = NewTopCrud().FindId(this.ParentId)
}
func (this *Top) LoadChildren() {
	children, _ := NewTopCrud().FindBy(ss.M{"parentId": this.Id}, []string{"id"}, 0, 0)
	this.Children = children
}
func (this *Top) ClearRelationsBeforeSave() mgo.Doc {
	this.Model = nil
	this.Parent = nil
	this.Children = nil
	return this
}
func neverUsed_Top() {
	//导入ss包
	a := ss.M{}
	ez.Debug(a)
}

type TopAccessControl struct {
	Access  bool
	Message string
	Action  string //控制器Action,小写开头，如c,u,r,d
	Model   *Top
	Session *ez.Session
}

func NewTopAccessControl(model *Top, action string, session *ez.Session) *TopAccessControl {
	ctrl := &TopAccessControl{
		Access:  true,
		Model:   model,
		Action:  action,
		Session: session,
	}
	ez.DispatchToMany(mc.TopAccessControlEvent, ctrl, context.TODO())
	return ctrl
}
