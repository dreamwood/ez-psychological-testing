package controller

import (
	"ez/apps/core/auto/controller"
	"ez/apps/core/service"
	"ez/config/r"
	"ez/custom/cc"
	"gitee.com/dreamwood/ez-go/ez"
)

type TopController struct {
	controller.TopAutoController
}

func init() {
	c := &TopController{}
	ez.CreateApi(r.AdminGet("Top"), c.GetAction)
	ez.CreateApi(r.AdminList("Top"), c.ListAction)
	ez.CreateApi(r.AdminSave("Top"), c.SaveAction)
	ez.CreateApi(r.AdminCopy("Top"), c.CopyAction)
	ez.CreateApi(r.AdminTree("Top"), c.TreeAction)
	ez.CreateApi(r.AdminUpdate("Top"), c.UpdateAction)
	ez.CreateApi(r.AdminChoice("Top"), c.ChoiceAction)
	ez.CreateApi(r.AdminDelete("Top"), c.DeleteAction)
	ez.CreateApi(r.AdminUnDelete("Top"), c.UnDeleteAction)
	ez.CreateApi(r.AdminDestroy("Top"), c.DestroyAction)
	ez.CreateApi(r.AdminUpdateMany("Top"), c.UpdateManyAction)
	ez.CreateApi(r.AdminDeleteMany("Top"), c.DeleteManyAction)
	ez.CreateApi(r.AdminDestroyMany("Top"), c.DestroyManyAction)
	ez.CreateApi(r.AdminApi("/Top/my"), c.MyAction).AddAccessRole(ez.Role_Admin)
}

// func (c TopController) AccessControl(session *ez.Session) { /* 在这里面重构 */ }
// func (c TopController) GetAction(session *ez.Session) { /* 在这里面重构 */ }
// func (c TopController) ListAction(session *ez.Session) { /* 在这里面重构 */ }
func (c TopController) MyAction(session *ez.Session) {
	this := cc.New(session)
	uid := this.GetUserId()
	list, e := service.GetMyTop(uid)
	if this.Try(e) {
		return
	}
	this.ReturnSuccess("OK", list)
}
