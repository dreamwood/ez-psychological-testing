package controller

import (
	"ez/apps/core/auto/controller"
	"ez/config/r"
	"gitee.com/dreamwood/ez-go/ez"
)

type RequestLogController struct {
	controller.RequestLogAutoController
}

func init() {
	c := &RequestLogController{}
	ez.CreateApi(r.AdminGet("RequestLog"), c.GetAction).
		AddAccessControl(c.AccessControl)
	ez.CreateApi(r.AdminList("RequestLog"), c.ListAction).
		AddAccessControl(c.AccessControl)
	ez.CreateApi(r.AdminSave("RequestLog"), c.SaveAction).
		AddAccessControl(c.AccessControl)
	ez.CreateApi(r.AdminCopy("RequestLog"), c.CopyAction).
		AddAccessControl(c.AccessControl)
	ez.CreateApi(r.AdminUpdate("RequestLog"), c.UpdateAction).
		AddAccessControl(c.AccessControl)
	ez.CreateApi(r.AdminChoice("RequestLog"), c.ChoiceAction).
		AddAccessControl(c.AccessControl)
	ez.CreateApi(r.AdminDelete("RequestLog"), c.DeleteAction).
		AddAccessControl(c.AccessControl)
	ez.CreateApi(r.AdminUnDelete("RequestLog"), c.UnDeleteAction).
		AddAccessControl(c.AccessControl)
	ez.CreateApi(r.AdminDestroy("RequestLog"), c.DestroyAction).
		AddAccessRole(ez.Role_Super_Admin)
	ez.CreateApi(r.AdminUpdateMany("RequestLog"), c.UpdateManyAction).
		AddAccessRole(ez.Role_Super_Admin)
	ez.CreateApi(r.AdminDeleteMany("RequestLog"), c.DeleteManyAction).
		AddAccessRole(ez.Role_Super_Admin)
	ez.CreateApi(r.AdminDestroyMany("RequestLog"), c.DestroyManyAction).
		AddAccessRole(ez.Role_Super_Admin)
}

//func (c RequestLogController) AccessControl(session *ez.Session) { /* 在这里面重构 */ }
//func (c RequestLogController) GetAction(session *ez.Session) { /* 在这里面重构 */ }
//func (c RequestLogController) ListAction(session *ez.Session) { /* 在这里面重构 */ }
