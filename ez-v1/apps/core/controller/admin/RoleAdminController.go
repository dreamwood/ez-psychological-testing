package controller

import (
	"ez/apps/core/auto/controller"
	"ez/apps/core/service"
	"ez/config/r"
	"ez/custom/cc"
	"gitee.com/dreamwood/ez-go/ez"
)

type RoleController struct {
	controller.RoleAutoController
}

func init() {
	c := &RoleController{}
	ez.CreateApi(r.AdminGet("Role"), c.GetAction)
	ez.CreateApi(r.AdminList("Role"), c.ListAction)
	ez.CreateApi(r.AdminSave("Role"), c.SaveAction)
	ez.CreateApi(r.AdminCopy("Role"), c.CopyAction)
	ez.CreateApi(r.AdminUpdate("Role"), c.UpdateAction)
	ez.CreateApi(r.AdminChoice("Role"), c.ChoiceAction)
	ez.CreateApi(r.AdminDelete("Role"), c.DeleteAction)
	ez.CreateApi(r.AdminUnDelete("Role"), c.UnDeleteAction)
	ez.CreateApi(r.AdminDestroy("Role"), c.DestroyAction)
	ez.CreateApi(r.AdminUpdateMany("Role"), c.UpdateManyAction)
	ez.CreateApi(r.AdminDeleteMany("Role"), c.DeleteManyAction)
	ez.CreateApi(r.AdminDestroyMany("Role"), c.DestroyManyAction)
	ez.CreateApi(r.AdminApi("/Role/reload"), c.ReloadAction)
}

// func (c RoleController) AccessControl(session *ez.Session) { /* 在这里面重构 */ }
// func (c RoleController) GetAction(session *ez.Session) { /* 在这里面重构 */ }
// func (c RoleController) ListAction(session *ez.Session) { /* 在这里面重构 */ }
func (c RoleController) ReloadAction(session *ez.Session) {
	this := cc.New(session)
	service.CreateApiRoles()
	this.ReturnSuccess("OK", "")
}
