package controller

import (
	"ez/apps/core/auto/controller"
	"ez/config/r"
	"gitee.com/dreamwood/ez-go/ez"
)

type GateWayController struct {
	controller.GateWayAutoController
}

func init() {
	c := &GateWayController{}
	ez.CreateApi(r.AdminGet("GateWay"), c.GetAction)
	ez.CreateApi(r.AdminList("GateWay"), c.ListAction)
	ez.CreateApi(r.AdminSave("GateWay"), c.SaveAction)
	ez.CreateApi(r.AdminCopy("GateWay"), c.CopyAction)
	ez.CreateApi(r.AdminUpdate("GateWay"), c.UpdateAction)
	ez.CreateApi(r.AdminChoice("GateWay"), c.ChoiceAction)
	ez.CreateApi(r.AdminDelete("GateWay"), c.DeleteAction)
	ez.CreateApi(r.AdminUnDelete("GateWay"), c.UnDeleteAction)
	ez.CreateApi(r.AdminDestroy("GateWay"), c.DestroyAction)
	ez.CreateApi(r.AdminUpdateMany("GateWay"), c.UpdateManyAction)
	ez.CreateApi(r.AdminDeleteMany("GateWay"), c.DeleteManyAction)
	ez.CreateApi(r.AdminDestroyMany("GateWay"), c.DestroyManyAction)
}

//func (c GateWayController) AccessControl(session *ez.Session) { /* 在这里面重构 */ }
//func (c GateWayController) GetAction(session *ez.Session) { /* 在这里面重构 */ }
//func (c GateWayController) ListAction(session *ez.Session) { /* 在这里面重构 */ }
