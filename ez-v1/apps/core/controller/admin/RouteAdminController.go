package controller

import (
	"ez/apps/core/auto/controller"
	"ez/config/r"
	"ez/custom/cc"
	"ez/plugin/gateway"
	"gitee.com/dreamwood/ez-go/ez"
)

type RouteController struct {
	controller.RouteAutoController
}

func init() {
	c := &RouteController{}
	ez.CreateApi(r.AdminGet("Route"), c.GetAction)
	ez.CreateApi(r.AdminList("Route"), c.ListAction)
	ez.CreateApi(r.AdminSave("Route"), c.SaveAction)
	ez.CreateApi(r.AdminCopy("Route"), c.CopyAction)
	ez.CreateApi(r.AdminUpdate("Route"), c.UpdateAction)
	ez.CreateApi(r.AdminChoice("Route"), c.ChoiceAction)
	ez.CreateApi(r.AdminDelete("Route"), c.DeleteAction)
	ez.CreateApi(r.AdminUnDelete("Route"), c.UnDeleteAction)
	ez.CreateApi(r.AdminDestroy("Route"), c.DestroyAction)
	ez.CreateApi(r.AdminUpdateMany("Route"), c.UpdateManyAction)
	ez.CreateApi(r.AdminDeleteMany("Route"), c.DeleteManyAction)
	ez.CreateApi(r.AdminDestroyMany("Route"), c.DestroyManyAction)
	ez.CreateApi(r.AdminApi("/Route/reload"), c.ReloadAction)
}

// func (c RouteController) AccessControl(session *ez.Session) { /* 在这里面重构 */ }
// func (c RouteController) GetAction(session *ez.Session) { /* 在这里面重构 */ }
// func (c RouteController) ListAction(session *ez.Session) { /* 在这里面重构 */ }
func (c RouteController) ReloadAction(session *ez.Session) {
	this := cc.New(session)
	gateway.PrepareRoute()
	this.ReturnSuccess("OK", "")
}
