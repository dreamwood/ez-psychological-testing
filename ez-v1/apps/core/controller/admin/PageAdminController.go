package controller

import (
	"ez/apps/core/auto/controller"
	core "ez/apps/core/document"
	"ez/apps/core/service"
	"ez/custom/cc"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
)

type PageController struct {
	controller.PageAutoController
}

func init() {
	c := &PageController{}
	c.SetRouteParam("/core", "/admin", "Page")
	ez.CreateApi(c.AdminGet(), c.GetAction).SetRouterName("[core]页面权限_创建")
	ez.CreateApi(c.AdminList(), c.ListAction).SetRouterName("[core]页面权限_列表")
	ez.CreateApi(c.AdminSave(), c.SaveAction).SetRouterName("[core]页面权限_保存")
	ez.CreateApi(c.AdminCopy(), c.CopyAction).SetRouterName("[core]页面权限_复制")
	ez.CreateApi(c.AdminUpdate(), c.UpdateAction).SetRouterName("[core]页面权限_更新")
	ez.CreateApi(c.AdminChoice(), c.ChoiceAction).SetRouterName("[core]页面权限_选项")
	ez.CreateApi(c.AdminDelete(), c.DeleteAction).SetRouterName("[core]页面权限_删除")
	ez.CreateApi(c.AdminUnDelete(), c.UnDeleteAction).SetRouterName("[core]页面权限_恢复")
	ez.CreateApi(c.AdminDestroy(), c.DestroyAction).SetRouterName("[core]页面权限_销毁")
	ez.CreateApi(c.AdminUpdateMany(), c.UpdateManyAction).SetRouterName("[core]页面权限_更新_批量")
	ez.CreateApi(c.AdminDeleteMany(), c.DeleteManyAction).SetRouterName("[core]页面权限_删除_批量")
	ez.CreateApi(c.AdminDestroyMany(), c.DestroyManyAction).SetRouterName("[core]页面权限_销毁_批量")
	ez.CreateApi(c.AdminApi("/Page/_reg"), c.RegAction).SetRouterName("[core]页面权限_注册")
	ez.CreateApi(c.AdminApi("/Page/my"), c.MyAction).SetRouterName("[core]页面权限_我的")
}

// func (c PageController) AccessControl(session *ez.Session) { /* 在这里面重构 */ }
// func (c PageController) GetAction(session *ez.Session) { /* 在这里面重构 */ }
// func (c PageController) ListAction(session *ez.Session) { /* 在这里面重构 */ }

func (c PageController) MyAction(session *ez.Session) {
	this := cc.New(session)
	uid := this.GetUserId()
	pages := service.GetMyPage(uid)
	this.ReturnSuccess("OK", pages)
}
func (c PageController) RegAction(session *ez.Session) {
	this := cc.New(session)
	query := struct {
		App  string `json:"app"`
		List []struct {
			Key  string `json:"key"`
			Name string `json:"name"`
		} `json:"list"`
	}{}

	_ = this.FillJson(&query)
	crudPage := core.NewPageCrud()
	for _, i := range query.List {
		find, _ := crudPage.FindOneBy(ss.M{
			"app": query.App,
			"key": i.Key,
		}, nil)
		find.App = query.App
		find.Name = i.Name
		find.Key = i.Key
		_ = find.Save()
	}
	this.ReturnSuccess("OK", query)
}
