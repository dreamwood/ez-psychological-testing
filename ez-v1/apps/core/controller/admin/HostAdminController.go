package controller

import (
	"ez/apps/core/auto/controller"
	"ez/config/r"
	"ez/custom/cc"
	"ez/plugin/gateway"
	"gitee.com/dreamwood/ez-go/ez"
)

type HostController struct {
	controller.HostAutoController
}

func init() {
	c := &HostController{}
	ez.CreateApi(r.AdminGet("Host"), c.GetAction)
	ez.CreateApi(r.AdminList("Host"), c.ListAction)
	ez.CreateApi(r.AdminSave("Host"), c.SaveAction)
	ez.CreateApi(r.AdminCopy("Host"), c.CopyAction)
	ez.CreateApi(r.AdminUpdate("Host"), c.UpdateAction)
	ez.CreateApi(r.AdminChoice("Host"), c.ChoiceAction)
	ez.CreateApi(r.AdminDelete("Host"), c.DeleteAction)
	ez.CreateApi(r.AdminUnDelete("Host"), c.UnDeleteAction)
	ez.CreateApi(r.AdminDestroy("Host"), c.DestroyAction)
	ez.CreateApi(r.AdminUpdateMany("Host"), c.UpdateManyAction)
	ez.CreateApi(r.AdminDeleteMany("Host"), c.DeleteManyAction)
	ez.CreateApi(r.AdminDestroyMany("Host"), c.DestroyManyAction)
	ez.CreateApi(r.AdminApi("/Host/reload"), c.ReloadAction)
}

// func (c HostController) AccessControl(session *ez.Session) { /* 在这里面重构 */ }
// func (c HostController) GetAction(session *ez.Session) { /* 在这里面重构 */ }
// func (c HostController) ListAction(session *ez.Session) { /* 在这里面重构 */ }
func (c HostController) ReloadAction(session *ez.Session) {
	this := cc.New(session)
	gateway.PrepareHost()
	this.ReturnSuccess("OK", "")
}
