package controller

import (
	"ez/apps/core/auto/controller"
	"ez/apps/core/service"
	"ez/config/r"
	"ez/custom/cc"
	"gitee.com/dreamwood/ez-go/ez"
)

type ModelController struct {
	controller.ModelAutoController
}

func init() {
	c := &ModelController{}
	ez.CreateApi(r.AdminGet("Model"), c.GetAction)
	ez.CreateApi(r.AdminList("Model"), c.ListAction)
	ez.CreateApi(r.AdminSave("Model"), c.SaveAction)
	ez.CreateApi(r.AdminCopy("Model"), c.CopyAction)
	ez.CreateApi(r.AdminUpdate("Model"), c.UpdateAction)
	ez.CreateApi(r.AdminChoice("Model"), c.ChoiceAction)
	ez.CreateApi(r.AdminDelete("Model"), c.DeleteAction)
	ez.CreateApi(r.AdminUnDelete("Model"), c.UnDeleteAction)
	ez.CreateApi(r.AdminDestroy("Model"), c.DestroyAction)
	ez.CreateApi(r.AdminUpdateMany("Model"), c.UpdateManyAction)
	ez.CreateApi(r.AdminDeleteMany("Model"), c.DeleteManyAction)
	ez.CreateApi(r.AdminDestroyMany("Model"), c.DestroyManyAction)
	ez.CreateApi(r.AdminApi("/Model/my"), c.MyAction).AddAccessRole(ez.Role_Admin)
}

// func (c ModelController) AccessControl(session *ez.Session) { /* 在这里面重构 */ }
// func (c ModelController) GetAction(session *ez.Session) { /* 在这里面重构 */ }
// func (c ModelController) ListAction(session *ez.Session) { /* 在这里面重构 */ }
func (c ModelController) MyAction(session *ez.Session) {
	this := cc.New(session)
	uid := this.GetUserId()
	list, e := service.GetMyModel(uid)
	if this.Try(e) {
		return
	}

	this.ReturnSuccess("OK", list)
}
