package gen

import "gitee.com/dreamwood/ez-go/maker"

func User() {
	doc := maker.CreateDoc("User", "用户", "core")
	doc.Add("Account", "账号").IsString()
	doc.Add("Password", "密码").IsString()
	doc.Add("Salt", "Salt").IsString()
	doc.Add("Token", "Token").IsString()
	doc.Add("Pic", "头像").IsString()
	doc.Add("Name", "姓名").IsString()
	doc.Add("Code", "工号").IsString()
	doc.Add("Phone", "手机号").IsString()
	doc.Add("IdCard", "身份证").IsString()
	doc.Add("Sex", "性别").IsInt()
	doc.Add("Age", "年龄").IsInt()
	doc.Add("Roles", "角色").IsJoinM2M("Role")

	doc.Generate()
}

func Role() {
	doc := maker.CreateDoc("Role", "角色", "core")
	doc.Add("name", "角色名").IsString()
	doc.Add("code", "编码").IsString()

	doc.Add("model", "可用模块").IsJoinM2M("Model")
	doc.Add("top", "顶栏权限").IsJoinM2M("Top")
	doc.Add("menu", "菜单权限").IsJoinM2M("Menu")
	doc.Add("apiAllow", "放通接口权限").IsJoinM2M("Api")
	doc.Add("apiDeny", "禁止接口权限").IsJoinM2M("Api")
	doc.Add("apiAllowKeys", "禁止接口权限").IsAny("[]string")
	doc.Add("apiDenyKeys", "禁止接口权限").IsAny("[]string")
	doc.Add("pageAllowKeys", "页面放行权限").IsAny("[]string")
	doc.Add("pageDenyKeys", "页面禁止权限").IsAny("[]string")

	doc.Generate()
}
