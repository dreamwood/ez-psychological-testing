import http from "@/assets/js/http";

export default {
    urlFind: '/core/admin/User/get',
    urlFindToEdit: '/core/admin/User/get',
    urlFindBy: '/core/admin/User/list',
    urlSave: '/core/admin/User/save',
    //urlDelete: '/core/admin/User/delete',
    //urlDeleteAll: '/core/admin/User/delete_many',
    urlDelete: '/core/admin/User/destroy',
    urlDeleteAll: '/core/admin/User/destroy_many',
    urlCopy: '/core/admin/User/copy',
    urlEditMany: '/core/admin/User/edit_many',
    urlTree: '/core/admin/User/tree',
    urlChoice: '/core/admin/User/choice',
    find(id, func) {
        http.get(this.urlFind, {id}, res => {
            func(res)
        })
    },
    findToEdit(id, func) {
        http.get(this.urlFindToEdit, {id}, res => {
            func(res)
        })
    },
    findBy(param, func) {
        http.post(this.urlFindBy, param, res => {
            func(res.data.lists, res.data.query)
        })
    },
    save(data, func) {
        http.post(this.urlSave, data, res => {
            func(res)
        })
    },
    delete(id, func) {
        http.get(this.urlDelete, {id}, res => {
            func(res)
        })
    },
    deleteAll(ids, func) {
        http.post(this.urlDeleteAll, {ids}, res => {
            func(res)
        })
    },
    copy(ids, func) {
        http.post(this.urlCopy, {ids}, res => {
            func(res)
        })
    },
    editMany(ids, data, func) {
        http.post(this.urlEditMany, {
            ids,
            _action: data
        }, res => {
            func(res)
        })
    },
}