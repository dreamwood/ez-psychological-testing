import http from "@/assets/js/http";

export default {
    urlFind: '/core/admin/Menu/get',
    urlFindToEdit: '/core/admin/Menu/get',
    urlFindBy: '/core/admin/Menu/list',
    urlSave: '/core/admin/Menu/save',
    //urlDelete: '/core/admin/Menu/delete',
    //urlDeleteAll: '/core/admin/Menu/delete_many',
    urlDelete: '/core/admin/Menu/destroy',
    urlDeleteAll: '/core/admin/Menu/destroy_many',
    urlCopy: '/core/admin/Menu/copy',
    urlEditMany: '/core/admin/Menu/edit_many',
    urlTree: '/core/admin/Menu/tree',
    urlChoice: '/core/admin/Menu/choice',
    find(id, func) {
        http.get(this.urlFind, {id}, res => {
            func(res)
        })
    },
    findToEdit(id, func) {
        http.get(this.urlFindToEdit, {id}, res => {
            func(res)
        })
    },
    findBy(param, func) {
        http.post(this.urlFindBy, param, res => {
            func(res.data.lists, res.data.query)
        })
    },
    save(data, func) {
        http.post(this.urlSave, data, res => {
            func(res)
        })
    },
    delete(id, func) {
        http.get(this.urlDelete, {id}, res => {
            func(res)
        })
    },
    deleteAll(ids, func) {
        http.post(this.urlDeleteAll, {ids}, res => {
            func(res)
        })
    },
    copy(ids, func) {
        http.post(this.urlCopy, {ids}, res => {
            func(res)
        })
    },
    editMany(ids, data, func) {
        http.post(this.urlEditMany, {
            ids,
            _action: data
        }, res => {
            func(res)
        })
    },
}