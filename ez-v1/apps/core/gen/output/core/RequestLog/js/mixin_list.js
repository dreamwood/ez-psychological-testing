import apis from "./api";
export default {
    data(){
        return{
            page:1,
            limit:10,
            group:"",
            total:0,
            //数据挖掘
            dumps:[],
            //筛选条件
            conditions:{},
            //排序
            order:[],
            //列表数据
            data:[],

            viewerModel:null,
            viewerOpen:false,
        }
    },
    mounted() {
        this.getList()
    },
    methods:{
        sort(order) {
            this.order = order
            this.getList(1)
        },
        getList(page){
            if (page !== undefined) this.page = page
            let cond = {}
            for (let key in this.params) {
                cond[key] = this.params[key]
            }
            if (this.showSearch){
                for (let key in this.conditions) {
                    if (!this.conditions[key])continue
                    cond[key] = this.conditions[key]
                }
            }
            if (this.searchText !== ""){
                cond["_and"]={
                    _ors:[
                        {name__like: this.searchText},
                    ]
                }
            }
            let param = {
                _where: cond,
                _order: this.order,
                page:this.page,
                limit:this.limit,
                _dumps:this.dumps,
            }
            apis.findBy(param,  (data,query) => {
                this.data = data
                if (query !== undefined) {
                    this.page = query.page
                    this.total = query.total
                    this.limit = query.limit
                }
            })
        },


        refresh() {
            this.getList(1)
        },

        copy: function () {
            if ( this.selected.length === 0){
                this.$toast.error("至少选择一条数据")
                return
            }
            let ids = []
            for (let row of this.selected) {
                ids.push(this.data[row].id)
            }
            apis.copy(ids, res => {
                this.$toast.success(
                    res.message
                )
                this.getList(this.page)
                this.selected = []
            })
        },
        remove: function (id) {
            if (this.askBeforeDelete){
                this.$confirm('确定要进行删除操作吗？', '提示').then(({ result }) => {
                    if (result) {
                        apis.delete(id, (res)=>{
                            this.$toast.success(
                                res.message
                            )
                            this.getList(this.page)
                        })
                    } else {
                        this.$toast.message('取消删除');
                    }
                });
            }else {
                apis.delete(id, (res)=>{
                    this.$toast.success(
                        res.message
                    )
                    this.getList(this.page)
                })
            }
        },
        removeAll: function () {
            if ( this.selected.length === 0){
                this.$toast.error("至少选择一条数据")
                return
            }
            this.$confirm('确定要进行删除操作吗？', '提示').then(({ result }) => {
                if (result) {
                    let ids = []
                    for (let row of this.selected) {
                        ids.push(this.data[row].id)
                    }
                    apis.deleteAll(ids, (res)=>{
                        this.$toast.info(
                            res.message
                        )
                        this.getList(this.page)
                    })
                    this.selected = []
                } else {
                    this.$toast.message('取消删除');
                }
            });
        },

        openViewer(model){
            this.viewerOpen = true
            this.viewerModel = model
        },

    }
}