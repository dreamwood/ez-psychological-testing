import listBuilder from "@/comps/list/listBuilder";

/*
lb.add("url","",80)
lb.add("session","",80)
lb.add("logs","",80)

*/
export default function () {
    let lb = listBuilder()
    lb.add("id","#",80)
    lb.addAction(120).setAlignRight()
    return lb.headers
}