import listBuilder from "@/comps/list/listBuilder";

/*
lb.add("appId","APPID",80)
lb.add("name","模块名",80)
lb.add("cnName","中文名",80)
lb.add("entry","入口地址",80)
lb.add("isOn","开启",80)

*/
export default function () {
    let lb = listBuilder()
    lb.add("id","#",80)
    lb.addAction(120).setAlignRight()
    return lb.headers
}