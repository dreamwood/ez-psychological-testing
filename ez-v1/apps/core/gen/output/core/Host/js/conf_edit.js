import formBuilder from "@/comps/form/formBuilder";
import api from "./api";
/*
fb.addText("appId","APPID",3)
fb.addText("machineCode","机器码",3)
fb.addText("ip","IP地址",3)
fb.addText("port","端口",3)
fb.addText("weight","权重",3)
fb.addText("isOn","启用",3)
fb.addText("state","状态码",3)
fb.addText("stateInfo","状态信息",3)
fb.addText("lastTime","上次心跳时间",3)

* */
export default {
    main:function () {
        let fb = formBuilder()
        fb.setLabelWidth(100)

        fb.addText("name").setSpan(3).setLabel("姓名")

        fb.addText("sort").setSpan(3).setLabel("排序").setTypeNumber()

        return fb
    },
    tree:function () {
        let fb = formBuilder()
        fb.setLabelWidth(100)

        fb.addCascade("parentId").setLabel("上级分类").setSpan(3)
            .setApi(api.urlTree)

        fb.addText("name").setSpan(6).setLabel("分类名称")
            .setRules([fb.R.notNull("名称不能为空")])

        fb.addText("sort").setSpan(3).setLabel("排序").setTypeNumber()
        return fb
    },
    other:function () {
        let fb = formBuilder()
        fb.setLabelWidth(100).setLabelPosition("right")

        return fb
    }
}