import listBuilder from "@/comps/list/listBuilder";

/*
lb.add("appId","APPID",80)
lb.add("machineCode","机器码",80)
lb.add("ip","IP地址",80)
lb.add("port","端口",80)
lb.add("weight","权重",80)
lb.add("isOn","启用",80)
lb.add("state","状态码",80)
lb.add("stateInfo","状态信息",80)
lb.add("lastTime","上次心跳时间",80)

*/
export default function () {
    let lb = listBuilder()
    lb.add("id","#",80)
    lb.addAction(120).setAlignRight()
    return lb.headers
}