package gen

import "testing"

func TestGenCore(t *testing.T) {
	GateWay()
	Host()

	//auth
	User()
	Role()
	Api()
	Model()
	Route()
	Page()

	//web
	Top()
	Menu()

	//dev
	RequestLog()
}

func TestPage(t *testing.T) {
	Role()
	Page()
}
