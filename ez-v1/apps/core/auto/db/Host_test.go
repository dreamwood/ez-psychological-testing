package db

import (
	"context"
	"encoding/json"
	core "ez/apps/core/document"
	"fmt"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"gitee.com/dreamwood/ez-go/tools"
	"go.mongodb.org/mongo-driver/bson"
	"os"
	"testing"
)

func DropHost() {
	ez.DBMongo.Collection("Host").Drop(context.TODO())
}
func TestDropHost(t *testing.T) {
	ez.DefaultInit()
	DropHost()
}
func BackUpHost(targetFile string) {
	if targetFile == "" {
		targetFile = "./Host.json"
	}
	ez.DefaultInit()
	//计数
	total, e := ez.DBMongo.Collection("Host").CountDocuments(context.TODO(), bson.M{})
	if e != nil {
		ez.LogToConsole(e.Error())
	}
	f, e := os.OpenFile(targetFile, os.O_CREATE|os.O_WRONLY|os.O_RDWR, 0666)
	if e != nil {
		ez.LogToConsole(e.Error())
	}
	defer f.Close()
	crud := core.NewHostCrud()
	data := make([]core.Host, 0)
	for i := 0; i <= int(total/1000); i++ {
		rows, e := crud.FindBy(ss.M{}, nil, i, 1000)
		if e != nil {
			ez.LogToConsole(e.Error())
		}
		for _, row := range rows {
			data = append(data, *row)
		}
	}
	content, e := json.Marshal(data)
	f.Write(content)
}
func TestBackUpHost(t *testing.T) {
	ez.DefaultInit()
	targetFile := fmt.Sprintf("./Host_%s.json", tools.GetDateYMDHIS("", "", "_"))
	BackUpHost(targetFile)
}
func RecoverHost(filePath string) {
	if filePath == "" {
		filePath = "./Host.json"
	}
	ez.DefaultInit()
	ez.DBMongo.Collection("Host").Drop(context.TODO())
	content := tools.ReadFile(filePath)
	data := make([]core.Host, 0)
	e := json.Unmarshal(content, &data)
	if e != nil {
		ez.LogToConsole(e.Error())
	}
	for _, row := range data {
		e = row.Create()
		if e != nil {
			ez.LogToConsole(e.Error())
		}
	}
	ez.LogToConsole(fmt.Sprintf("写入数据%d条", len(data)))
}
func TestRecoverHost(t *testing.T) {
	filePath := "./Host_20240107_190550.json"
	ez.DefaultInit()
	RecoverHost(filePath)
}
