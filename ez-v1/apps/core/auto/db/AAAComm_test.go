package db

import (
	"gitee.com/dreamwood/ez-go/ez"
	"testing"
)

func TestBackUpAuthData(t *testing.T) {
	ez.DefaultInit()
	BackUpApi("")
	BackUpRole("")
	BackUpGateWay("")
	BackUpUser("")
	BackUpTop("")
	BackUpModel("")
	BackUpMenu("")
	BackUpHost("")
	BackUpRoute("")
}

func TestRecoverAuthData(t *testing.T) {
	ez.DefaultInit()
	DropApi()
	DropRole()
	DropGateWay()
	DropUser()
	DropTop()
	DropModel()
	DropMenu()
	DropHost()
	DropRoute()
	RecoverApi("")
	RecoverGateWay("")
	RecoverRole("")
	RecoverUser("")
	RecoverModel("")
	RecoverMenu("")
	RecoverTop("")
	RecoverHost("")
	RecoverRoute("")
}
