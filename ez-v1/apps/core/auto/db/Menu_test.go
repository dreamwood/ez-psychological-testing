package db

import (
	"context"
	"encoding/json"
	core "ez/apps/core/document"
	"fmt"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"gitee.com/dreamwood/ez-go/tools"
	"go.mongodb.org/mongo-driver/bson"
	"os"
	"testing"
)

func DropMenu() {
	ez.DBMongo.Collection("Menu").Drop(context.TODO())
}
func TestDropMenu(t *testing.T) {
	ez.DefaultInit()
	DropMenu()
}
func BackUpMenu(targetFile string) {
	if targetFile == "" {
		targetFile = "./Menu.json"
	}
	ez.DefaultInit()
	//计数
	total, e := ez.DBMongo.Collection("Menu").CountDocuments(context.TODO(), bson.M{})
	if e != nil {
		ez.LogToConsole(e.Error())
	}
	f, e := os.OpenFile(targetFile, os.O_CREATE|os.O_WRONLY|os.O_RDWR, 0666)
	if e != nil {
		ez.LogToConsole(e.Error())
	}
	defer f.Close()
	crud := core.NewMenuCrud()
	data := make([]core.Menu, 0)
	for i := 0; i <= int(total/1000); i++ {
		rows, e := crud.FindBy(ss.M{}, nil, i, 1000)
		if e != nil {
			ez.LogToConsole(e.Error())
		}
		for _, row := range rows {
			data = append(data, *row)
		}
	}
	content, e := json.Marshal(data)
	f.Write(content)
}
func TestBackUpMenu(t *testing.T) {
	ez.DefaultInit()
	targetFile := fmt.Sprintf("./Menu_%s.json", tools.GetDateYMDHIS("", "", "_"))
	BackUpMenu(targetFile)
}
func RecoverMenu(filePath string) {
	if filePath == "" {
		filePath = "./Menu.json"
	}
	ez.DefaultInit()
	ez.DBMongo.Collection("Menu").Drop(context.TODO())
	content := tools.ReadFile(filePath)
	data := make([]core.Menu, 0)
	e := json.Unmarshal(content, &data)
	if e != nil {
		ez.LogToConsole(e.Error())
	}
	for _, row := range data {
		e = row.Create()
		if e != nil {
			ez.LogToConsole(e.Error())
		}
	}
	ez.LogToConsole(fmt.Sprintf("写入数据%d条", len(data)))
}
func TestRecoverMenu(t *testing.T) {
	filePath := "./Menu_20240107_190550.json"
	ez.DefaultInit()
	RecoverMenu(filePath)
}
