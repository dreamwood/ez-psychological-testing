package db

import (
	"context"
	"encoding/json"
	core "ez/apps/core/document"
	"fmt"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"gitee.com/dreamwood/ez-go/tools"
	"go.mongodb.org/mongo-driver/bson"
	"os"
	"testing"
)

func DropRoute() {
	ez.DBMongo.Collection("Route").Drop(context.TODO())
}
func TestDropRoute(t *testing.T) {
	ez.DefaultInit()
	DropRoute()
}
func BackUpRoute(targetFile string) {
	if targetFile == "" {
		targetFile = "./Route.json"
	}
	ez.DefaultInit()
	//计数
	total, e := ez.DBMongo.Collection("Route").CountDocuments(context.TODO(), bson.M{})
	if e != nil {
		ez.LogToConsole(e.Error())
	}
	f, e := os.OpenFile(targetFile, os.O_CREATE|os.O_WRONLY|os.O_RDWR, 0666)
	if e != nil {
		ez.LogToConsole(e.Error())
	}
	defer f.Close()
	crud := core.NewRouteCrud()
	data := make([]core.Route, 0)
	for i := 0; i <= int(total/1000); i++ {
		rows, e := crud.FindBy(ss.M{}, nil, i, 1000)
		if e != nil {
			ez.LogToConsole(e.Error())
		}
		for _, row := range rows {
			data = append(data, *row)
		}
	}
	content, e := json.Marshal(data)
	f.Write(content)
}
func TestBackUpRoute(t *testing.T) {
	ez.DefaultInit()
	targetFile := fmt.Sprintf("./Route_%s.json", tools.GetDateYMDHIS("", "", "_"))
	BackUpRoute(targetFile)
}
func RecoverRoute(filePath string) {
	if filePath == "" {
		filePath = "./Route.json"
	}
	ez.DefaultInit()
	ez.DBMongo.Collection("Route").Drop(context.TODO())
	content := tools.ReadFile(filePath)
	data := make([]core.Route, 0)
	e := json.Unmarshal(content, &data)
	if e != nil {
		ez.LogToConsole(e.Error())
	}
	for _, row := range data {
		e = row.Create()
		if e != nil {
			ez.LogToConsole(e.Error())
		}
	}
	ez.LogToConsole(fmt.Sprintf("写入数据%d条", len(data)))
}
func TestRecoverRoute(t *testing.T) {
	filePath := "./Route_20240107_190550.json"
	ez.DefaultInit()
	RecoverRoute(filePath)
}
