package mc

import (
	"gitee.com/dreamwood/ez-go/db/mgo"
)

const (
	UserEventNew           = "core.UserNew"
	UserEventBeforeCreate  = "core.UserBeforeCreate"
	UserEventBeforeUpdate  = "core.UserBeforeUpdate"
	UserEventBeforeSave    = "core.UserBeforeCreate core.UserBeforeUpdate"
	UserEventAfterCreate   = "core.UserAfterCreate"
	UserEventAfterUpdate   = "core.UserAfterUpdate"
	UserEventAfterSave     = "core.UserAfterCreate core.UserAfterUpdate"
	UserEventDelete        = "core.UserDelete"
	UserAccessControlEvent = "core.UserAccessControl"
)

func GetUserConfig() *mgo.DocConfig {
	return User_Config
}

var User_Config *mgo.DocConfig

func init() {
	User_Config = NewUserConfig()
}
func NewUserConfig() *mgo.DocConfig {
	return &mgo.DocConfig{
		ContainerKey: "core.User",
		Fields: []string{
			"account", "password", "salt", "token", "pic", "name", "code", "phone", "idCard", "sex", "age", "roles",
		},
		RelationFields: []string{
			"roles",
		},
		RelationConfigs: map[string]*mgo.DocRelation{
			"roles": {
				Config:     GetRoleConfig,
				DocName:    "Role",
				JoinType:   "MM",
				KeyInside:  "rolesIds",
				KeyOutSide: "id",
			},
		},
		FieldFilter: map[string]*mgo.DocFieldFilter{
			"default": {
				Select: []string{},
				Omit:   []string{},
			},
		},
	}
}
