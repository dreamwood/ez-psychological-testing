package mc

import (
	"gitee.com/dreamwood/ez-go/db/mgo"
)

const (
	MenuEventNew           = "core.MenuNew"
	MenuEventBeforeCreate  = "core.MenuBeforeCreate"
	MenuEventBeforeUpdate  = "core.MenuBeforeUpdate"
	MenuEventBeforeSave    = "core.MenuBeforeCreate core.MenuBeforeUpdate"
	MenuEventAfterCreate   = "core.MenuAfterCreate"
	MenuEventAfterUpdate   = "core.MenuAfterUpdate"
	MenuEventAfterSave     = "core.MenuAfterCreate core.MenuAfterUpdate"
	MenuEventDelete        = "core.MenuDelete"
	MenuAccessControlEvent = "core.MenuAccessControl"
)

func GetMenuConfig() *mgo.DocConfig {
	return Menu_Config
}

var Menu_Config *mgo.DocConfig

func init() {
	Menu_Config = NewMenuConfig()
}
func NewMenuConfig() *mgo.DocConfig {
	return &mgo.DocConfig{
		ContainerKey: "core.Menu",
		Fields: []string{
			"model", "name", "url", "icon", "sort", "l", "r", "level", "link", "parent", "children",
		},
		RelationFields: []string{
			"model", "parent", "children",
		},
		RelationConfigs: map[string]*mgo.DocRelation{
			"model": {
				Config:     GetModelConfig,
				DocName:    "Model",
				JoinType:   "O",
				KeyInside:  "modelId",
				KeyOutSide: "id",
			},
			"parent": {
				Config:     GetMenuConfig,
				DocName:    "Menu",
				JoinType:   "O",
				KeyInside:  "parentId",
				KeyOutSide: "id",
			},
			"children": {
				Config:     GetMenuConfig,
				DocName:    "Menu",
				JoinType:   "M",
				KeyInside:  "id",
				KeyOutSide: "parentId",
			},
		},
		FieldFilter: map[string]*mgo.DocFieldFilter{
			"default": {
				Select: []string{},
				Omit:   []string{},
			},
		},
	}
}
