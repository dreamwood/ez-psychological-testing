package mc

import (
	"gitee.com/dreamwood/ez-go/db/mgo"
)

const (
	TopEventNew           = "core.TopNew"
	TopEventBeforeCreate  = "core.TopBeforeCreate"
	TopEventBeforeUpdate  = "core.TopBeforeUpdate"
	TopEventBeforeSave    = "core.TopBeforeCreate core.TopBeforeUpdate"
	TopEventAfterCreate   = "core.TopAfterCreate"
	TopEventAfterUpdate   = "core.TopAfterUpdate"
	TopEventAfterSave     = "core.TopAfterCreate core.TopAfterUpdate"
	TopEventDelete        = "core.TopDelete"
	TopAccessControlEvent = "core.TopAccessControl"
)

func GetTopConfig() *mgo.DocConfig {
	return Top_Config
}

var Top_Config *mgo.DocConfig

func init() {
	Top_Config = NewTopConfig()
}
func NewTopConfig() *mgo.DocConfig {
	return &mgo.DocConfig{
		ContainerKey: "core.Top",
		Fields: []string{
			"model", "name", "url", "icon", "sort", "l", "r", "level", "link", "parent", "children",
		},
		RelationFields: []string{
			"model", "parent", "children",
		},
		RelationConfigs: map[string]*mgo.DocRelation{
			"model": {
				Config:     GetModelConfig,
				DocName:    "Model",
				JoinType:   "O",
				KeyInside:  "modelId",
				KeyOutSide: "id",
			},
			"parent": {
				Config:     GetTopConfig,
				DocName:    "Top",
				JoinType:   "O",
				KeyInside:  "parentId",
				KeyOutSide: "id",
			},
			"children": {
				Config:     GetTopConfig,
				DocName:    "Top",
				JoinType:   "M",
				KeyInside:  "id",
				KeyOutSide: "parentId",
			},
		},
		FieldFilter: map[string]*mgo.DocFieldFilter{
			"default": {
				Select: []string{},
				Omit:   []string{},
			},
		},
	}
}
