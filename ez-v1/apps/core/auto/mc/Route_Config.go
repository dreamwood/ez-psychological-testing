package mc

import (
	"gitee.com/dreamwood/ez-go/db/mgo"
)

const (
	RouteEventNew           = "core.RouteNew"
	RouteEventBeforeCreate  = "core.RouteBeforeCreate"
	RouteEventBeforeUpdate  = "core.RouteBeforeUpdate"
	RouteEventBeforeSave    = "core.RouteBeforeCreate core.RouteBeforeUpdate"
	RouteEventAfterCreate   = "core.RouteAfterCreate"
	RouteEventAfterUpdate   = "core.RouteAfterUpdate"
	RouteEventAfterSave     = "core.RouteAfterCreate core.RouteAfterUpdate"
	RouteEventDelete        = "core.RouteDelete"
	RouteAccessControlEvent = "core.RouteAccessControl"
)

func GetRouteConfig() *mgo.DocConfig {
	return Route_Config
}

var Route_Config *mgo.DocConfig

func init() {
	Route_Config = NewRouteConfig()
}
func NewRouteConfig() *mgo.DocConfig {
	return &mgo.DocConfig{
		ContainerKey: "core.Route",
		Fields: []string{
			"appId", "from", "to", "isOn", "sort", "gateWay",
		},
		RelationFields: []string{
			"gateWay",
		},
		RelationConfigs: map[string]*mgo.DocRelation{
			"gateWay": {
				Config:     GetGateWayConfig,
				DocName:    "GateWay",
				JoinType:   "O",
				KeyInside:  "gateWayId",
				KeyOutSide: "id",
			},
		},
		FieldFilter: map[string]*mgo.DocFieldFilter{
			"default": {
				Select: []string{},
				Omit:   []string{},
			},
		},
	}
}
