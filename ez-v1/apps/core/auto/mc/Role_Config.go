package mc

import (
	"gitee.com/dreamwood/ez-go/db/mgo"
)

const (
	RoleEventNew           = "core.RoleNew"
	RoleEventBeforeCreate  = "core.RoleBeforeCreate"
	RoleEventBeforeUpdate  = "core.RoleBeforeUpdate"
	RoleEventBeforeSave    = "core.RoleBeforeCreate core.RoleBeforeUpdate"
	RoleEventAfterCreate   = "core.RoleAfterCreate"
	RoleEventAfterUpdate   = "core.RoleAfterUpdate"
	RoleEventAfterSave     = "core.RoleAfterCreate core.RoleAfterUpdate"
	RoleEventDelete        = "core.RoleDelete"
	RoleAccessControlEvent = "core.RoleAccessControl"
)

func GetRoleConfig() *mgo.DocConfig {
	return Role_Config
}

var Role_Config *mgo.DocConfig

func init() {
	Role_Config = NewRoleConfig()
}
func NewRoleConfig() *mgo.DocConfig {
	return &mgo.DocConfig{
		ContainerKey: "core.Role",
		Fields: []string{
			"name", "code", "model", "top", "menu", "apiAllow", "apiDeny", "apiAllowKeys", "apiDenyKeys", "page", "pageAllowKeys", "pageDenyKeys",
		},
		RelationFields: []string{
			"model", "top", "menu", "apiAllow", "apiDeny", "page",
		},
		RelationConfigs: map[string]*mgo.DocRelation{
			"model": {
				Config:     GetModelConfig,
				DocName:    "Model",
				JoinType:   "MM",
				KeyInside:  "modelIds",
				KeyOutSide: "id",
			},
			"top": {
				Config:     GetTopConfig,
				DocName:    "Top",
				JoinType:   "MM",
				KeyInside:  "topIds",
				KeyOutSide: "id",
			},
			"menu": {
				Config:     GetMenuConfig,
				DocName:    "Menu",
				JoinType:   "MM",
				KeyInside:  "menuIds",
				KeyOutSide: "id",
			},
			"apiAllow": {
				Config:     GetApiConfig,
				DocName:    "Api",
				JoinType:   "MM",
				KeyInside:  "apiAllowIds",
				KeyOutSide: "id",
			},
			"apiDeny": {
				Config:     GetApiConfig,
				DocName:    "Api",
				JoinType:   "MM",
				KeyInside:  "apiDenyIds",
				KeyOutSide: "id",
			},
			"page": {
				Config:     GetPageConfig,
				DocName:    "Page",
				JoinType:   "MM",
				KeyInside:  "pageIds",
				KeyOutSide: "key",
			},
		},
		FieldFilter: map[string]*mgo.DocFieldFilter{
			"default": {
				Select: []string{},
				Omit:   []string{},
			},
		},
	}
}
