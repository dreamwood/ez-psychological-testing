package mc

import (
	"gitee.com/dreamwood/ez-go/db/mgo"
)

const (
	GateWayEventNew           = "core.GateWayNew"
	GateWayEventBeforeCreate  = "core.GateWayBeforeCreate"
	GateWayEventBeforeUpdate  = "core.GateWayBeforeUpdate"
	GateWayEventBeforeSave    = "core.GateWayBeforeCreate core.GateWayBeforeUpdate"
	GateWayEventAfterCreate   = "core.GateWayAfterCreate"
	GateWayEventAfterUpdate   = "core.GateWayAfterUpdate"
	GateWayEventAfterSave     = "core.GateWayAfterCreate core.GateWayAfterUpdate"
	GateWayEventDelete        = "core.GateWayDelete"
	GateWayAccessControlEvent = "core.GateWayAccessControl"
)

func GetGateWayConfig() *mgo.DocConfig {
	return GateWay_Config
}

var GateWay_Config *mgo.DocConfig

func init() {
	GateWay_Config = NewGateWayConfig()
}
func NewGateWayConfig() *mgo.DocConfig {
	return &mgo.DocConfig{
		ContainerKey: "core.GateWay",
		Fields: []string{
			"ip", "port", "isOn", "hosts",
		},
		RelationFields: []string{
			"hosts",
		},
		RelationConfigs: map[string]*mgo.DocRelation{
			"hosts": {
				Config:     GetHostConfig,
				DocName:    "Host",
				JoinType:   "MM",
				KeyInside:  "hostsIds",
				KeyOutSide: "id",
			},
		},
		FieldFilter: map[string]*mgo.DocFieldFilter{
			"default": {
				Select: []string{},
				Omit:   []string{},
			},
		},
	}
}
