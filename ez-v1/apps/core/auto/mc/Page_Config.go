package mc

import (
	"gitee.com/dreamwood/ez-go/db/mgo"
)

const (
	PageEventNew           = "core.PageNew"
	PageEventBeforeCreate  = "core.PageBeforeCreate"
	PageEventBeforeUpdate  = "core.PageBeforeUpdate"
	PageEventBeforeSave    = "core.PageBeforeCreate core.PageBeforeUpdate"
	PageEventAfterCreate   = "core.PageAfterCreate"
	PageEventAfterUpdate   = "core.PageAfterUpdate"
	PageEventAfterSave     = "core.PageAfterCreate core.PageAfterUpdate"
	PageEventDelete        = "core.PageDelete"
	PageAccessControlEvent = "core.PageAccessControl"
)

func GetPageConfig() *mgo.DocConfig {
	return Page_Config
}

var Page_Config *mgo.DocConfig

func init() {
	Page_Config = NewPageConfig()
}
func NewPageConfig() *mgo.DocConfig {
	return &mgo.DocConfig{
		ContainerKey: "core.Page",
		Fields: []string{
			"app", "name", "key", "route", "db",
		},
		RelationFields:  []string{},
		RelationConfigs: map[string]*mgo.DocRelation{},
		FieldFilter: map[string]*mgo.DocFieldFilter{
			"default": {
				Select: []string{},
				Omit:   []string{},
			},
		},
	}
}
