package mc

import (
	"gitee.com/dreamwood/ez-go/db/mgo"
)

const (
	HostEventNew           = "core.HostNew"
	HostEventBeforeCreate  = "core.HostBeforeCreate"
	HostEventBeforeUpdate  = "core.HostBeforeUpdate"
	HostEventBeforeSave    = "core.HostBeforeCreate core.HostBeforeUpdate"
	HostEventAfterCreate   = "core.HostAfterCreate"
	HostEventAfterUpdate   = "core.HostAfterUpdate"
	HostEventAfterSave     = "core.HostAfterCreate core.HostAfterUpdate"
	HostEventDelete        = "core.HostDelete"
	HostAccessControlEvent = "core.HostAccessControl"
)

func GetHostConfig() *mgo.DocConfig {
	return Host_Config
}

var Host_Config *mgo.DocConfig

func init() {
	Host_Config = NewHostConfig()
}
func NewHostConfig() *mgo.DocConfig {
	return &mgo.DocConfig{
		ContainerKey: "core.Host",
		Fields: []string{
			"appId", "machineCode", "ip", "port", "weight", "isOn", "state", "stateInfo", "lastTime",
		},
		RelationFields:  []string{},
		RelationConfigs: map[string]*mgo.DocRelation{},
		FieldFilter: map[string]*mgo.DocFieldFilter{
			"default": {
				Select: []string{},
				Omit:   []string{},
			},
		},
	}
}
