package event

import (
	"context"
	"ez/apps/core/auto/mc"
	core "ez/apps/core/document"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/tools"
)

func init() {
	ez.Subscribe(mc.UserEventAfterCreate, func(v interface{}, ctx context.Context) {
		md, ok := v.(*core.User)
		if ok {
			md.Salt = tools.CreateRandString(16)
		}
	})
}
