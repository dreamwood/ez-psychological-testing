package event

import (
	"context"
	"ez/apps/core/service"
	"ez/config"
	"gitee.com/dreamwood/ez-go/ez"
)

func init() {
	ez.Subscribe(service.EventUserLogin, func(v interface{}, ctx context.Context) {
		data, ok := v.(map[string]interface{})
		if ok {
			data["redirect"] = config.ConfigLogin.LoginSuccessRedirect
		}
	})
}
