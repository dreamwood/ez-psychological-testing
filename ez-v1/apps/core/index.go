package core

import (
	_ "ez/apps/core/controller"
	_ "ez/apps/core/controller/admin"
	_ "ez/apps/core/event"
	_ "ez/apps/core/nsq"
	_ "ez/apps/core/sync"
	_ "ez/apps/core/task"
)
