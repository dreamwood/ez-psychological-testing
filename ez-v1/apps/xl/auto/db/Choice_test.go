package db

import (
	"context"
	"encoding/json"
	xl "ez/apps/xl/document"
	"fmt"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"gitee.com/dreamwood/ez-go/tools"
	"go.mongodb.org/mongo-driver/bson"
	"os"
	"testing"
)

func DropChoice() {
	ez.DBMongo.Collection("Choice").Drop(context.TODO())
}
func TestDropChoice(t *testing.T) {
	ez.DefaultInit()
	DropChoice()
}
func BackUpChoice(targetFile string) {
	if targetFile == "" {
		targetFile = "./Choice.json"
	}
	ez.DefaultInit()
	//计数
	total, e := ez.DBMongo.Collection("Choice").CountDocuments(context.TODO(), bson.M{})
	if e != nil {
		ez.LogToConsole(e.Error())
	}
	f, e := os.OpenFile(targetFile, os.O_CREATE|os.O_WRONLY|os.O_RDWR, 0666)
	if e != nil {
		ez.LogToConsole(e.Error())
	}
	defer f.Close()
	crud := xl.NewChoiceCrud()
	data := make([]xl.Choice, 0)
	for i := 0; i <= int(total/1000); i++ {
		rows, e := crud.FindBy(ss.M{}, nil, i, 1000)
		if e != nil {
			ez.LogToConsole(e.Error())
		}
		for _, row := range rows {
			data = append(data, *row)
		}
	}
	content, e := json.Marshal(data)
	f.Write(content)
}
func TestBackUpChoice(t *testing.T) {
	ez.DefaultInit()
	targetFile := fmt.Sprintf("./Choice_%s.json", tools.GetDateYMDHIS("", "", "_"))
	BackUpChoice(targetFile)
}
func RecoverChoice(filePath string) {
	if filePath == "" {
		filePath = "./Choice.json"
	}
	ez.DefaultInit()
	ez.DBMongo.Collection("Choice").Drop(context.TODO())
	content := tools.ReadFile(filePath)
	data := make([]xl.Choice, 0)
	e := json.Unmarshal(content, &data)
	if e != nil {
		ez.LogToConsole(e.Error())
	}
	for _, row := range data {
		e = row.Create()
		if e != nil {
			ez.LogToConsole(e.Error())
		}
	}
	ez.LogToConsole(fmt.Sprintf("写入数据%d条", len(data)))
}
func TestRecoverChoice(t *testing.T) {
	filePath := "./Choice_20240107_190550.json"
	ez.DefaultInit()
	RecoverChoice(filePath)
}
