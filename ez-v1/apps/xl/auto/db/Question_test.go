package db

import (
	"context"
	"encoding/json"
	xl "ez/apps/xl/document"
	"fmt"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"gitee.com/dreamwood/ez-go/tools"
	"go.mongodb.org/mongo-driver/bson"
	"os"
	"testing"
)

func DropQuestion() {
	ez.DBMongo.Collection("Question").Drop(context.TODO())
}
func TestDropQuestion(t *testing.T) {
	ez.DefaultInit()
	DropQuestion()
}
func BackUpQuestion(targetFile string) {
	if targetFile == "" {
		targetFile = "./Question.json"
	}
	ez.DefaultInit()
	//计数
	total, e := ez.DBMongo.Collection("Question").CountDocuments(context.TODO(), bson.M{})
	if e != nil {
		ez.LogToConsole(e.Error())
	}
	f, e := os.OpenFile(targetFile, os.O_CREATE|os.O_WRONLY|os.O_RDWR, 0666)
	if e != nil {
		ez.LogToConsole(e.Error())
	}
	defer f.Close()
	crud := xl.NewQuestionCrud()
	data := make([]xl.Question, 0)
	for i := 0; i <= int(total/1000); i++ {
		rows, e := crud.FindBy(ss.M{}, nil, i, 1000)
		if e != nil {
			ez.LogToConsole(e.Error())
		}
		for _, row := range rows {
			data = append(data, *row)
		}
	}
	content, e := json.Marshal(data)
	f.Write(content)
}
func TestBackUpQuestion(t *testing.T) {
	ez.DefaultInit()
	targetFile := fmt.Sprintf("./Question_%s.json", tools.GetDateYMDHIS("", "", "_"))
	BackUpQuestion(targetFile)
}
func RecoverQuestion(filePath string) {
	if filePath == "" {
		filePath = "./Question.json"
	}
	ez.DefaultInit()
	ez.DBMongo.Collection("Question").Drop(context.TODO())
	content := tools.ReadFile(filePath)
	data := make([]xl.Question, 0)
	e := json.Unmarshal(content, &data)
	if e != nil {
		ez.LogToConsole(e.Error())
	}
	for _, row := range data {
		e = row.Create()
		if e != nil {
			ez.LogToConsole(e.Error())
		}
	}
	ez.LogToConsole(fmt.Sprintf("写入数据%d条", len(data)))
}
func TestRecoverQuestion(t *testing.T) {
	filePath := "./Question_20240107_190550.json"
	ez.DefaultInit()
	RecoverQuestion(filePath)
}
