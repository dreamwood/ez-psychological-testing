package db

import (
	"context"
	"encoding/json"
	xl "ez/apps/xl/document"
	"fmt"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"gitee.com/dreamwood/ez-go/tools"
	"go.mongodb.org/mongo-driver/bson"
	"os"
	"testing"
)

func DropAnswer() {
	ez.DBMongo.Collection("Answer").Drop(context.TODO())
}
func TestDropAnswer(t *testing.T) {
	ez.DefaultInit()
	DropAnswer()
}
func BackUpAnswer(targetFile string) {
	if targetFile == "" {
		targetFile = "./Answer.json"
	}
	ez.DefaultInit()
	//计数
	total, e := ez.DBMongo.Collection("Answer").CountDocuments(context.TODO(), bson.M{})
	if e != nil {
		ez.LogToConsole(e.Error())
	}
	f, e := os.OpenFile(targetFile, os.O_CREATE|os.O_WRONLY|os.O_RDWR, 0666)
	if e != nil {
		ez.LogToConsole(e.Error())
	}
	defer f.Close()
	crud := xl.NewAnswerCrud()
	data := make([]xl.Answer, 0)
	for i := 0; i <= int(total/1000); i++ {
		rows, e := crud.FindBy(ss.M{}, nil, i, 1000)
		if e != nil {
			ez.LogToConsole(e.Error())
		}
		for _, row := range rows {
			data = append(data, *row)
		}
	}
	content, e := json.Marshal(data)
	f.Write(content)
}
func TestBackUpAnswer(t *testing.T) {
	ez.DefaultInit()
	targetFile := fmt.Sprintf("./Answer_%s.json", tools.GetDateYMDHIS("", "", "_"))
	BackUpAnswer(targetFile)
}
func RecoverAnswer(filePath string) {
	if filePath == "" {
		filePath = "./Answer.json"
	}
	ez.DefaultInit()
	ez.DBMongo.Collection("Answer").Drop(context.TODO())
	content := tools.ReadFile(filePath)
	data := make([]xl.Answer, 0)
	e := json.Unmarshal(content, &data)
	if e != nil {
		ez.LogToConsole(e.Error())
	}
	for _, row := range data {
		e = row.Create()
		if e != nil {
			ez.LogToConsole(e.Error())
		}
	}
	ez.LogToConsole(fmt.Sprintf("写入数据%d条", len(data)))
}
func TestRecoverAnswer(t *testing.T) {
	filePath := "./Answer_20240107_190550.json"
	ez.DefaultInit()
	RecoverAnswer(filePath)
}
