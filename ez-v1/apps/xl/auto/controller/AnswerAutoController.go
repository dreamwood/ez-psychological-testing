package controller

import (
	xl "ez/apps/xl/document"
	"ez/config/code"
	"ez/custom/cc"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ezc"
	"gitee.com/dreamwood/ez-go/ss"
)

type AnswerAutoController struct {
	ezc.BaseAdminController
}

func (c AnswerAutoController) AccessControl(session *ez.Session) {
	//session.StopHandle()
}
func (c AnswerAutoController) SaveAction(session *ez.Session) {
	this := cc.New(session)
	model := new(xl.Answer)
	if this.Try(this.FillJson(model)) {
		return
	}
	if ac := xl.NewAnswerAccessControl(model, "save", session); !ac.Access {
		this.ReturnError(code.ErrorAccess, ac.Message, "")
		return
	}
	model.SetEvent(true)
	model.SetFactoryParams(session)
	if this.Try(model.Save()) {
		return
	}
	this.ReturnSuccess("OK", model)
}
func (c AnswerAutoController) GetAction(session *ez.Session) {
	this := cc.New(session)
	model, err := xl.NewAnswerCrud(session).FindId(this.Get("id").IsInt64())
	if this.Try(err) {
		return
	}
	if ac := xl.NewAnswerAccessControl(model, "get", session); !ac.Access {
		this.ReturnError(code.ErrorAccess, ac.Message, "")
		return
	}
	this.ReturnSuccess("OK", model)
}
func (c AnswerAutoController) ChoiceAction(session *ez.Session) {
	this := cc.New(session)
	this.GetHttpQuery()
	list := make([]*xl.Answer, 0)
	err := xl.NewAnswerCrud(session).Factory.AggregateFind(&list, this.HttpQuery)
	if this.Try(err) {
		return
	}
	count, err := xl.NewAnswerCrud(session).Factory.AggregateCount(this.HttpQuery)
	if this.Try(err) {
		return
	}
	choices := make([]*ss.M, 0)
	for _, row := range list {
		choices = append(choices, row.MakeChoice())
	}
	this.ReturnSuccess("OK", ss.M{
		"lists": choices,
		"query": ss.M{"page": this.HttpQuery.Page, "limit": this.HttpQuery.Limit, "total": count},
	})
}
func (c AnswerAutoController) ListAction(session *ez.Session) {
	this := cc.New(session)
	this.GetHttpQuery()
	list := make([]*xl.Answer, 0)
	err := xl.NewAnswerCrud(session).Factory.AggregateFind(&list, this.HttpQuery)
	if this.Try(err) {
		return
	}
	count, err := xl.NewAnswerCrud(session).Factory.AggregateCount(this.HttpQuery)
	if this.Try(err) {
		return
	}
	this.ReturnSuccess("ok", ss.M{
		"lists": list,
		"query": ss.M{"page": this.HttpQuery.Page, "limit": this.HttpQuery.Limit, "total": count},
	})
}
func (c AnswerAutoController) DeleteAction(session *ez.Session) {
	this := cc.New(session)
	model, err := xl.NewAnswerCrud(session).FindId(this.Get("id").IsInt64())
	if this.Try(err) {
		return
	}
	if ac := xl.NewAnswerAccessControl(model, "delete", session); !ac.Access {
		this.ReturnError(code.ErrorAccess, ac.Message, "")
		return
	}
	model.SetEvent(true)
	if this.Try(model.Delete()) {
		return
	}
	this.ReturnSuccess("OK", model)
}
func (c AnswerAutoController) UnDeleteAction(session *ez.Session) {
	this := cc.New(session)
	model, err := xl.NewAnswerCrud(session).FindId(this.Get("id").IsInt64())
	if this.Try(err) {
		return
	}
	model.SetEvent(true)
	if this.Try(model.UnDelete()) {
		return
	}
	this.ReturnSuccess("OK", model)
}
func (c AnswerAutoController) DeleteManyAction(session *ez.Session) {
	this := cc.New(session)
	ids := ss.NewIds()
	if this.Try(this.FillJson(ids)) {
		return
	}
	list, err := xl.NewAnswerCrud(session).FindBy(ss.M{
		"id__in": ids.Ids,
	}, nil, 0, 0)
	if this.Try(err) {
		return
	}
	for _, row := range list {
		row.SetEvent(true)
		if ac := xl.NewAnswerAccessControl(row, "delete", session); !ac.Access {
			this.ReturnError(code.ErrorAccess, ac.Message, "")
			return
		}
		if this.Try(row.Delete()) {
			return
		}
	}
	this.ReturnSuccess("OK", "删除成功")
}
func (c AnswerAutoController) DestroyAction(session *ez.Session) {
	this := cc.New(session)
	model, err := xl.NewAnswerCrud(session).FindId(this.Get("id").IsInt64())
	if this.Try(err) {
		return
	}
	model.SetEvent(true)
	if this.Try(model.Destroy()) {
		return
	}
	this.ReturnSuccess("OK", model)
}
func (c AnswerAutoController) DestroyManyAction(session *ez.Session) {
	this := cc.New(session)
	ids := ss.NewIds()
	if this.Try(this.FillJson(ids)) {
		return
	}
	list, err := xl.NewAnswerCrud(session).FindBy(ss.M{
		"id__in": ids.Ids,
	}, nil, 0, 0)
	if this.Try(err) {
		return
	}
	for _, row := range list {
		if ac := xl.NewAnswerAccessControl(row, "destroy", session); !ac.Access {
			this.ReturnError(code.ErrorAccess, ac.Message, "")
			return
		}
		row.SetEvent(true)
		if this.Try(row.Destroy()) {
			return
		}
	}
	this.ReturnSuccess("OK", "销毁成功")
}
func (c AnswerAutoController) CopyAction(session *ez.Session) {
	this := cc.New(session)
	idVo := new(ss.DocIds)
	if this.Try(this.FillJson(idVo)) {
		return
	}
	for _, id := range idVo.Ids {
		model, err := xl.NewAnswerCrud(session).FindId(id)
		if this.Try(err) {
			return
		}
		if ac := xl.NewAnswerAccessControl(model, "copy", session); !ac.Access {
			this.ReturnError(code.ErrorAccess, ac.Message, "")
			return
		}
		model.Id = 0
		model.SetEvent(true)
		if this.Try(model.Save()) {
			return
		}
	}
	this.ReturnSuccess("OK", idVo)
}
func (c AnswerAutoController) UpdateAction(session *ez.Session) {
	this := cc.New(session)
	updater := ss.NewDocUpdater()
	if this.Try(this.FillJson(updater)) {
		return
	}
	//id, _ := primitive.ObjectIDFromHex(updater.Id)
	doc := &xl.Answer{Id: updater.Id}
	if ac := xl.NewAnswerAccessControl(doc, "update", session); !ac.Access {
		this.ReturnError(code.ErrorAccess, ac.Message, "")
		return
	}
	if this.Try(xl.NewAnswerCrud(session).Factory.Update(doc, updater.Model)) {
		return
	}
	this.ReturnSuccess("OK", updater)
}
func (c AnswerAutoController) UpdateManyAction(session *ez.Session) {
	this := cc.New(session)
	updater := ss.NewDocUpdater()
	if this.Try(this.FillJson(updater)) {
		return
	}
	for _, id := range updater.Ids {
		doc := &xl.Answer{Id: id}
		if ac := xl.NewAnswerAccessControl(doc, "update", session); !ac.Access {
			this.ReturnError(code.ErrorAccess, ac.Message, "")
			return
		}
		if this.Try(xl.NewAnswerCrud(session).Factory.Update(doc, updater.Model)) {
			return
		}
	}
	this.ReturnSuccess("OK", updater)
}
