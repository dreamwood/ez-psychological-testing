package mc

import (
	"gitee.com/dreamwood/ez-go/db/mgo"
)

const (
	AnswerEventNew          = "xl.AnswerNew"
	AnswerEventBeforeCreate = "xl.AnswerBeforeCreate"
	AnswerEventBeforeUpdate = "xl.AnswerBeforeUpdate"
	AnswerEventBeforeSave   = "xl.AnswerBeforeCreate xl.AnswerBeforeUpdate"
	AnswerEventAfterCreate  = "xl.AnswerAfterCreate"
	AnswerEventAfterUpdate  = "xl.AnswerAfterUpdate"
	AnswerEventAfterSave    = "xl.AnswerAfterCreate xl.AnswerAfterUpdate"
	AnswerEventDelete       = "xl.AnswerDelete"

	AnswerAccessControlEvent = "xl.AnswerAccessControl"
)

func GetAnswerConfig() *mgo.DocConfig {
	return Answer_Config
}

var Answer_Config *mgo.DocConfig

func init() {
	Answer_Config = NewAnswerConfig()
}

func NewAnswerConfig() *mgo.DocConfig {
	return &mgo.DocConfig{
		ContainerKey: "xl.Answer",
		Fields: []string{
			"question", "title", "scoreMin", "scoreMax",
		},
		RelationFields: []string{
			"question",
		},
		RelationConfigs: map[string]*mgo.DocRelation{

			"question": {
				Config:     GetQuestionConfig,
				DocName:    "Question",
				JoinType:   "O",
				KeyInside:  "questionId",
				KeyOutSide: "id",
			},
		},
		FieldFilter: map[string]*mgo.DocFieldFilter{
			"default": {
				Select: []string{},
				Omit:   []string{},
			},
		},
	}
}
