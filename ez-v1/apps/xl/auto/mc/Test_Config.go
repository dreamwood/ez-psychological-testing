package mc

import (
	"gitee.com/dreamwood/ez-go/db/mgo"
)

const (
	TestEventNew          = "xl.TestNew"
	TestEventBeforeCreate = "xl.TestBeforeCreate"
	TestEventBeforeUpdate = "xl.TestBeforeUpdate"
	TestEventBeforeSave   = "xl.TestBeforeCreate xl.TestBeforeUpdate"
	TestEventAfterCreate  = "xl.TestAfterCreate"
	TestEventAfterUpdate  = "xl.TestAfterUpdate"
	TestEventAfterSave    = "xl.TestAfterCreate xl.TestAfterUpdate"
	TestEventDelete       = "xl.TestDelete"

	TestAccessControlEvent = "xl.TestAccessControl"
)

func GetTestConfig() *mgo.DocConfig {
	return Test_Config
}

var Test_Config *mgo.DocConfig

func init() {
	Test_Config = NewTestConfig()
}

func NewTestConfig() *mgo.DocConfig {
	return &mgo.DocConfig{
		ContainerKey: "xl.Test",
		Fields: []string{
			"title", "short", "introduction", "note", "cover", "cat", "type", "question", "answer",
		},
		RelationFields: []string{
			"cat", "type", "question", "answer",
		},
		RelationConfigs: map[string]*mgo.DocRelation{

			"cat": {
				Config:     GetTestCategoryConfig,
				DocName:    "TestCategory",
				JoinType:   "O",
				KeyInside:  "catId",
				KeyOutSide: "id",
			},

			"type": {
				Config:     GetTestTypeConfig,
				DocName:    "TestType",
				JoinType:   "O",
				KeyInside:  "typeId",
				KeyOutSide: "id",
			},

			"question": {
				Config:     GetQuestionConfig,
				DocName:    "Question",
				JoinType:   "M",
				KeyInside:  "id",
				KeyOutSide: "testId",
			},

			"answer": {
				Config:     GetTestAnswerConfig,
				DocName:    "TestAnswer",
				JoinType:   "M",
				KeyInside:  "id",
				KeyOutSide: "testId",
			},
		},
		FieldFilter: map[string]*mgo.DocFieldFilter{
			"default": {
				Select: []string{},
				Omit:   []string{},
			},
		},
	}
}
