package mc

import (
	"gitee.com/dreamwood/ez-go/db/mgo"
)

const (
	QuestionEventNew          = "xl.QuestionNew"
	QuestionEventBeforeCreate = "xl.QuestionBeforeCreate"
	QuestionEventBeforeUpdate = "xl.QuestionBeforeUpdate"
	QuestionEventBeforeSave   = "xl.QuestionBeforeCreate xl.QuestionBeforeUpdate"
	QuestionEventAfterCreate  = "xl.QuestionAfterCreate"
	QuestionEventAfterUpdate  = "xl.QuestionAfterUpdate"
	QuestionEventAfterSave    = "xl.QuestionAfterCreate xl.QuestionAfterUpdate"
	QuestionEventDelete       = "xl.QuestionDelete"

	QuestionAccessControlEvent = "xl.QuestionAccessControl"
)

func GetQuestionConfig() *mgo.DocConfig {
	return Question_Config
}

var Question_Config *mgo.DocConfig

func init() {
	Question_Config = NewQuestionConfig()
}

func NewQuestionConfig() *mgo.DocConfig {
	return &mgo.DocConfig{
		ContainerKey: "xl.Question",
		Fields: []string{
			"test", "sort", "title", "isMulti", "note", "choice", "answer",
		},
		RelationFields: []string{
			"test", "choice", "answer",
		},
		RelationConfigs: map[string]*mgo.DocRelation{

			"test": {
				Config:     GetTestConfig,
				DocName:    "Test",
				JoinType:   "O",
				KeyInside:  "testId",
				KeyOutSide: "id",
			},

			"choice": {
				Config:     GetChoiceConfig,
				DocName:    "Choice",
				JoinType:   "M",
				KeyInside:  "id",
				KeyOutSide: "questionId",
			},

			"answer": {
				Config:     GetAnswerConfig,
				DocName:    "Answer",
				JoinType:   "M",
				KeyInside:  "id",
				KeyOutSide: "questionId",
			},
		},
		FieldFilter: map[string]*mgo.DocFieldFilter{
			"default": {
				Select: []string{},
				Omit:   []string{},
			},
		},
	}
}
