package mc

import (
	"gitee.com/dreamwood/ez-go/db/mgo"
)

const (
	TestCategoryEventNew          = "xl.TestCategoryNew"
	TestCategoryEventBeforeCreate = "xl.TestCategoryBeforeCreate"
	TestCategoryEventBeforeUpdate = "xl.TestCategoryBeforeUpdate"
	TestCategoryEventBeforeSave   = "xl.TestCategoryBeforeCreate xl.TestCategoryBeforeUpdate"
	TestCategoryEventAfterCreate  = "xl.TestCategoryAfterCreate"
	TestCategoryEventAfterUpdate  = "xl.TestCategoryAfterUpdate"
	TestCategoryEventAfterSave    = "xl.TestCategoryAfterCreate xl.TestCategoryAfterUpdate"
	TestCategoryEventDelete       = "xl.TestCategoryDelete"

	TestCategoryAccessControlEvent = "xl.TestCategoryAccessControl"
)

func GetTestCategoryConfig() *mgo.DocConfig {
	return TestCategory_Config
}

var TestCategory_Config *mgo.DocConfig

func init() {
	TestCategory_Config = NewTestCategoryConfig()
}

func NewTestCategoryConfig() *mgo.DocConfig {
	return &mgo.DocConfig{
		ContainerKey: "xl.TestCategory",
		Fields: []string{
			"name", "cover", "sort",
		},
		RelationFields:  []string{},
		RelationConfigs: map[string]*mgo.DocRelation{},
		FieldFilter: map[string]*mgo.DocFieldFilter{
			"default": {
				Select: []string{},
				Omit:   []string{},
			},
		},
	}
}
