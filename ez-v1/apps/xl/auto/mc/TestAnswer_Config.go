package mc

import (
	"gitee.com/dreamwood/ez-go/db/mgo"
)

const (
	TestAnswerEventNew          = "xl.TestAnswerNew"
	TestAnswerEventBeforeCreate = "xl.TestAnswerBeforeCreate"
	TestAnswerEventBeforeUpdate = "xl.TestAnswerBeforeUpdate"
	TestAnswerEventBeforeSave   = "xl.TestAnswerBeforeCreate xl.TestAnswerBeforeUpdate"
	TestAnswerEventAfterCreate  = "xl.TestAnswerAfterCreate"
	TestAnswerEventAfterUpdate  = "xl.TestAnswerAfterUpdate"
	TestAnswerEventAfterSave    = "xl.TestAnswerAfterCreate xl.TestAnswerAfterUpdate"
	TestAnswerEventDelete       = "xl.TestAnswerDelete"

	TestAnswerAccessControlEvent = "xl.TestAnswerAccessControl"
)

func GetTestAnswerConfig() *mgo.DocConfig {
	return TestAnswer_Config
}

var TestAnswer_Config *mgo.DocConfig

func init() {
	TestAnswer_Config = NewTestAnswerConfig()
}

func NewTestAnswerConfig() *mgo.DocConfig {
	return &mgo.DocConfig{
		ContainerKey: "xl.TestAnswer",
		Fields: []string{
			"test", "scoreMin", "scoreMax", "content",
		},
		RelationFields: []string{
			"test",
		},
		RelationConfigs: map[string]*mgo.DocRelation{

			"test": {
				Config:     GetTestConfig,
				DocName:    "Test",
				JoinType:   "O",
				KeyInside:  "testId",
				KeyOutSide: "id",
			},
		},
		FieldFilter: map[string]*mgo.DocFieldFilter{
			"default": {
				Select: []string{},
				Omit:   []string{},
			},
		},
	}
}
