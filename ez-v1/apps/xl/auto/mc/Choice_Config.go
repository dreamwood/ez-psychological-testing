package mc

import (
	"gitee.com/dreamwood/ez-go/db/mgo"
)

const (
	ChoiceEventNew          = "xl.ChoiceNew"
	ChoiceEventBeforeCreate = "xl.ChoiceBeforeCreate"
	ChoiceEventBeforeUpdate = "xl.ChoiceBeforeUpdate"
	ChoiceEventBeforeSave   = "xl.ChoiceBeforeCreate xl.ChoiceBeforeUpdate"
	ChoiceEventAfterCreate  = "xl.ChoiceAfterCreate"
	ChoiceEventAfterUpdate  = "xl.ChoiceAfterUpdate"
	ChoiceEventAfterSave    = "xl.ChoiceAfterCreate xl.ChoiceAfterUpdate"
	ChoiceEventDelete       = "xl.ChoiceDelete"

	ChoiceAccessControlEvent = "xl.ChoiceAccessControl"
)

func GetChoiceConfig() *mgo.DocConfig {
	return Choice_Config
}

var Choice_Config *mgo.DocConfig

func init() {
	Choice_Config = NewChoiceConfig()
}

func NewChoiceConfig() *mgo.DocConfig {
	return &mgo.DocConfig{
		ContainerKey: "xl.Choice",
		Fields: []string{
			"question", "sort", "title", "score", "analyze",
		},
		RelationFields: []string{
			"question",
		},
		RelationConfigs: map[string]*mgo.DocRelation{

			"question": {
				Config:     GetQuestionConfig,
				DocName:    "Question",
				JoinType:   "O",
				KeyInside:  "questionId",
				KeyOutSide: "id",
			},
		},
		FieldFilter: map[string]*mgo.DocFieldFilter{
			"default": {
				Select: []string{},
				Omit:   []string{},
			},
		},
	}
}
