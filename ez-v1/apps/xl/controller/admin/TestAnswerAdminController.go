package controller

import (
	"ez/apps/xl/auto/controller"
	"gitee.com/dreamwood/ez-go/ez"
)

type TestAnswerController struct {
	controller.TestAnswerAutoController
}

func init() {
	c := &TestAnswerController{}
	c.SetRouteParam("/xl", "/admin", "TestAnswer")
	ez.CreateApi(c.AdminGet(), c.GetAction).SetRouterName("[xl]整卷解析_创建")
	ez.CreateApi(c.AdminList(), c.ListAction).SetRouterName("[xl]整卷解析_列表")
	ez.CreateApi(c.AdminSave(), c.SaveAction).SetRouterName("[xl]整卷解析_保存")
	ez.CreateApi(c.AdminCopy(), c.CopyAction).SetRouterName("[xl]整卷解析_复制")
	ez.CreateApi(c.AdminUpdate(), c.UpdateAction).SetRouterName("[xl]整卷解析_更新")
	ez.CreateApi(c.AdminChoice(), c.ChoiceAction).SetRouterName("[xl]整卷解析_选项")
	ez.CreateApi(c.AdminDelete(), c.DeleteAction).SetRouterName("[xl]整卷解析_删除")
	ez.CreateApi(c.AdminUnDelete(), c.UnDeleteAction).SetRouterName("[xl]整卷解析_恢复")
	ez.CreateApi(c.AdminDestroy(), c.DestroyAction).SetRouterName("[xl]整卷解析_销毁")
	ez.CreateApi(c.AdminUpdateMany(), c.UpdateManyAction).SetRouterName("[xl]整卷解析_更新_批量")
	ez.CreateApi(c.AdminDeleteMany(), c.DeleteManyAction).SetRouterName("[xl]整卷解析_删除_批量")
	ez.CreateApi(c.AdminDestroyMany(), c.DestroyManyAction).SetRouterName("[xl]整卷解析_销毁_批量")

}

//func (c TestAnswerController) AccessControl(session *ez.Session) { /* 在这里面重构 */ }
//func (c TestAnswerController) GetAction(session *ez.Session) { /* 在这里面重构 */ }
//func (c TestAnswerController) ListAction(session *ez.Session) { /* 在这里面重构 */ }
