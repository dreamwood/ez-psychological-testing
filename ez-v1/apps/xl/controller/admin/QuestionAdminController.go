package controller

import (
	"ez/apps/xl/auto/controller"
	"gitee.com/dreamwood/ez-go/ez"
)

type QuestionController struct {
	controller.QuestionAutoController
}

func init() {
	c := &QuestionController{}
	c.SetRouteParam("/xl", "/admin", "Question")
	ez.CreateApi(c.AdminGet(), c.GetAction).SetRouterName("[xl]问题_创建")
	ez.CreateApi(c.AdminList(), c.ListAction).SetRouterName("[xl]问题_列表")
	ez.CreateApi(c.AdminSave(), c.SaveAction).SetRouterName("[xl]问题_保存")
	ez.CreateApi(c.AdminCopy(), c.CopyAction).SetRouterName("[xl]问题_复制")
	ez.CreateApi(c.AdminUpdate(), c.UpdateAction).SetRouterName("[xl]问题_更新")
	ez.CreateApi(c.AdminChoice(), c.ChoiceAction).SetRouterName("[xl]问题_选项")
	ez.CreateApi(c.AdminDelete(), c.DeleteAction).SetRouterName("[xl]问题_删除")
	ez.CreateApi(c.AdminUnDelete(), c.UnDeleteAction).SetRouterName("[xl]问题_恢复")
	ez.CreateApi(c.AdminDestroy(), c.DestroyAction).SetRouterName("[xl]问题_销毁")
	ez.CreateApi(c.AdminUpdateMany(), c.UpdateManyAction).SetRouterName("[xl]问题_更新_批量")
	ez.CreateApi(c.AdminDeleteMany(), c.DeleteManyAction).SetRouterName("[xl]问题_删除_批量")
	ez.CreateApi(c.AdminDestroyMany(), c.DestroyManyAction).SetRouterName("[xl]问题_销毁_批量")

}

//func (c QuestionController) AccessControl(session *ez.Session) { /* 在这里面重构 */ }
//func (c QuestionController) GetAction(session *ez.Session) { /* 在这里面重构 */ }
//func (c QuestionController) ListAction(session *ez.Session) { /* 在这里面重构 */ }
