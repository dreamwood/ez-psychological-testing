package controller

import (
	"ez/apps/xl/auto/controller"
	xl "ez/apps/xl/document"
	"ez/custom/cc"
	"ez/plugin/excel"
	"fmt"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
)

type TestController struct {
	controller.TestAutoController
}

func init() {
	c := &TestController{}
	c.SetRouteParam("/xl", "/admin", "Test")
	ez.CreateApi(c.AdminGet(), c.GetAction).SetRouterName("[xl]心理测试_创建")
	ez.CreateApi(c.AdminList(), c.ListAction).SetRouterName("[xl]心理测试_列表")
	ez.CreateApi(c.AdminSave(), c.SaveAction).SetRouterName("[xl]心理测试_保存")
	ez.CreateApi(c.AdminCopy(), c.CopyAction).SetRouterName("[xl]心理测试_复制")
	ez.CreateApi(c.AdminUpdate(), c.UpdateAction).SetRouterName("[xl]心理测试_更新")
	ez.CreateApi(c.AdminChoice(), c.ChoiceAction).SetRouterName("[xl]心理测试_选项")
	ez.CreateApi(c.AdminDelete(), c.DeleteAction).SetRouterName("[xl]心理测试_删除")
	ez.CreateApi(c.AdminUnDelete(), c.UnDeleteAction).SetRouterName("[xl]心理测试_恢复")
	ez.CreateApi(c.AdminDestroy(), c.DestroyAction).SetRouterName("[xl]心理测试_销毁")
	ez.CreateApi(c.AdminUpdateMany(), c.UpdateManyAction).SetRouterName("[xl]心理测试_更新_批量")
	ez.CreateApi(c.AdminDeleteMany(), c.DeleteManyAction).SetRouterName("[xl]心理测试_删除_批量")
	ez.CreateApi(c.AdminDestroyMany(), c.DestroyManyAction).SetRouterName("[xl]心理测试_销毁_批量")
	ez.CreateApi(c.AdminApi("/Test/submit"), c.SubmitAction).SetRouterName("[xl]心理测试_提交")
	ez.CreateApi(c.AdminApi("/Test/import"), c.ImportAction).SetRouterName("[xl]心理测试_导入")

}

//func (c TestController) AccessControl(session *ez.Session) { /* 在这里面重构 */ }
//func (c TestController) GetAction(session *ez.Session) { /* 在这里面重构 */ }
//func (c TestController) ListAction(session *ez.Session) { /* 在这里面重构 */ }

type submitter struct {
	Test    int64 `json:"test"` //测试ID
	Answers []struct {
		Question int64   `json:"question"`
		Choice   []int64 `json:"choice"`
	} `json:"answers"`
}

func (c TestController) SubmitAction(session *ez.Session) {
	this := cc.New(session)
	rst := new(submitter)
	if this.Try(this.FillJson(&rst)) {
		return
	}
	this.ReturnSuccess("OK", rst)
}

type importer struct {
	Test int64 `json:"test"` //测试ID
	File struct {
		Name string `json:"name"`
		Size string `json:"size"`
		Url  string `json:"url"`
	} `json:"file"`
}

func (c TestController) ImportAction(session *ez.Session) {
	this := cc.New(session)
	rst := new(importer)
	if this.Try(this.FillJson(&rst)) {
		return
	}
	expt := excel.NewImporter()
	if this.Try(expt.LoadFile(rst.File.Url)) {
		return
	}
	expt.Sheet = "批量导入"
	text := expt.GetText("B3")
	choiceStart := 'C'
	analyzeStart := 'K'
	choiceMaxCount := 8
	startRow := 2
	crudQuestion := xl.NewQuestionCrud()

	for {
		println("startRow", startRow)
		startRow++
		questionType := expt.GetText(fmt.Sprintf("A%d", startRow))
		isMulti := false
		done := false
		switch questionType {
		case "单选题":
		case "多选题":
			isMulti = true
		default:
			done = true
		}
		if done {
			break
		}
		questionTitle := expt.GetText(fmt.Sprintf("B%d", startRow))
		question, _ := crudQuestion.FindOneBy(ss.M{"title": questionTitle}, nil)
		if question.Id > 0 {
			continue
		}
		question.TestId = rst.Test
		question.Title = questionTitle
		question.IsMulti = isMulti
		if this.Try(question.Save()) {
			return
		}
		println("question", question.Id)
		ez.JsonLog(question)
		for i := 0; i < choiceMaxCount; i++ {
			choiceModel := new(xl.Choice)
			//choice := expt.GetText(string(choiceStart+rune(i)) + "3")
			choice := expt.GetText(fmt.Sprintf("%s%d", string(choiceStart+rune(i)), startRow))
			if choice == "" {
				break
			}
			choiceModel.Title = choice
			analyze := expt.GetText(fmt.Sprintf("%s%d", string(analyzeStart+rune(i)), startRow))
			choiceModel.Analyze = analyze
			choiceModel.Score = float64(i)
			choiceModel.Sort = int64(i)
			choiceModel.QuestionId = question.Id
			if this.Try(choiceModel.Save()) {
				return
			}
		}
	}
	this.ReturnSuccess(text, rst)
}
