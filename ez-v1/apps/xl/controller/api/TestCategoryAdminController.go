package api

import (
	"ez/apps/xl/auto/controller"
	"gitee.com/dreamwood/ez-go/ez"
)

type TestCategoryController struct {
	controller.TestCategoryAutoController
}

func init() {
	c := &TestCategoryController{}
	c.SetRouteParam("/xl", "/api", "TestCategory")
	ez.CreateApi(c.AdminGet(), c.GetAction).SetRouterName("[xl]测试分类_创建")
	ez.CreateApi(c.AdminList(), c.ListAction).SetRouterName("[xl]测试分类_列表")
	ez.CreateApi(c.AdminSave(), c.SaveAction).SetRouterName("[xl]测试分类_保存")
	ez.CreateApi(c.AdminCopy(), c.CopyAction).SetRouterName("[xl]测试分类_复制")
	ez.CreateApi(c.AdminUpdate(), c.UpdateAction).SetRouterName("[xl]测试分类_更新")
	ez.CreateApi(c.AdminChoice(), c.ChoiceAction).SetRouterName("[xl]测试分类_选项")
	ez.CreateApi(c.AdminDelete(), c.DeleteAction).SetRouterName("[xl]测试分类_删除")
	ez.CreateApi(c.AdminUnDelete(), c.UnDeleteAction).SetRouterName("[xl]测试分类_恢复")
	ez.CreateApi(c.AdminDestroy(), c.DestroyAction).SetRouterName("[xl]测试分类_销毁")
	ez.CreateApi(c.AdminUpdateMany(), c.UpdateManyAction).SetRouterName("[xl]测试分类_更新_批量")
	ez.CreateApi(c.AdminDeleteMany(), c.DeleteManyAction).SetRouterName("[xl]测试分类_删除_批量")
	ez.CreateApi(c.AdminDestroyMany(), c.DestroyManyAction).SetRouterName("[xl]测试分类_销毁_批量")

}

//func (c TestCategoryController) AccessControl(session *ez.Session) { /* 在这里面重构 */ }
//func (c TestCategoryController) GetAction(session *ez.Session) { /* 在这里面重构 */ }
//func (c TestCategoryController) ListAction(session *ez.Session) { /* 在这里面重构 */ }
