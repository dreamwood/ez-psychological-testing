package api

import (
	"ez/apps/xl/auto/controller"
	"gitee.com/dreamwood/ez-go/ez"
)

type AnswerController struct {
	controller.AnswerAutoController
}

func init() {
	c := &AnswerController{}
	c.SetRouteParam("/xl", "/api", "Answer")
	ez.CreateApi(c.AdminGet(), c.GetAction).SetRouterName("[xl]解析_创建")
	ez.CreateApi(c.AdminList(), c.ListAction).SetRouterName("[xl]解析_列表")
	ez.CreateApi(c.AdminSave(), c.SaveAction).SetRouterName("[xl]解析_保存")
	ez.CreateApi(c.AdminCopy(), c.CopyAction).SetRouterName("[xl]解析_复制")
	ez.CreateApi(c.AdminUpdate(), c.UpdateAction).SetRouterName("[xl]解析_更新")
	ez.CreateApi(c.AdminChoice(), c.ChoiceAction).SetRouterName("[xl]解析_选项")
	ez.CreateApi(c.AdminDelete(), c.DeleteAction).SetRouterName("[xl]解析_删除")
	ez.CreateApi(c.AdminUnDelete(), c.UnDeleteAction).SetRouterName("[xl]解析_恢复")
	ez.CreateApi(c.AdminDestroy(), c.DestroyAction).SetRouterName("[xl]解析_销毁")
	ez.CreateApi(c.AdminUpdateMany(), c.UpdateManyAction).SetRouterName("[xl]解析_更新_批量")
	ez.CreateApi(c.AdminDeleteMany(), c.DeleteManyAction).SetRouterName("[xl]解析_删除_批量")
	ez.CreateApi(c.AdminDestroyMany(), c.DestroyManyAction).SetRouterName("[xl]解析_销毁_批量")

}

//func (c AnswerController) AccessControl(session *ez.Session) { /* 在这里面重构 */ }
//func (c AnswerController) GetAction(session *ez.Session) { /* 在这里面重构 */ }
//func (c AnswerController) ListAction(session *ez.Session) { /* 在这里面重构 */ }
