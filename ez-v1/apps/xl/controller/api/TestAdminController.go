package api

import (
	"ez/apps/xl/auto/controller"
	"gitee.com/dreamwood/ez-go/ez"
)

type TestController struct {
	controller.TestAutoController
}

func init() {
	c := &TestController{}
	c.SetRouteParam("/xl", "/api", "Test")
	ez.CreateApi(c.AdminGet(), c.GetAction).SetRouterName("[xl]心理测试_创建")
	ez.CreateApi(c.AdminList(), c.ListAction).SetRouterName("[xl]心理测试_列表")
	ez.CreateApi(c.AdminSave(), c.SaveAction).SetRouterName("[xl]心理测试_保存")
	ez.CreateApi(c.AdminCopy(), c.CopyAction).SetRouterName("[xl]心理测试_复制")
	ez.CreateApi(c.AdminUpdate(), c.UpdateAction).SetRouterName("[xl]心理测试_更新")
	ez.CreateApi(c.AdminChoice(), c.ChoiceAction).SetRouterName("[xl]心理测试_选项")
	ez.CreateApi(c.AdminDelete(), c.DeleteAction).SetRouterName("[xl]心理测试_删除")
	ez.CreateApi(c.AdminUnDelete(), c.UnDeleteAction).SetRouterName("[xl]心理测试_恢复")
	ez.CreateApi(c.AdminDestroy(), c.DestroyAction).SetRouterName("[xl]心理测试_销毁")
	ez.CreateApi(c.AdminUpdateMany(), c.UpdateManyAction).SetRouterName("[xl]心理测试_更新_批量")
	ez.CreateApi(c.AdminDeleteMany(), c.DeleteManyAction).SetRouterName("[xl]心理测试_删除_批量")
	ez.CreateApi(c.AdminDestroyMany(), c.DestroyManyAction).SetRouterName("[xl]心理测试_销毁_批量")

}

// func (c TestController) AccessControl(session   *ez.Session) { /* 在这里面重构 */ }
// func (c TestController) GetAction(session *ez.Session) { /* 在这里面重构 */ }
// func (c TestController) ListAction(session *ez.Session) { /* 在这里面重构 */ }
