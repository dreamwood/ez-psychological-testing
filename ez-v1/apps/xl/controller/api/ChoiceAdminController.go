package api

import (
	"ez/apps/xl/auto/controller"
	"gitee.com/dreamwood/ez-go/ez"
)

type ChoiceController struct {
	controller.ChoiceAutoController
}

func init() {
	c := &ChoiceController{}
	c.SetRouteParam("/xl", "/api", "Choice")
	ez.CreateApi(c.AdminGet(), c.GetAction).SetRouterName("[xl]选项_创建")
	ez.CreateApi(c.AdminList(), c.ListAction).SetRouterName("[xl]选项_列表")
	ez.CreateApi(c.AdminSave(), c.SaveAction).SetRouterName("[xl]选项_保存")
	ez.CreateApi(c.AdminCopy(), c.CopyAction).SetRouterName("[xl]选项_复制")
	ez.CreateApi(c.AdminUpdate(), c.UpdateAction).SetRouterName("[xl]选项_更新")
	ez.CreateApi(c.AdminChoice(), c.ChoiceAction).SetRouterName("[xl]选项_选项")
	ez.CreateApi(c.AdminDelete(), c.DeleteAction).SetRouterName("[xl]选项_删除")
	ez.CreateApi(c.AdminUnDelete(), c.UnDeleteAction).SetRouterName("[xl]选项_恢复")
	ez.CreateApi(c.AdminDestroy(), c.DestroyAction).SetRouterName("[xl]选项_销毁")
	ez.CreateApi(c.AdminUpdateMany(), c.UpdateManyAction).SetRouterName("[xl]选项_更新_批量")
	ez.CreateApi(c.AdminDeleteMany(), c.DeleteManyAction).SetRouterName("[xl]选项_删除_批量")
	ez.CreateApi(c.AdminDestroyMany(), c.DestroyManyAction).SetRouterName("[xl]选项_销毁_批量")

}

//func (c ChoiceController) AccessControl(session *ez.Session) { /* 在这里面重构 */ }
//func (c ChoiceController) GetAction(session *ez.Session) { /* 在这里面重构 */ }
//func (c ChoiceController) ListAction(session *ez.Session) { /* 在这里面重构 */ }
