package gen

import (
	"gitee.com/dreamwood/ez-go/maker"
	"testing"
)

func TestTest(t *testing.T) {
	doc := maker.CreateDoc("Test", "心理测试", "xl")
	doc.Add("title", "问题标题").IsString()
	doc.Add("short", "简介").IsString()
	doc.Add("introduction", "详细书名").IsString()
	doc.Add("note", "备注").IsString()
	doc.Add("cover", "封面").IsAny("ss.M")
	doc.Add("cat", "分类").IsJoinM2O("TestCategory")
	doc.Add("type", "测试类型").IsJoinM2O("TestType")
	doc.Add("question", "题目").IsJoinO2M("Question", "id", "testId")
	doc.Add("answer", "解析").IsJoinO2M("TestAnswer", "id", "testId")
	doc.Generate()
}

func TestTestAnswer(t *testing.T) {
	doc := maker.CreateDoc("TestAnswer", "整卷解析", "xl")
	doc.Add("test", "所属测试").IsJoinM2O("Test")
	doc.Add("scoreMin", "分数下限").IsFloat()
	doc.Add("scoreMax", "分数上限").IsFloat()
	doc.Add("content", "解析").IsString()
	doc.Generate()
}

func TestTestCategory(t *testing.T) {
	doc := maker.CreateDoc("TestCategory", "测试分类", "xl")
	doc.Add("name", "分类名称").IsString()
	doc.Add("cover", "封面").IsAny("ss.M")
	doc.Add("sort", "排序").IsInt()
	doc.Generate()
}

func TestTestType(t *testing.T) {
	doc := maker.CreateDoc("TestType", "测试类型", "xl")
	doc.Add("name", "类型名称").IsString()
	doc.Add("code", "类型编码").IsString()
	doc.Generate()
}

func TestQuestion(t *testing.T) {
	doc := maker.CreateDoc("Question", "问题", "xl")
	doc.Add("test", "所属测试").IsJoinM2O("Test")
	doc.Add("sort", "排序").IsInt()
	doc.Add("title", "问题标题").IsString()
	doc.Add("isMulti", "是否多选").IsBool()
	doc.Add("note", "备注").IsString()
	doc.Add("choice", "选项").IsJoinO2M("Choice", "id", "questionId")
	doc.Add("answer", "解析").IsJoinO2M("Answer", "id", "questionId")
	doc.Generate()
}
func TestChoice(t *testing.T) {
	doc := maker.CreateDoc("Choice", "选项", "xl")
	doc.Add("question", "所属题目").IsJoinM2O("Question")
	doc.Add("sort", "排序").IsInt()
	doc.Add("title", "选项内容").IsString()
	doc.Add("score", "分数").IsFloat()
	doc.Add("analyze", "选项分析").IsString()

	doc.Generate()
}
func TestAnswer(t *testing.T) {
	doc := maker.CreateDoc("Answer", "解析", "xl")
	doc.Add("question", "所属题目").IsJoinM2O("Question")
	doc.Add("title", "解析").IsString()
	doc.Add("scoreMin", "分数下限").IsFloat()
	doc.Add("scoreMax", "分数上限").IsFloat()
	doc.Generate()
}
