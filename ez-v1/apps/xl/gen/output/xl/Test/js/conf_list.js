import listBuilder from "@/comps/list/listBuilder";

/*
lb.add("title","问题标题",80)
lb.add("short","简介",80)
lb.add("introduction","详细书名",80)
lb.add("note","备注",80)
lb.add("cover","封面",80)
lb.add("cat","分类",80)
lb.add("type","测试类型",80)
lb.add("question","题目",80)
lb.add("answer","解析",80)

*/
export default function () {
    let lb = listBuilder()
    lb.add("id","#",80)
    lb.addAction(120).setAlignRight()
    return lb.headers
}