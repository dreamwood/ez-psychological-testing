import http from "@/assets/js/http";

export default {
    urlFind: '/xl/admin/Test/get',
    urlFindToEdit: '/xl/admin/Test/get',
    urlFindBy: '/xl/admin/Test/list',
    urlSave: '/xl/admin/Test/save',
    //urlDelete: '/xl/admin/Test/delete',
    //urlDeleteAll: '/xl/admin/Test/delete_many',
    urlDelete: '/xl/admin/Test/destroy',
    urlDeleteAll: '/xl/admin/Test/destroy_many',
    urlCopy: '/xl/admin/Test/copy',
    urlEditMany: '/xl/admin/Test/edit_many',
    urlTree: '/xl/admin/Test/tree',
    urlChoice: '/xl/admin/Test/choice',
    find(id, func) {
        http.get(this.urlFind, {id}, res => {
            func(res)
        })
    },
    findToEdit(id, func) {
        http.get(this.urlFindToEdit, {id}, res => {
            func(res)
        })
    },
    findBy(param, func) {
        http.post(this.urlFindBy, param, res => {
            func(res.data.lists, res.data.query)
        })
    },
    save(data, func) {
        http.post(this.urlSave, data, res => {
            func(res)
        })
    },
    delete(id, func) {
        http.get(this.urlDelete, {id}, res => {
            func(res)
        })
    },
    deleteAll(ids, func) {
        http.post(this.urlDeleteAll, {ids}, res => {
            func(res)
        })
    },
    copy(ids, func) {
        http.post(this.urlCopy, {ids}, res => {
            func(res)
        })
    },
    editMany(ids, data, func) {
        http.post(this.urlEditMany, {
            ids,
            _action: data
        }, res => {
            func(res)
        })
    },
}