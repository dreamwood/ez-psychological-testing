import ezAuth from "@/assets/js/ez-auth";

export default {
    data() {
        return {
            auth_add: ezAuth.reg("+xl.TestAnswer.add", "整卷解析_新增"),//新增
            auth_copy: ezAuth.reg("+xl.TestAnswer.copy", "整卷解析_复制"),//复制
            auth_edit: ezAuth.reg("+xl.TestAnswer.edit", "整卷解析_编辑"),//编辑
            auth_del: ezAuth.reg("-xl.TestAnswer.del", "整卷解析_删除"),//删除
            auth_view: ezAuth.reg("+xl.TestAnswer.view", "整卷解析_查看"),//查看
        }
    },
    directives: {
        auth: {
            bind: ($el, binding, vnode, prevVnode) => {
                ezAuth.checkAllow(binding.value) ? $el.style.display = "normal" : $el.style.display = "none"
            }
        }
    }
}