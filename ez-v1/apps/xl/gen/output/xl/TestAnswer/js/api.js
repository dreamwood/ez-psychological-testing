import http from "@/assets/js/http";

export default {
    urlFind: '/xl/admin/TestAnswer/get',
    urlFindToEdit: '/xl/admin/TestAnswer/get',
    urlFindBy: '/xl/admin/TestAnswer/list',
    urlSave: '/xl/admin/TestAnswer/save',
    //urlDelete: '/xl/admin/TestAnswer/delete',
    //urlDeleteAll: '/xl/admin/TestAnswer/delete_many',
    urlDelete: '/xl/admin/TestAnswer/destroy',
    urlDeleteAll: '/xl/admin/TestAnswer/destroy_many',
    urlCopy: '/xl/admin/TestAnswer/copy',
    urlEditMany: '/xl/admin/TestAnswer/edit_many',
    urlTree: '/xl/admin/TestAnswer/tree',
    urlChoice: '/xl/admin/TestAnswer/choice',
    find(id, func) {
        http.get(this.urlFind, {id}, res => {
            func(res)
        })
    },
    findToEdit(id, func) {
        http.get(this.urlFindToEdit, {id}, res => {
            func(res)
        })
    },
    findBy(param, func) {
        http.post(this.urlFindBy, param, res => {
            func(res.data.lists, res.data.query)
        })
    },
    save(data, func) {
        http.post(this.urlSave, data, res => {
            func(res)
        })
    },
    delete(id, func) {
        http.get(this.urlDelete, {id}, res => {
            func(res)
        })
    },
    deleteAll(ids, func) {
        http.post(this.urlDeleteAll, {ids}, res => {
            func(res)
        })
    },
    copy(ids, func) {
        http.post(this.urlCopy, {ids}, res => {
            func(res)
        })
    },
    editMany(ids, data, func) {
        http.post(this.urlEditMany, {
            ids,
            _action: data
        }, res => {
            func(res)
        })
    },
}