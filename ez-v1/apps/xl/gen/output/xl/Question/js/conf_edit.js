import formBuilder from "@/comps/form/formBuilder";
import api from "./api";
/*
fb.addText("test","所属测试",3)
fb.addText("sort","排序",3)
fb.addText("title","问题标题",3)
fb.addText("isMulti","是否多选",3)
fb.addText("note","备注",3)
fb.addText("choice","选项",3)
fb.addText("answer","解析",3)

* */
export default {
    main:function () {
        let fb = formBuilder()
        fb.setLabelWidth(100)

        fb.addText("name").setSpan(3).setLabel("姓名")

        fb.addText("sort").setSpan(3).setLabel("排序").setTypeNumber()

        return fb
    },
    tree:function () {
        let fb = formBuilder()
        fb.setLabelWidth(100)

        fb.addCascade("parentId").setLabel("上级分类").setSpan(3)
            .setApi(api.urlTree)

        fb.addText("name").setSpan(6).setLabel("分类名称")
            .setRules([fb.R.notNull("名称不能为空")])

        fb.addText("sort").setSpan(3).setLabel("排序").setTypeNumber()
        return fb
    },
    other:function () {
        let fb = formBuilder()
        fb.setLabelWidth(100).setLabelPosition("right")

        return fb
    }
}