import listBuilder from "@/comps/list/listBuilder";

/*
lb.add("test","所属测试",80)
lb.add("sort","排序",80)
lb.add("title","问题标题",80)
lb.add("isMulti","是否多选",80)
lb.add("note","备注",80)
lb.add("choice","选项",80)
lb.add("answer","解析",80)

*/
export default function () {
    let lb = listBuilder()
    lb.add("id","#",80)
    lb.addAction(120).setAlignRight()
    return lb.headers
}