import listBuilder from "@/comps/list/listBuilder";

/*
lb.add("question","所属题目",80)
lb.add("sort","排序",80)
lb.add("title","选项内容",80)
lb.add("score","分数",80)
lb.add("analyze","选项分析",80)

*/
export default function () {
    let lb = listBuilder()
    lb.add("id","#",80)
    lb.addAction(120).setAlignRight()
    return lb.headers
}