import formBuilder from "@/comps/form/formBuilder";
import api from "./api";
/*
fb.addText("question","所属题目",3)
fb.addText("sort","排序",3)
fb.addText("title","选项内容",3)
fb.addText("score","分数",3)
fb.addText("analyze","选项分析",3)

* */
export default {
    main:function () {
        let fb = formBuilder()
        fb.setLabelWidth(100)

        fb.addText("name").setSpan(3).setLabel("姓名")

        fb.addText("sort").setSpan(3).setLabel("排序").setTypeNumber()

        return fb
    },
    tree:function () {
        let fb = formBuilder()
        fb.setLabelWidth(100)

        fb.addCascade("parentId").setLabel("上级分类").setSpan(3)
            .setApi(api.urlTree)

        fb.addText("name").setSpan(6).setLabel("分类名称")
            .setRules([fb.R.notNull("名称不能为空")])

        fb.addText("sort").setSpan(3).setLabel("排序").setTypeNumber()
        return fb
    },
    other:function () {
        let fb = formBuilder()
        fb.setLabelWidth(100).setLabelPosition("right")

        return fb
    }
}