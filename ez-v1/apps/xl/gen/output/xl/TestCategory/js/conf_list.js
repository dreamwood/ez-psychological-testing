import listBuilder from "@/comps/list/listBuilder";

/*
lb.add("name","分类名称",80)
lb.add("cover","封面",80)
lb.add("sort","排序",80)

*/
export default function () {
    let lb = listBuilder()
    lb.add("id","#",80)
    lb.addAction(120).setAlignRight()
    return lb.headers
}