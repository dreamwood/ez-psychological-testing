package xl

import (
	"context"
	"encoding/json"
	"ez/apps/xl/auto/mc"
	"gitee.com/dreamwood/ez-go/db/mgo"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"time"
)

type Test struct {
	mgo.BaseDoc `bson:"-" json:"-"`
	Id          int64 `json:"id" bson:"id,omitempty"`

	Title string `json:"title" bson:"title"` //问题标题

	Short string `json:"short" bson:"short"` //简介

	Introduction string `json:"introduction" bson:"introduction"` //详细书名

	Note string `json:"note" bson:"note"` //备注

	Cover ss.M `json:"cover" bson:"cover"` //封面

	Cat *TestCategory `json:"cat" bson:"cat"` //分类

	CatId int64 `json:"catId" bson:"catId"` //分类

	Type *TestType `json:"type" bson:"type"` //测试类型

	TypeId int64 `json:"typeId" bson:"typeId"` //测试类型

	Question []*Question `json:"question" bson:"question"` //题目

	Answer []*TestAnswer `json:"answer" bson:"answer"` //解析

	CreateAt time.Time  `json:"createAt" bson:"createAt"`
	UpdateAt time.Time  `json:"updateAt" bson:"updateAt"`
	DeleteAt *time.Time `json:"deleteAt" bson:"deleteAt"`
}

func (this *Test) DocName() string { return "Test" }

func (this *Test) GetId() int64 { return this.Id }

func (this *Test) SetId(id int64) { this.Id = id }

func (this *Test) Create() error {
	return this.GetFactory().Create(this)
}

func (this *Test) Replace() error {
	return this.GetFactory().Replace(this)
}

func (this *Test) Save() error {
	if this.Id == 0 {
		this.CreateAt = time.Now()
		this.UpdateAt = time.Now()
		return this.GetFactory().Create(this)
	} else {
		this.UpdateAt = time.Now()
		return this.GetFactory().Replace(this)
	}
}

// 伪删除
func (this *Test) Delete() error {
	return this.GetFactory().Delete(this)
}

func (this *Test) UnDelete() error {
	return this.GetFactory().UnDelete(this)
}

// 真删除
func (this *Test) Destroy() error {
	return this.GetFactory().Destroy(this)
}

func (this *Test) ToString() string {
	return string(this.ToBytes())
}

func (this *Test) ToBytes() []byte {
	data, e := json.Marshal(this)
	ez.Try(e)
	return data
}
func (this *Test) Serialize() []byte {
	data, e := json.Marshal(this)
	ez.Try(e)
	return data
}
func (this *Test) UnSerialize(data []byte) []byte {
	ez.Try(json.Unmarshal(data, this))
	return data
}

func (this *Test) LoadCat() {
	if this.CatId == 0 {
		return
	}
	this.Cat, _ = NewTestCategoryCrud().FindId(this.CatId)
}

func (this *Test) LoadType() {
	if this.TypeId == 0 {
		return
	}
	this.Type, _ = NewTestTypeCrud().FindId(this.TypeId)
}

func (this *Test) LoadQuestion() {
	question, _ := NewQuestionCrud().FindBy(ss.M{"testId": this.Id}, []string{"id"}, 0, 0)
	this.Question = question
}

func (this *Test) LoadAnswer() {
	answer, _ := NewTestAnswerCrud().FindBy(ss.M{"testId": this.Id}, []string{"id"}, 0, 0)
	this.Answer = answer
}

func (this *Test) ClearRelationsBeforeSave() mgo.Doc {

	this.Cat = nil

	this.Type = nil

	this.Question = nil

	this.Answer = nil

	return this
}

func neverUsed_Test() {
	//导入ss包
	a := ss.M{}
	ez.Debug(a)
}

type TestAccessControl struct {
	Access  bool
	Message string
	Action  string //控制器Action,小写开头，如c,u,r,d
	Model   *Test
	Session *ez.Session
}

func NewTestAccessControl(model *Test, action string, session *ez.Session) *TestAccessControl {
	ctrl := &TestAccessControl{
		Access:  true,
		Model:   model,
		Action:  action,
		Session: session,
	}
	ez.DispatchToMany(mc.TestAccessControlEvent, ctrl, context.TODO())
	return ctrl
}
