package xl

import (
	"context"
	"ez/apps/xl/auto/mc"
	"fmt"
	"gitee.com/dreamwood/ez-go/db/mgo"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"go.mongodb.org/mongo-driver/mongo"
)

func (this *Answer) MakeChoice() *ss.M {
	return &ss.M{
		"value": this.Id,
		"label": this.Title,
	}
}

type AnswerCrud struct {
	Factory *mgo.Factory
}

func NewAnswerCrud(args ...interface{}) *AnswerCrud {
	factory := mgo.NewFactory(args...)
	factory.SetDoc(&Answer{})
	factory.SetArgus(mc.GetAnswerConfig())
	crud := &AnswerCrud{
		Factory: factory,
	}
	return crud
}

func (this *AnswerCrud) GetCollection() *mongo.Collection {
	return this.Factory.GetCollection()
}

func (this *AnswerCrud) FindId(id int64) (*Answer, error) {
	md := new(Answer)
	e := this.Factory.FindId(md, id)
	return md, e
}

func (this *AnswerCrud) FindBy(where ss.M, order []string, page int, limit int) ([]*Answer, error) {
	list := make([]*Answer, 0)
	qb := &ez.HttpQuery{
		Conditions: where,
		Order:      order,
		Page:       page,
		Limit:      limit,
	}

	e := this.Factory.FindBy(&list, qb)
	return list, e
}

func (this *AnswerCrud) FindOneBy(where ss.M, order []string) (*Answer, error) {
	md := new(Answer)
	qb := &ez.HttpQuery{
		Conditions: where,
		Order:      order,
	}
	e := this.Factory.FindOneBy(md, qb)
	return md, e
}

func neverUsed_Answer_func() {
	//导入ss包
	a := ss.M{}
	fmt.Printf("%v", a)
	fmt.Printf("%v", context.TODO())
}
