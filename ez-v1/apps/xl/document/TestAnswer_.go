package xl

import (
	"context"
	"encoding/json"
	"ez/apps/xl/auto/mc"
	"gitee.com/dreamwood/ez-go/db/mgo"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"time"
)

type TestAnswer struct {
	mgo.BaseDoc `bson:"-" json:"-"`
	Id          int64 `json:"id" bson:"id,omitempty"`

	Test *Test `json:"test" bson:"test"` //所属测试

	TestId int64 `json:"testId" bson:"testId"` //所属测试

	ScoreMin float64 `json:"scoreMin" bson:"scoreMin"` //分数下限

	ScoreMax float64 `json:"scoreMax" bson:"scoreMax"` //分数上限

	Content string `json:"content" bson:"content"` //解析

	CreateAt time.Time  `json:"createAt" bson:"createAt"`
	UpdateAt time.Time  `json:"updateAt" bson:"updateAt"`
	DeleteAt *time.Time `json:"deleteAt" bson:"deleteAt"`
}

func (this *TestAnswer) DocName() string { return "TestAnswer" }

func (this *TestAnswer) GetId() int64 { return this.Id }

func (this *TestAnswer) SetId(id int64) { this.Id = id }

func (this *TestAnswer) Create() error {
	return this.GetFactory().Create(this)
}

func (this *TestAnswer) Replace() error {
	return this.GetFactory().Replace(this)
}

func (this *TestAnswer) Save() error {
	if this.Id == 0 {
		this.CreateAt = time.Now()
		this.UpdateAt = time.Now()
		return this.GetFactory().Create(this)
	} else {
		this.UpdateAt = time.Now()
		return this.GetFactory().Replace(this)
	}
}

// 伪删除
func (this *TestAnswer) Delete() error {
	return this.GetFactory().Delete(this)
}

func (this *TestAnswer) UnDelete() error {
	return this.GetFactory().UnDelete(this)
}

// 真删除
func (this *TestAnswer) Destroy() error {
	return this.GetFactory().Destroy(this)
}

func (this *TestAnswer) ToString() string {
	return string(this.ToBytes())
}

func (this *TestAnswer) ToBytes() []byte {
	data, e := json.Marshal(this)
	ez.Try(e)
	return data
}
func (this *TestAnswer) Serialize() []byte {
	data, e := json.Marshal(this)
	ez.Try(e)
	return data
}
func (this *TestAnswer) UnSerialize(data []byte) []byte {
	ez.Try(json.Unmarshal(data, this))
	return data
}

func (this *TestAnswer) LoadTest() {
	if this.TestId == 0 {
		return
	}
	this.Test, _ = NewTestCrud().FindId(this.TestId)
}

func (this *TestAnswer) ClearRelationsBeforeSave() mgo.Doc {

	this.Test = nil

	return this
}

func neverUsed_TestAnswer() {
	//导入ss包
	a := ss.M{}
	ez.Debug(a)
}

type TestAnswerAccessControl struct {
	Access  bool
	Message string
	Action  string //控制器Action,小写开头，如c,u,r,d
	Model   *TestAnswer
	Session *ez.Session
}

func NewTestAnswerAccessControl(model *TestAnswer, action string, session *ez.Session) *TestAnswerAccessControl {
	ctrl := &TestAnswerAccessControl{
		Access:  true,
		Model:   model,
		Action:  action,
		Session: session,
	}
	ez.DispatchToMany(mc.TestAnswerAccessControlEvent, ctrl, context.TODO())
	return ctrl
}
