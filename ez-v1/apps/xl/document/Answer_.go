package xl

import (
	"context"
	"encoding/json"
	"ez/apps/xl/auto/mc"
	"gitee.com/dreamwood/ez-go/db/mgo"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"time"
)

type Answer struct {
	mgo.BaseDoc `bson:"-" json:"-"`
	Id          int64 `json:"id" bson:"id,omitempty"`

	Question *Question `json:"question" bson:"question"` //所属题目

	QuestionId int64 `json:"questionId" bson:"questionId"` //所属题目

	Title string `json:"title" bson:"title"` //解析

	ScoreMin float64 `json:"scoreMin" bson:"scoreMin"` //分数下限

	ScoreMax float64 `json:"scoreMax" bson:"scoreMax"` //分数上限

	CreateAt time.Time  `json:"createAt" bson:"createAt"`
	UpdateAt time.Time  `json:"updateAt" bson:"updateAt"`
	DeleteAt *time.Time `json:"deleteAt" bson:"deleteAt"`
}

func (this *Answer) DocName() string { return "Answer" }

func (this *Answer) GetId() int64 { return this.Id }

func (this *Answer) SetId(id int64) { this.Id = id }

func (this *Answer) Create() error {
	return this.GetFactory().Create(this)
}

func (this *Answer) Replace() error {
	return this.GetFactory().Replace(this)
}

func (this *Answer) Save() error {
	if this.Id == 0 {
		this.CreateAt = time.Now()
		this.UpdateAt = time.Now()
		return this.GetFactory().Create(this)
	} else {
		this.UpdateAt = time.Now()
		return this.GetFactory().Replace(this)
	}
}

// 伪删除
func (this *Answer) Delete() error {
	return this.GetFactory().Delete(this)
}

func (this *Answer) UnDelete() error {
	return this.GetFactory().UnDelete(this)
}

// 真删除
func (this *Answer) Destroy() error {
	return this.GetFactory().Destroy(this)
}

func (this *Answer) ToString() string {
	return string(this.ToBytes())
}

func (this *Answer) ToBytes() []byte {
	data, e := json.Marshal(this)
	ez.Try(e)
	return data
}
func (this *Answer) Serialize() []byte {
	data, e := json.Marshal(this)
	ez.Try(e)
	return data
}
func (this *Answer) UnSerialize(data []byte) []byte {
	ez.Try(json.Unmarshal(data, this))
	return data
}

func (this *Answer) LoadQuestion() {
	if this.QuestionId == 0 {
		return
	}
	this.Question, _ = NewQuestionCrud().FindId(this.QuestionId)
}

func (this *Answer) ClearRelationsBeforeSave() mgo.Doc {

	this.Question = nil

	return this
}

func neverUsed_Answer() {
	//导入ss包
	a := ss.M{}
	ez.Debug(a)
}

type AnswerAccessControl struct {
	Access  bool
	Message string
	Action  string //控制器Action,小写开头，如c,u,r,d
	Model   *Answer
	Session *ez.Session
}

func NewAnswerAccessControl(model *Answer, action string, session *ez.Session) *AnswerAccessControl {
	ctrl := &AnswerAccessControl{
		Access:  true,
		Model:   model,
		Action:  action,
		Session: session,
	}
	ez.DispatchToMany(mc.AnswerAccessControlEvent, ctrl, context.TODO())
	return ctrl
}
