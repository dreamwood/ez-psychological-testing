package xl

import (
	"context"
	"ez/apps/xl/auto/mc"
	"fmt"
	"gitee.com/dreamwood/ez-go/db/mgo"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"go.mongodb.org/mongo-driver/mongo"
)

func (this *TestAnswer) MakeChoice() *ss.M {
	return &ss.M{
		"value": this.Id,
		"label": this.Id,
	}
}

type TestAnswerCrud struct {
	Factory *mgo.Factory
}

func NewTestAnswerCrud(args ...interface{}) *TestAnswerCrud {
	factory := mgo.NewFactory(args...)
	factory.SetDoc(&TestAnswer{})
	factory.SetArgus(mc.GetTestAnswerConfig())
	crud := &TestAnswerCrud{
		Factory: factory,
	}
	return crud
}

func (this *TestAnswerCrud) GetCollection() *mongo.Collection {
	return this.Factory.GetCollection()
}

func (this *TestAnswerCrud) FindId(id int64) (*TestAnswer, error) {
	md := new(TestAnswer)
	e := this.Factory.FindId(md, id)
	return md, e
}

func (this *TestAnswerCrud) FindBy(where ss.M, order []string, page int, limit int) ([]*TestAnswer, error) {
	list := make([]*TestAnswer, 0)
	qb := &ez.HttpQuery{
		Conditions: where,
		Order:      order,
		Page:       page,
		Limit:      limit,
	}

	e := this.Factory.FindBy(&list, qb)
	return list, e
}

func (this *TestAnswerCrud) FindOneBy(where ss.M, order []string) (*TestAnswer, error) {
	md := new(TestAnswer)
	qb := &ez.HttpQuery{
		Conditions: where,
		Order:      order,
	}
	e := this.Factory.FindOneBy(md, qb)
	return md, e
}

func neverUsed_TestAnswer_func() {
	//导入ss包
	a := ss.M{}
	fmt.Printf("%v", a)
	fmt.Printf("%v", context.TODO())
}
