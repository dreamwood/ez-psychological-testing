package xl

import (
	"context"
	"ez/apps/xl/auto/mc"
	"fmt"
	"gitee.com/dreamwood/ez-go/db/mgo"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"go.mongodb.org/mongo-driver/mongo"
)

func (this *Choice) MakeChoice() *ss.M {
	return &ss.M{
		"value": this.Id,
		"label": this.Title,
	}
}

type ChoiceCrud struct {
	Factory *mgo.Factory
}

func NewChoiceCrud(args ...interface{}) *ChoiceCrud {
	factory := mgo.NewFactory(args...)
	factory.SetDoc(&Choice{})
	factory.SetArgus(mc.GetChoiceConfig())
	crud := &ChoiceCrud{
		Factory: factory,
	}
	return crud
}

func (this *ChoiceCrud) GetCollection() *mongo.Collection {
	return this.Factory.GetCollection()
}

func (this *ChoiceCrud) FindId(id int64) (*Choice, error) {
	md := new(Choice)
	e := this.Factory.FindId(md, id)
	return md, e
}

func (this *ChoiceCrud) FindBy(where ss.M, order []string, page int, limit int) ([]*Choice, error) {
	list := make([]*Choice, 0)
	qb := &ez.HttpQuery{
		Conditions: where,
		Order:      order,
		Page:       page,
		Limit:      limit,
	}

	e := this.Factory.FindBy(&list, qb)
	return list, e
}

func (this *ChoiceCrud) FindOneBy(where ss.M, order []string) (*Choice, error) {
	md := new(Choice)
	qb := &ez.HttpQuery{
		Conditions: where,
		Order:      order,
	}
	e := this.Factory.FindOneBy(md, qb)
	return md, e
}

func neverUsed_Choice_func() {
	//导入ss包
	a := ss.M{}
	fmt.Printf("%v", a)
	fmt.Printf("%v", context.TODO())
}
