package xl

import (
	"context"
	"encoding/json"
	"ez/apps/xl/auto/mc"
	"gitee.com/dreamwood/ez-go/db/mgo"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"time"
)

type Question struct {
	mgo.BaseDoc `bson:"-" json:"-"`
	Id          int64 `json:"id" bson:"id,omitempty"`

	Test *Test `json:"test" bson:"test"` //所属测试

	TestId int64 `json:"testId" bson:"testId"` //所属测试

	Sort int64 `json:"sort" bson:"sort"` //排序

	Title string `json:"title" bson:"title"` //问题标题

	IsMulti bool `json:"isMulti" bson:"isMulti"` //是否多选

	Note string `json:"note" bson:"note"` //备注

	Choice []*Choice `json:"choice" bson:"choice"` //选项

	Answer []*Answer `json:"answer" bson:"answer"` //解析

	CreateAt time.Time  `json:"createAt" bson:"createAt"`
	UpdateAt time.Time  `json:"updateAt" bson:"updateAt"`
	DeleteAt *time.Time `json:"deleteAt" bson:"deleteAt"`
}

func (this *Question) DocName() string { return "Question" }

func (this *Question) GetId() int64 { return this.Id }

func (this *Question) SetId(id int64) { this.Id = id }

func (this *Question) Create() error {
	return this.GetFactory().Create(this)
}

func (this *Question) Replace() error {
	return this.GetFactory().Replace(this)
}

func (this *Question) Save() error {
	if this.Id == 0 {
		this.CreateAt = time.Now()
		this.UpdateAt = time.Now()
		return this.GetFactory().Create(this)
	} else {
		this.UpdateAt = time.Now()
		return this.GetFactory().Replace(this)
	}
}

// 伪删除
func (this *Question) Delete() error {
	return this.GetFactory().Delete(this)
}

func (this *Question) UnDelete() error {
	return this.GetFactory().UnDelete(this)
}

// 真删除
func (this *Question) Destroy() error {
	return this.GetFactory().Destroy(this)
}

func (this *Question) ToString() string {
	return string(this.ToBytes())
}

func (this *Question) ToBytes() []byte {
	data, e := json.Marshal(this)
	ez.Try(e)
	return data
}
func (this *Question) Serialize() []byte {
	data, e := json.Marshal(this)
	ez.Try(e)
	return data
}
func (this *Question) UnSerialize(data []byte) []byte {
	ez.Try(json.Unmarshal(data, this))
	return data
}

func (this *Question) LoadTest() {
	if this.TestId == 0 {
		return
	}
	this.Test, _ = NewTestCrud().FindId(this.TestId)
}

func (this *Question) LoadChoice() {
	choice, _ := NewChoiceCrud().FindBy(ss.M{"questionId": this.Id}, []string{"id"}, 0, 0)
	this.Choice = choice
}

func (this *Question) LoadAnswer() {
	answer, _ := NewAnswerCrud().FindBy(ss.M{"questionId": this.Id}, []string{"id"}, 0, 0)
	this.Answer = answer
}

func (this *Question) ClearRelationsBeforeSave() mgo.Doc {

	this.Test = nil

	this.Choice = nil

	this.Answer = nil

	return this
}

func neverUsed_Question() {
	//导入ss包
	a := ss.M{}
	ez.Debug(a)
}

type QuestionAccessControl struct {
	Access  bool
	Message string
	Action  string //控制器Action,小写开头，如c,u,r,d
	Model   *Question
	Session *ez.Session
}

func NewQuestionAccessControl(model *Question, action string, session *ez.Session) *QuestionAccessControl {
	ctrl := &QuestionAccessControl{
		Access:  true,
		Model:   model,
		Action:  action,
		Session: session,
	}
	ez.DispatchToMany(mc.QuestionAccessControlEvent, ctrl, context.TODO())
	return ctrl
}
