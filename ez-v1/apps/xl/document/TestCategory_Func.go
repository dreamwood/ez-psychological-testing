package xl

import (
	"context"
	"ez/apps/xl/auto/mc"
	"fmt"
	"gitee.com/dreamwood/ez-go/db/mgo"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"go.mongodb.org/mongo-driver/mongo"
)

func (this *TestCategory) MakeChoice() *ss.M {
	return &ss.M{
		"value": this.Id,
		"label": this.Name,
	}
}

type TestCategoryCrud struct {
	Factory *mgo.Factory
}

func NewTestCategoryCrud(args ...interface{}) *TestCategoryCrud {
	factory := mgo.NewFactory(args...)
	factory.SetDoc(&TestCategory{})
	factory.SetArgus(mc.GetTestCategoryConfig())
	crud := &TestCategoryCrud{
		Factory: factory,
	}
	return crud
}

func (this *TestCategoryCrud) GetCollection() *mongo.Collection {
	return this.Factory.GetCollection()
}

func (this *TestCategoryCrud) FindId(id int64) (*TestCategory, error) {
	md := new(TestCategory)
	e := this.Factory.FindId(md, id)
	return md, e
}

func (this *TestCategoryCrud) FindBy(where ss.M, order []string, page int, limit int) ([]*TestCategory, error) {
	list := make([]*TestCategory, 0)
	qb := &ez.HttpQuery{
		Conditions: where,
		Order:      order,
		Page:       page,
		Limit:      limit,
	}

	e := this.Factory.FindBy(&list, qb)
	return list, e
}

func (this *TestCategoryCrud) FindOneBy(where ss.M, order []string) (*TestCategory, error) {
	md := new(TestCategory)
	qb := &ez.HttpQuery{
		Conditions: where,
		Order:      order,
	}
	e := this.Factory.FindOneBy(md, qb)
	return md, e
}

func neverUsed_TestCategory_func() {
	//导入ss包
	a := ss.M{}
	fmt.Printf("%v", a)
	fmt.Printf("%v", context.TODO())
}
