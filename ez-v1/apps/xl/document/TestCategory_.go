package xl

import (
	"context"
	"encoding/json"
	"ez/apps/xl/auto/mc"
	"gitee.com/dreamwood/ez-go/db/mgo"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"time"
)

type TestCategory struct {
	mgo.BaseDoc `bson:"-" json:"-"`
	Id          int64 `json:"id" bson:"id,omitempty"`

	Name string `json:"name" bson:"name"` //分类名称

	Cover ss.M `json:"cover" bson:"cover"` //封面

	Sort int64 `json:"sort" bson:"sort"` //排序

	CreateAt time.Time  `json:"createAt" bson:"createAt"`
	UpdateAt time.Time  `json:"updateAt" bson:"updateAt"`
	DeleteAt *time.Time `json:"deleteAt" bson:"deleteAt"`
}

func (this *TestCategory) DocName() string { return "TestCategory" }

func (this *TestCategory) GetId() int64 { return this.Id }

func (this *TestCategory) SetId(id int64) { this.Id = id }

func (this *TestCategory) Create() error {
	return this.GetFactory().Create(this)
}

func (this *TestCategory) Replace() error {
	return this.GetFactory().Replace(this)
}

func (this *TestCategory) Save() error {
	if this.Id == 0 {
		this.CreateAt = time.Now()
		this.UpdateAt = time.Now()
		return this.GetFactory().Create(this)
	} else {
		this.UpdateAt = time.Now()
		return this.GetFactory().Replace(this)
	}
}

// 伪删除
func (this *TestCategory) Delete() error {
	return this.GetFactory().Delete(this)
}

func (this *TestCategory) UnDelete() error {
	return this.GetFactory().UnDelete(this)
}

// 真删除
func (this *TestCategory) Destroy() error {
	return this.GetFactory().Destroy(this)
}

func (this *TestCategory) ToString() string {
	return string(this.ToBytes())
}

func (this *TestCategory) ToBytes() []byte {
	data, e := json.Marshal(this)
	ez.Try(e)
	return data
}
func (this *TestCategory) Serialize() []byte {
	data, e := json.Marshal(this)
	ez.Try(e)
	return data
}
func (this *TestCategory) UnSerialize(data []byte) []byte {
	ez.Try(json.Unmarshal(data, this))
	return data
}

func (this *TestCategory) ClearRelationsBeforeSave() mgo.Doc {

	return this
}

func neverUsed_TestCategory() {
	//导入ss包
	a := ss.M{}
	ez.Debug(a)
}

type TestCategoryAccessControl struct {
	Access  bool
	Message string
	Action  string //控制器Action,小写开头，如c,u,r,d
	Model   *TestCategory
	Session *ez.Session
}

func NewTestCategoryAccessControl(model *TestCategory, action string, session *ez.Session) *TestCategoryAccessControl {
	ctrl := &TestCategoryAccessControl{
		Access:  true,
		Model:   model,
		Action:  action,
		Session: session,
	}
	ez.DispatchToMany(mc.TestCategoryAccessControlEvent, ctrl, context.TODO())
	return ctrl
}
