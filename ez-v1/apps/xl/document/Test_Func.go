package xl

import (
	"context"
	"ez/apps/xl/auto/mc"
	"fmt"
	"gitee.com/dreamwood/ez-go/db/mgo"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"go.mongodb.org/mongo-driver/mongo"
)

func (this *Test) MakeChoice() *ss.M {
	return &ss.M{
		"value": this.Id,
		"label": this.Title,
	}
}

type TestCrud struct {
	Factory *mgo.Factory
}

func NewTestCrud(args ...interface{}) *TestCrud {
	factory := mgo.NewFactory(args...)
	factory.SetDoc(&Test{})
	factory.SetArgus(mc.GetTestConfig())
	crud := &TestCrud{
		Factory: factory,
	}
	return crud
}

func (this *TestCrud) GetCollection() *mongo.Collection {
	return this.Factory.GetCollection()
}

func (this *TestCrud) FindId(id int64) (*Test, error) {
	md := new(Test)
	e := this.Factory.FindId(md, id)
	return md, e
}

func (this *TestCrud) FindBy(where ss.M, order []string, page int, limit int) ([]*Test, error) {
	list := make([]*Test, 0)
	qb := &ez.HttpQuery{
		Conditions: where,
		Order:      order,
		Page:       page,
		Limit:      limit,
	}

	e := this.Factory.FindBy(&list, qb)
	return list, e
}

func (this *TestCrud) FindOneBy(where ss.M, order []string) (*Test, error) {
	md := new(Test)
	qb := &ez.HttpQuery{
		Conditions: where,
		Order:      order,
	}
	e := this.Factory.FindOneBy(md, qb)
	return md, e
}

func neverUsed_Test_func() {
	//导入ss包
	a := ss.M{}
	fmt.Printf("%v", a)
	fmt.Printf("%v", context.TODO())
}
