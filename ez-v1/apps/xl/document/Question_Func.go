package xl

import (
	"context"
	"ez/apps/xl/auto/mc"
	"fmt"
	"gitee.com/dreamwood/ez-go/db/mgo"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"go.mongodb.org/mongo-driver/mongo"
)

func (this *Question) MakeChoice() *ss.M {
	return &ss.M{
		"value": this.Id,
		"label": this.Title,
	}
}

type QuestionCrud struct {
	Factory *mgo.Factory
}

func NewQuestionCrud(args ...interface{}) *QuestionCrud {
	factory := mgo.NewFactory(args...)
	factory.SetDoc(&Question{})
	factory.SetArgus(mc.GetQuestionConfig())
	crud := &QuestionCrud{
		Factory: factory,
	}
	return crud
}

func (this *QuestionCrud) GetCollection() *mongo.Collection {
	return this.Factory.GetCollection()
}

func (this *QuestionCrud) FindId(id int64) (*Question, error) {
	md := new(Question)
	e := this.Factory.FindId(md, id)
	return md, e
}

func (this *QuestionCrud) FindBy(where ss.M, order []string, page int, limit int) ([]*Question, error) {
	list := make([]*Question, 0)
	qb := &ez.HttpQuery{
		Conditions: where,
		Order:      order,
		Page:       page,
		Limit:      limit,
	}

	e := this.Factory.FindBy(&list, qb)
	return list, e
}

func (this *QuestionCrud) FindOneBy(where ss.M, order []string) (*Question, error) {
	md := new(Question)
	qb := &ez.HttpQuery{
		Conditions: where,
		Order:      order,
	}
	e := this.Factory.FindOneBy(md, qb)
	return md, e
}

func neverUsed_Question_func() {
	//导入ss包
	a := ss.M{}
	fmt.Printf("%v", a)
	fmt.Printf("%v", context.TODO())
}
